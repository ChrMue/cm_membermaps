<?php 

/**
 * Language file for tl_page (de).
 *
 * PHP version 5
 * @copyright  Christian Muenster 2009-2020
 * @Author    : Christian Münster (ChrMue)
 * @Translator: Christian Münster (ChrMue) 
 * @package    CM_MemberMaps
 * @license    LGPL 
 * @filesource
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_page']['cm_map_apikey']= array('Google API Key', 'Bitte geben Sie den Key, den Sie von Google unter <a href="http://code.google.com/apis/maps/signup.html">http://code.google.com/apis/maps/signup.html</a> erhalten haben.');

$GLOBALS['TL_LANG']['tl_page']['cm_gm_accepance_legend'] = "Datenschutz";
$GLOBALS['TL_LANG']['tl_page']['cm_gm_acceptance_required']['0']="Datenschutz-Bestätigung";
$GLOBALS['TL_LANG']['tl_page']['cm_gm_acceptance_required']['1']="Verhindert die Datenübertragung zu Google bis zur Bestätigung";
$GLOBALS['TL_LANG']['tl_page']['cm_gm_acceptance_text']['0']="Datenschutzhinweis";
$GLOBALS['TL_LANG']['tl_page']['cm_gm_acceptance_text']['1']="Datenschutzhinweis - Insert-Tags werden unterstützt.";
