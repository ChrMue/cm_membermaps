<?php
/**
 * Contao extension: cm_membermaps
 * 
 * Copyright : &copy; 2020 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Christian Münster (ChrMue) 
 * 
 */
 
$GLOBALS['TL_LANG']['MOD']['cm_memberfinder'][0] = "Mitgliedersuche";
$GLOBALS['TL_LANG']['MOD']['cm_memberfinder'][1] = "Modul zur Anzeige eines Suchformulars zur Mitgliedersuche.";
$GLOBALS['TL_LANG']['MOD']['cm_membergooglemaps'][0] = "Mitgliederliste mit Google Maps";
$GLOBALS['TL_LANG']['MOD']['cm_membergooglemaps'][1] = "Mit diesem Modul können Sie die Mitgliedertabelle im Frontend mit einem Kartenausschnitt (Google Map) anzeigen.";
$GLOBALS['TL_LANG']['MOD']['cm_membergooglemapspos'][0] = "Googlemap zu einem Mitglied";
$GLOBALS['TL_LANG']['MOD']['cm_membergooglemapspos'][1] = "Mit diesem Modul können Sie im Frontend zu einem Mitglieder die Position in einem Kartenausschnitt (Google Map) anzuzeigen z.B. Hier finden Sie uns...";
$GLOBALS['TL_LANG']['FMD']['cm_memberfinder'][0] = "Mitgliedersuche";
$GLOBALS['TL_LANG']['FMD']['cm_memberfinder'][1] = "Verwenden Sie dieses Modul, um ein Suchformular zur Mitgliedersuche anzuzeigen.";
$GLOBALS['TL_LANG']['FMD']['cm_membergooglemaps'][0] = "Mitglieder mit Google Maps";
$GLOBALS['TL_LANG']['FMD']['cm_membergooglemaps'][1] = "Verwenden Sie dieses Modul, um zu den Mitgliederinformationen die Position in einem Kartenausschnitt (Google Map) anzuzeigen.";
$GLOBALS['TL_LANG']['FMD']['cm_membergooglemapspos'][0] = "Googlemap zu einem Mitglied";
$GLOBALS['TL_LANG']['FMD']['cm_membergooglemapspos'][1] = "Verwenden Sie dieses Modul, um zu einem Mitglied die Position in einem Kartenausschnitt (Google Map) anzuzeigen z.B. Hier finden Sie uns...";

$GLOBALS['TL_LANG']['FMD']['cm_membergooglemapsList'][0] = "Listenansicht - Mitgliederliste mit Google Maps";
$GLOBALS['TL_LANG']['FMD']['cm_membergooglemapsList'][1] = "Mit diesem Modul können Sie die Mitgliedertabelle im Frontend mit einem Kartenausschnitt (Google Map) anzeigen.";
$GLOBALS['TL_LANG']['FMD']['cm_membergooglemapsReader'][0] = "Detailansicht - Map zu einem Mitglied";
$GLOBALS['TL_LANG']['FMD']['cm_membergooglemapsReader'][1] = "Mit diesem Modul können Sie im Frontend zu einem Mitglieder die Position in einem Kartenausschnitt (Google Map) anzeigen";

$GLOBALS['TL_LANG']['MOD']['cm_mapstyles'][0] = "Kartenlayout";
$GLOBALS['TL_LANG']['MOD']['cm_mapstyles'][1] = "Definieren Sie individuelle Karten-Layouts für die Verwendung in der Erweiterung cm_membermaps.";
$GLOBALS['TL_LANG']['MOD']['cm_mapclusterstyles'][0] = "Markercluster-Layout";
$GLOBALS['TL_LANG']['MOD']['cm_mapclusterstyles'][1] = "Definieren Sie individuelle Layouts der Marker Clusters für die Verwendung in der Erweiterung cm_membermaps.";
