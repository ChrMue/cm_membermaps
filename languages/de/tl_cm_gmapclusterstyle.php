<?php
/**
 * Contao extension: cm_membermaps
 * 
 * Copyright : &copy; 2020 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Christian Münster (ChrMue) 
 * 
 */
$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['new']  = array('Neues Clustericon', 'Ein neues Clustericon erstellen');
 
$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['edit'] = array('Clustericon bearbeiten', 'Clustericon ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['editheader'] =array('Clustericon-Einstellungen bearbeiten', 'Einstellungen des Clusterlayouts ID %s bearbeiten');

$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['copy'] = array('Clustericon duplizieren', 'Clustericon ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['cut'] = array('Clustericon verschieben ', 'Clustericon ID %s verschieben');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['show'] = array('Clustericon-Details','Details des Clustericons ID %s anzeigen');

$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['delete']     = array('Clustericon löschen', 'Clustericon ID %s löschen');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['title_legend']='Clustericon';

$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['cm_clusterstylecomment_legend']='Cluster Icon Kommentar';
$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['comment']=array('Cluster Icon Kommntar','Geben Sie dem Icon einen Bechreibung');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['cm_clusterstyle_legend']='Cluster Icon';
$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['settextcolor']=array('Textfarbe festlegen','Aktivieren Sie die Checkbox um eine individuelle Textfarbe für das Icon festzulegen.');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['textcolor']=array('Textfarbe','Geben Sie die Textfarbe ein. Verwendenn Sie den Hexcode, statt der Namen wie z.B. black or red.');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['seturl']=array('Icon festlegen','Aktivieren Sie die Checkbox um eine inividuelles Icon festzulegen.');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['singleSRC']=array('Icon über die Dateiverwaltung wählen','Wählen Sie ein Icon über die Dateiverwaltung. Um eine URL einzugeben, heben sie ggf. die Auswahl auf.');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['url']=array('Url','Geben Sie eine Url an, sofern Sie kein Icon ausgewählt haben.');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['setsize']=array('Icongröße festlegen','Aktivieren Sie die Checkbox um inividuelle Werte für die Icon-Größe festzulegen.');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['width']=array('Breite (px)','Geben Sie die Breite des Icons ein. ');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['height']=array('Höhe (px)','Geben Sie die Höhe des Icons ein. Geben sie 0 ein um den Wert der Breite auch für die Höhe zu verwenden.');

