<?php
/**
 * Contao extension: cm_membermaps
 * 
 * Copyright : &copy; 2020 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Christian Münster (ChrMue) 
 * 
 */
$GLOBALS['TL_LANG']['tl_cm_gmapclusterlayout']['new']  = array('Neues Clusterlayout', 'Ein neues Clusterlayout erstellen');
 
$GLOBALS['TL_LANG']['tl_cm_gmapclusterlayout']['edit'] = array('Clusterlayout bearbeiten', 'Clusterlayout ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterlayout']['editheader'] =array('Clusterlayout-Einstellungen bearbeiten', 'Einstellungen des Clusterlayouts ID %s bearbeiten');

$GLOBALS['TL_LANG']['tl_cm_gmapclusterlayout']['copy'] = array('Clusterlayout duplizieren', 'Clusterlayout ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterlayout']['cut'] = array('Clusterlayout verschieben ', 'Clusterlayout ID %s verschieben');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterlayout']['show'] = array('Clusterlayout-Details','Details des Clusterlayouts ID %s anzeigen');

$GLOBALS['TL_LANG']['tl_cm_gmapclusterlayout']['delete']     = array('Clusterlayout löschen', 'Clusterlayout ID %s löschen');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterlayout']['title_legend']='Clusterlayout';
$GLOBALS['TL_LANG']['tl_cm_gmapclusterlayout']['tstamp']=array('Änderungsdatum','');
$GLOBALS['TL_LANG']['tl_cm_gmapclusterlayout']['name']=array('Layoutname','');
