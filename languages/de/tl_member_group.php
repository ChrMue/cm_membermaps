<?php
/*
 * Contao extension: cm_membermaps
 * 
 * Copyright : &copy; 2020 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Christian Münster (ChrMue) 
 * 
 */
 
$GLOBALS['TL_LANG']['tl_member_group']['cm_membergooglemaps_mapview'] = "Kartenansicht";
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconstd']['0'] = "Gruppenspezifisches Marker-Icon - Standard";
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconstd']['1'] = "Wählen Sie das Icon, dass für diese Gruppe verwendet werden soll. Wenn Sie kein Icon auswählen, wird das Standardicon in der Karte gezeigt.";
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconstd_anchor']['0']='Icon-Anker';
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconstd_anchor']['1']='Geben Sie den Ankerpunkt des Markers ein. left=0 und top=0 entsprechen oben links';
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconstdpopup_anchor']['0']='Popup-Anker (nur OSM)';
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconstdpopup_anchor']['1']='Geben Sie den Ankerpunkt des Popups ein (relativ zum Icon-Anker). Wenn der Icon-Anker z.B. (4,20) ist, dann ist (-4,-20) oben links';

$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconnear']['0'] = "Gruppenspezifisches Marker-Icon - Nahbereich";
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconnear']['1'] = "Wählen Sie das Icon, dass für diese Gruppe als ergebnis der Umkreissuche verwendet werden soll. Wenn Sie kein Icon auswählen, wird das Standardicon in der Karte gezeigt.";
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconnear_anchor']['0']='Icon-Anker';
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconnear_anchor']['1']='Geben Sie den Ankerpunkt des Markers ein. left=0 und top=0 entsprechen oben links';
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconnearpopup_anchor']['0']='Popup-Anker (nur OSM)';
$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconnearpopup_anchor']['1']='Geben Sie den Ankerpunkt des Markers ein (relativ zum Icon-Anker). Wenn der Icon-Anker z.B. (4,20) ist, dann ist (-4,-20) oben links';
