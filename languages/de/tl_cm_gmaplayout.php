<?php
/**
 * 
 * Contao extension: cm_membermaps
 * 
 * Copyright : &copy; 2020 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Christian Münster (ChrMue) 
 * 
 */
$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['new']  = array('Neues Kartenlayout', 'Ein neues Kartenlayout erstellen');
 
$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['edit'] = array('Kartenlayout bearbeiten', 'Kartenlayout ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['editheader'] =array('Kartenlayout-Einstellungen bearbeiten', 'Einstellungen des Kartenlayouts ID %s bearbeiten');

$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['copy'] = array('Kartenlayout duplizieren', 'Kartenlayout ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['cut'] = array('Kartenlayout verschieben ', 'Kartenlayout ID %s verschieben');
$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['show'] = array('Kartenlayout-Details','Details des Kartenlayouts ID %s anzeigen');

$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['delete']     = array('Kartenlayout löschen', 'Kartenlayout ID %s löschen');
$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['title_legend']='Kartenlayout';
$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['tstamp']=array('Änderungsdatum','');
$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['name']=array('Layoutname','');
?>
