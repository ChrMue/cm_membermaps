<?php 
/**
 * Language file for tl_module (de).
 *
 * PHP version 5
 * @copyright  Christian Muenster 2009-2020
 * @Author    : Christian Münster (ChrMue)
 * @Translator: Christian Münster (ChrMue) 
 * @package    CM_MemberMaps
 * @license    LGPL 
 * @filesource
 */

$GLOBALS['TL_LANG']['tl_module']['cm_map_coorderr'] = "Feld %s muss leer sein oder Geo-Koordindaten (ohne Lehrzeichnen) enthalten.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_intvalsanddefaulterr'] = "Die Werte im Feld %s müsen ohne Leerzeichen mit Komma getrennt eingegeben werden. in Wert muss als Standardwert in [] eingeschlossen sein.";

$GLOBALS['TL_LANG']['tl_module']['cm_field'] = "Feldname";
$GLOBALS['TL_LANG']['tl_module']['cm_order'] = "Reihenfolge";

$GLOBALS['TL_LANG']['tl_module']['sort_order'] = "Listensortierung";
$GLOBALS['TL_LANG']['tl_module']['cm_search_legend'] = "Such-Einstellungen";
$GLOBALS['TL_LANG']['tl_module']['cm_allowsearch_legend'] = "Suchanfragen zulassen";
$GLOBALS['TL_LANG']['tl_module']['cm_emailSettings_legend'] = "E-Mail-Einstellungen";

$GLOBALS['TL_LANG']['tl_module']['cm_usetags']['0']= 'Tags verwenden';
$GLOBALS['TL_LANG']['tl_module']['cm_usetags']['1']= 'Tags für die Suche verwenden';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_fieldsearch']['0'] = 'Suche in Feldern';
//$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_multifieldsearch']['1'] = 'Der vom Besucher eingebene Suchbegriff wird in mehrerer Feldern gesucht.';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_nofieldsearch']['0'] = 'keine';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_multifieldsearch']['0'] = 'Suche in mehreren Feldern';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_singlefieldsearch']['0'] = 'Suche in einem Feld';
//$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_multifieldsearch']['1'] = 'Der vom Besucher eingebene Suchbegriff wird in mehrerer Feldern gesucht.';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_searchfieldslist']['0']= 'Durchsuchte Felder';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_searchfieldslist']['1']= 'Wählen Sie die Felder, die durchsucht werden sollen.';

$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_showmapdetail'] ="Kartendarstellung - Detailseite";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_showmaplist'] ="Kartendarstellung - Listenansicht";

$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_showmap'] = "Kartenansicht";

$GLOBALS['TL_LANG']['tl_module']['cm_map_showcircle']['0'] = "Umkreis anzeigen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_showcircle']['1'] = "Bei aktiviertem Kontrollfeld wird ein Kreis mit dem vom Besucher angegeben Radius eingezeichnet.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_circlecolor']['0'] = "Farbe";
$GLOBALS['TL_LANG']['tl_module']['cm_map_circlecolor']['1'] = "Legen Sie die Farbe des Umkreises fest (Standard: rot).";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_sortandorder']['0'] = "Sortierung der Mitgliederliste";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_sortandorder']['1'] = "Legen Sie fest, nach welchen Feldern die Tabelle sortiert werden soll.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ondetail']['0'] = "Karte in der Detailansicht";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ondetail']['1'] = "Bei aktiviertem Kontrollfeld wird in der Detailansicht eine Karte angezeigt und die Position durch einen Marker gekennzeichnet.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_posdetail']['0'] = "Platzierung der Karte";
$GLOBALS['TL_LANG']['tl_module']['cm_map_posdetail']['1'] = "Wählen Sie, an welcher Position (bezogen auf die Detaildaten) die Karte angezeigt werden soll.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_heightdetail']['0'] = "Höhe";
$GLOBALS['TL_LANG']['tl_module']['cm_map_heightdetail']['1'] = "Geben Sie die Höhe der Karte in der Detailansicht.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_onlist']['0'] = "Karte in der Listenansicht";
$GLOBALS['TL_LANG']['tl_module']['cm_map_onlist']['1'] = "Bei aktiviertem Kontrollfeld wird in der Listenansicht eine Karte mit Markern angezeigt.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_poslist']['0'] = "Platzierung der Karte";
$GLOBALS['TL_LANG']['tl_module']['cm_map_poslist']['1'] = "Wählen Sie, an welcher Position (bezogen auf die Mitgliederliste) die Karte angezeigt werden soll.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_heightlist']['0'] = "Höhe";
$GLOBALS['TL_LANG']['tl_module']['cm_map_heightlist']['1'] = "Geben Sie die Höhe der Karte in der Listenansicht.";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_hidetable']['0'] = "Liste ausblenden";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_hidetable']['1'] = "Bei aktiviertem Kontrollfeld wird die Liste ausgeblendet.";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_showtableonsearch']['0'] = "Liste bei Suchanfragen anzeigen";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_showtableonsearch']['1'] = "Bei aktiviertem Kontrollfeld wird die Liste nach einer Suchanfrage angezeigt.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_maptypedetail']['0'] = "Kartentyp";
$GLOBALS['TL_LANG']['tl_module']['cm_map_maptypedetail']['1'] = "Wählen Sie den Kartentypen für der Detailansicht aus.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_disablewheeldetail']['0'] = "Mausrad (zoom) deaktivieren";
$GLOBALS['TL_LANG']['tl_module']['cm_map_disablewheeldetail']['1'] = "Aktivieren Sie dieses Kontrollfeld, um zu verhindern, dass sich durch Drehen des Mausrades die Zoomeinstellung der Karte ändert.";

$GLOBALS['TL_LANG']['tl_module']['cm_map_choosetypedetail']['0'] = "Kartentyp-Panel anzeigen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosetypedetail']['1'] = "Bei aktiviertem Kontrollfeld wird in der Detailansicht ein Panel zur Auswahl des Kartentyps eingeblendet";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrltypedetail']['0'] = "Form des Kartentyp-Panels";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrltypedetail']['1'] = "Wählen Sie die Form des Kartentyp-Panels (keine Auswahl=automatisch)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosenavdetail']['0'] = "Navigations-Panel anzeigen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosenavdetail']['1'] = "Bei aktiviertem Kontrollfeld wird in der Detailansicht ein Panel zur Navigation eingeblendet";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlnavdetail']['0'] = "Form des Navigations-Panels";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlnavdetail']['1'] = "Wählen Sie die Form des Navigations-Panels (keine Auswahl=automatisch)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosezoomdetail']['0'] = "Zoom-Panel anzeigen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosezoomdetail']['1'] = "Bei aktiviertem Kontrollfeld wird in der Detailansicht ein Panel zur Einstellung des Zoomfaktors eingeblendet";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlzoomdetail']['0'] = "Form des Zoom-Panels";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlzoomdetail']['1'] = "Wählen Sie die Form des Zoom-Panels (keine Auswahl=automatisch)";

$GLOBALS['TL_LANG']['tl_module']['cm_map_setstylelist']['0'] = "Kartenlayout festlegen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_setstylelist']['1'] = "Wählen Sie ein von Ihnen definiertes Kartenlayout für die Listenansicht.";

$GLOBALS['TL_LANG']['tl_module']['cm_map_setstyledetail']['0'] = "Kartenlayout festlegen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_setstyledetail']['1'] = "Wählen Sie ein von Ihnen definiertes Kartenlayout für die Detailansicht.";

$GLOBALS['TL_LANG']['tl_module']['cm_map_styleidlist']['0'] = "Individuelles Kartenlayout";
$GLOBALS['TL_LANG']['tl_module']['cm_map_styleidlist']['1'] = "Wählen Sie das geswünschte Kartenlayout für die Listenansicht.";

$GLOBALS['TL_LANG']['tl_module']['cm_map_styleiddetail']['0'] = "Individuelles Kartenlayout";
$GLOBALS['TL_LANG']['tl_module']['cm_map_styleiddetail']['1'] = "Wählen Sie das geswünschte Kartenlayout für die Detailansicht.";

$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_layout'] ="Layout der Listenansicht";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distancesearch']='Umkreissuche';

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_addressform']['0']='Adressfelder anzeigen';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_addressform']['1']='Feld zur Standorteingabe einblenden';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceform']['0']='Entfernungsfeld anzeigen';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceform']['1']='Feld zur Eingabe einer maximalen Entfernung eingeben';

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceasdropdown']['0']='Entfernungsfeld als Dropdown';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceasdropdown']['1']='Maximale Entfernung in einem Dropdown zur Auswahl bereitstellen';

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distancevalues']['0']='Werte des DropDown-Feldes';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distancevalues']['1']='Werte mit Komma getrennt eingeben. ein Wert muss in [] eingeschlossen und kennzeichnet damit den standardmäßig voreingestellten Wert.';

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distancesort']['0']='Nach Entfernung sortieren (Vorrang)';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distancesort']['1']='Sofern die Entfernungssuche verwendet wird, soll vorrangig nach der Entfernung sortiert werden.';

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceorder']['0']='Sortierung der Entfernung';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceorder']['1']='Reihenfolge der Sortierung der Entfernung';

$GLOBALS['TL_LANG']['tl_module']['cm_map_indivlocicon']['0']='Individuelles Standortsymbol';
$GLOBALS['TL_LANG']['tl_module']['cm_map_indivlocicon']['1']='Aktivieren Sie dieses Kontollfeld, um eine eigenes Standordicon festzulegen.';
$GLOBALS['TL_LANG']['tl_module']['cm_map_locicon']['0']='Standortsymbol';
$GLOBALS['TL_LANG']['tl_module']['cm_map_locicon']['1']='Wählen Sie das Symbol, dass bei der Entfernungssuche als Standortmarker angezeigt werden soll.';
$GLOBALS['TL_LANG']['tl_module']['cm_map_locicon_anchor']['0']='Anker';
$GLOBALS['TL_LANG']['tl_module']['cm_map_locicon_anchor']['1']='Geben Sie den Ankerpunkt des Markers ein. left=0 und top=0 entsprechen oben links';
$GLOBALS['TL_LANG']['tl_module']['cm_map_lociconpopup_anchor']['0']='Popup-Anker (nur OSM)';
$GLOBALS['TL_LANG']['tl_module']['cm_map_lociconpopup_anchor']['1']='Geben Sie den Ankerpunkt des Markers ein (relativ zum Icon-Anker). Wenn der Icon-Anker z.B. (4,20) ist, dann ist (-4,-20) oben links';

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceintab']['0']='Entfernungen in der Tabelle zeigen';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceintab']['1']='Entferungsangaben in der Tabelle anzeigen';

$GLOBALS['TL_LANG']['tl_module']['cm_map_showfar']['0']='Entfernte anzeigen';
$GLOBALS['TL_LANG']['tl_module']['cm_map_showfar']['1']='Bei aktiviertem Kontrollfeld werden auch Einträge außerhalb des angegebenen Radius angezeigt.';

$GLOBALS['TL_LANG']['tl_module']['cm_map_nearmarker']['0']='Unterschiedliche Marker (Nah/Fern)';
$GLOBALS['TL_LANG']['tl_module']['cm_map_nearmarker']['1']='Bei aktiviertem Kontrollfeld wird für Einträge innerhalb des angegebenen Radius ein anderer Marker verwendet.';

$GLOBALS['TL_LANG']['tl_module']['cm_addresslist_tableless']['0']='Tabellenloses Layout';
$GLOBALS['TL_LANG']['tl_module']['cm_addresslist_tableless']['1']='Bei aktiviertem Kontrollfeld erfolgt die Darstellung der Liste tabellenfrei. Die Formatierung nehmen Sie mitttels css vor.';

$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_fieldslist']['0'] = "Felder der Liste/Tabelle";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_fieldslist']['1'] = "Legen Sie fest, welche Felder in der Tabelle aufgeführt werden sollen und bestimmen Sie die Reihenfolge.";
$GLOBALS['TL_LANG']['tl_module']['cm_shownumberofadresses']['0']='Anzahl anzeigen';
$GLOBALS['TL_LANG']['tl_module']['cm_shownumberofadresses']['1']='Anzahl der gefundenn Ergebnisse anzeigen';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_hidedetaillink']['0']='Link auf Detailseite ausblenden';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_hidedetaillink']['1']='Link auf Detailseite ausblenden';
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_hidedetaillink']['0']='Link auf Detailseite ausblenden';
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_hidedetaillink']['1']='Link auf Detailseite ausblenden';
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_linktowebsite']['0']='Link zur Webseite';
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_linktowebsite']['1']='Den Namen der Webseite des Mitglieds als Link ausbilden';
$GLOBALS['TL_LANG']['tl_module']['cm_map_routetotable']['0'] = "Spalte mit Link zur Routenplanung anfügen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_routetotable']['1'] = "Aktivieren Sie das Kontrollfeld, wenn zur Google Routenplanung verlinkt werden soll.";

$GLOBALS['TL_LANG']['tl_module']['cm_map_linktowebsite']['0']='Link zur Webseite';
$GLOBALS['TL_LANG']['tl_module']['cm_map_linktowebsite']['1']='Den Namen der Webseite des Mitglieds als Link ausbilden';
$GLOBALS['TL_LANG']['tl_module']['cm_map_staybubbleopened']['0']='Sprechblase geöffnet lassen';
$GLOBALS['TL_LANG']['tl_module']['cm_map_staybubbleopened']['1']='Nach dem Anklicken bleibt die Sprechblase geöffnet, bis ein anderer Marker angeklickt oder die Blase über das Schließfeld geschlossen wird ';

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_asc']='aufsteigend';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_desc']='absteigend';

$GLOBALS['TL_LANG']['tl_module']['cm_map_maptypelist']['0'] = "Kartentyp";
$GLOBALS['TL_LANG']['tl_module']['cm_map_maptypelist']['1'] = "Wählen Sie den Kartentypen für der Listenansicht aus.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_disablewheellist']['0'] = "Mausrad (zoom) deaktivieren";
$GLOBALS['TL_LANG']['tl_module']['cm_map_disablewheellist']['1'] = "Aktivieren Sie dieses Kontrollfeld, um zu verhindern, dass sich durch Drehen des Mausrades die Zoomeinstellung der Karte ändert.";

$GLOBALS['TL_LANG']['tl_module']['cm_map_choosetypelist']['0'] = "Kartentyp-Panel anzeigen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosetypelist']['1'] = "Bei aktiviertem Kontrollfeld wird in der Listenansicht ein Panel zur Auswahl des Kartentyps eingeblendet";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrltypelist']['0'] = "Form des Kartentyp-Panels";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrltypelist']['1'] = "Wählen Sie die Form des Kartentyp-Panels (keine Auswahl=automatisch)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosenavlist']['0'] = "Navigations-Panel anzeigen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosenavlist']['1'] = "Bei aktiviertem Kontrollfeld wird in der Listenansicht ein Panel zur Navigation eingeblendet";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlnavlist']['0'] = "Form des Navigations-Panels";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlnavlist']['1'] = "Wählen Sie die Form des Navigations-Panels (keine Auswahl=automatisch)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosezoomlist']['0'] = "Zoom-Panel anzeigen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosezoomlist']['1'] = "Bei aktiviertem Kontrollfeld wird in der Listenansicht ein Panel zur Einstellung der Kartenvergrößerung eingeblendet";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlzoomlist']['0'] = "Form des Zoom-Panels";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlzoomlist']['1'] = "Wählen Sie die Form des Zoom-Panels (keine Auswahl=automatisch)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_routetolist']['0'] = "Link zur Routenplanung anzeigen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_routetolist']['1'] = "Aktivieren Sie das Kontrollfeld, wenn zur Google Routenplanung verlinkt werden soll.";

$GLOBALS['TL_LANG']['tl_module']['cm_map_zoomsm']='Klein';
$GLOBALS['TL_LANG']['tl_module']['cm_map_zoomlg']='Groß';
$GLOBALS['TL_LANG']['tl_module']['cm_map_zoomdfld']='Standard';

$GLOBALS['TL_LANG']['tl_module']['cm_map_stdzoom']['0'] = "Standard-Zoomfaktor";
$GLOBALS['TL_LANG']['tl_module']['cm_map_stdzoom']['1'] = "Wählen Sie den Zoomfaktor, der verwendet werden soll, sofern bei dem jeweiligen Mitglied kein individueller Faktor eingestellt ist.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_indivcenterlist']['0'] = "Individuelle Anordnung";
$GLOBALS['TL_LANG']['tl_module']['cm_map_indivcenterlist']['1'] = "Aktivieren Sie dieses Kontrollfeld, wenn Sie den Zoomfaktor und das Kartenzentrum manuell festlegen möchten.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_zoomlist']['0'] = "(max) Zoomfaktor";
$GLOBALS['TL_LANG']['tl_module']['cm_map_zoomlist']['1'] = "Legen Sie den max. Zoomfaktor für die Karte in der Listendarstellung fest. Bei individueller Anordung wird ebenfalls dieser Zoomfaktor verwendet.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_centerlist']['0'] = "Kartenzentrum";
$GLOBALS['TL_LANG']['tl_module']['cm_map_centerlist']['1'] = "Geben Sie den Koordinaten des Kartenzentrums in der Listenansicht ein.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_zoompos']['0'] = "Zoomfaktor";
$GLOBALS['TL_LANG']['tl_module']['cm_map_zoompos']['1'] = "Legen Sie den Zoomfaktor für die Karte in der Listendarstellung fest.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_indivcenterpos']['0'] = "Individuelles Kartenzentrum";
$GLOBALS['TL_LANG']['tl_module']['cm_map_indivcenterpos']['1'] = "Aktivieren Sie das Kontrollfeld um ein individuelles Kartenzentrum festzulegen.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_centerpos']['0'] = "Kartenzentrum";
$GLOBALS['TL_LANG']['tl_module']['cm_map_centerpos']['1'] = "Geben Sie den Koordinaten des Kartenzentrums in der Listenansicht ein.";

$GLOBALS['TL_LANG']['tl_module']['cm_map_viewnorm'] = "Karte";
$GLOBALS['TL_LANG']['tl_module']['cm_map_viewsat'] = "Satellit";
$GLOBALS['TL_LANG']['tl_module']['cm_map_viewhyb'] = "Satellit und Labels anzeigen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_viewphys'] = "Gelände";

$GLOBALS['TL_LANG']['tl_module']['cm_map_typhor_bar'] = "Horizontal";
$GLOBALS['TL_LANG']['tl_module']['cm_map_typdpdwn_menu'] = "DropDown";
$GLOBALS['TL_LANG']['tl_module']['cm_map_typdfld'] = "Standard";

$GLOBALS['TL_LANG']['tl_module']['cm_map_navsm'] = "SMALL";
$GLOBALS['TL_LANG']['tl_module']['cm_map_navzo'] = "ZOOM_PAN";
$GLOBALS['TL_LANG']['tl_module']['cm_map_navand'] = "ANDROID";
$GLOBALS['TL_LANG']['tl_module']['cm_map_navdfld'] = "DEFAULT";

$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_member']['0'] = "Mitglied";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_member']['1'] = "Wählen Sie das Mitglied, dessen Position (Adresse) in der Karte angezeigt werden soll.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_infotextdetail']['0'] = "Sprechblasentext";
$GLOBALS['TL_LANG']['tl_module']['cm_map_infotextdetail']['1'] = "Gestalten Sie den Text, der in der Sprechblase erscheinen soll.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_route'] = "Routenplanung";
$GLOBALS['TL_LANG']['tl_module']['cm_map_routetodetail']['0'] = "Formular zur Routenplanung anzeigen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_routetodetail']['1'] = "Aktivieren Sie das Kontrollfeld, wenn eine Routenplanung ermöglicht werden soll.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_infoShowOnload']['0'] = "Sprechblase sofort anzeigen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_infoShowOnload']['1'] = "Aktivieren Sie das Kontrollfeld, wenn die Sprechblase beim Laden der Seite sofort anfgezeigt werden soll.";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_pg']['0'] = "Suchergebnis-Seite";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_pg']['1'] = "Wählen Sie die Seite, auf der das Modul \"Mitgliederliste mit Google map\" integriert ist.";

$GLOBALS['TL_LANG']['tl_module']['cm_map_country']['0'] = "Land (Voreinstellung)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_country']['1'] = "Voreinstellung für das Ländereingabefeld/die Länderauswahl";

$GLOBALS['TL_LANG']['tl_module']['cm_map_country_as_select']['0'] = "Länderfeld als Auswahl";
$GLOBALS['TL_LANG']['tl_module']['cm_map_country_as_select']['1'] = "Stellt das Länderfeld als Auswahl statt als Eingabefeld bereit.";

$GLOBALS['TL_LANG']['tl_module']['cm_map_country_list']['0'] = "Länderliste";
$GLOBALS['TL_LANG']['tl_module']['cm_map_country_list']['1'] = "Länder (Kürzel), die zur Auswahl stehen sollen. z.B.  ch,de,nl. Lassen Sie das Feld leer, wenn keine Einschränkung erfolgen soll.";

$GLOBALS['TL_LANG']['tl_module']['cm_map_distance_as_select']['0'] = "Entfernungsfeld als Auswahl";
$GLOBALS['TL_LANG']['tl_module']['cm_map_distance_as_select']['1'] = "Stellt das Entfernungsfeld als Auswahl statt als Eingabefeld bereit.";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_fieldsearch']['0'] = "Suche in einzelnen Feldern";
$GLOBALS['TL_LANG']['tl_module']['cm_map_distance_as_select']['1'] = "Stellt eine Liste zur Auswahl der Feldes und ein Eingabefeld für den Suchbegriff bereit.";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_plzsearch']['0'] = "Postleitzahlensuche";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_plzsearch']['1'] = "Stellt ein Suchfeld für die Eingabe der ersten Stellen einer Postleitzahl bereit.";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_plznumberdigits']['0'] = "Anzahl der Stellen";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_plznumberdigits']['1'] = "Anzahl der Stellen, die mindestens eingegeben werden müssen.";

$GLOBALS['TL_LANG']['tl_module']['cm_map_clicktype']['0'] = "Sprechblase/Detailansicht";
$GLOBALS['TL_LANG']['tl_module']['cm_map_clicktype']['1'] = "Aktion zum Anzeigen der Sprechblase bzw. Öffnen der Detailansicht";

$GLOBALS['TL_LANG']['tl_module']['cm_mouseover_click'] = "Zeigen/Klick";
$GLOBALS['TL_LANG']['tl_module']['cm_click_dblclick'] = "Klick/Doppelklick";
$GLOBALS['TL_LANG']['tl_module']['cm_map_empty_legend'] = "Einstellungen bei leerem Suchergebnis";
$GLOBALS['TL_LANG']['tl_module']['cm_map_showmaponempty']['0'] = "Karte anzeigen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_showmaponempty']['1'] = "Aktivieren Sie das Kontrollfeld, wenn bei einem leeren Suchergebnis trotzdem einen Karte angezeigt werden soll.";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_notfound']['0'] = "Text";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_notfound']['1'] = "Lassen Sie das Textfeld leer, wenn bei leerem Suchergebnis keine Textausgabe erfolgen soll.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_zoomempty']['0'] = "Zoomfaktor";
$GLOBALS['TL_LANG']['tl_module']['cm_map_zoomempty']['1'] = "Legen Sie den Zoomfaktor für die Standardkarte fest.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_centerempty']['0'] = "Kartenzentrum";
$GLOBALS['TL_LANG']['tl_module']['cm_map_centerempty']['1'] = "Legen Sie das Kartenzentrum für die Standardkarte fest.";

$GLOBALS['TL_LANG']['tl_module']['cm_memberdetail_pg']['0'] = "Detailseite";
$GLOBALS['TL_LANG']['tl_module']['cm_memberdetail_pg']['1'] = "Wählen Sie die zugehörige Detailseite.";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_pg']['0'] = "Listenseite";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_pg']['1'] = "Wählen Sie die zugehörige Seite mit der Mitgliederliste.";

$GLOBALS['TL_LANG']['tl_module']['cm_register_showonmap']['0'] = "Kartendarstellung aktivieren";
$GLOBALS['TL_LANG']['tl_module']['cm_register_showonmap']['1'] = "Aktivieren Sie diese Option um die Kartendarstellung bei der Registrierung automatisch zu aktivieren.";
$GLOBALS['TL_LANG']['tl_module']['cm_register_autocoord']['0'] = "Koordinaten automatisch ermitteln";
$GLOBALS['TL_LANG']['tl_module']['cm_register_autocoord']['1'] = "Aktivieren Sie diese Option um die Koordinaten anhand der Adressdaten bei der Registrierung automatisch zu ermitteln.";

$GLOBALS['TL_LANG']['tl_module']['templatelst_legend']= "Templates für die Listenansicht";
$GLOBALS['TL_LANG']['tl_module']['templatedtl_legend']= "Templates für die Detailansicht";
$GLOBALS['TL_LANG']['tl_module']['map_lsttemplate']['0']="Haupttemplate";
$GLOBALS['TL_LANG']['tl_module']['map_lsttemplate']['1']="Haupttemplate der Listenansicht";
$GLOBALS['TL_LANG']['tl_module']['map_tbltemplate']['0']="Listentemplate";
$GLOBALS['TL_LANG']['tl_module']['map_tbltemplate']['1']="Template der Liste (Tabelle oder tabellenlos)";
$GLOBALS['TL_LANG']['tl_module']['map_inftemplate']['0']="Infotemplate";
$GLOBALS['TL_LANG']['tl_module']['map_inftemplate']['1']="Template der Bubble";
$GLOBALS['TL_LANG']['tl_module']['map_dtltemplate']['0']="Haupttemplate";
$GLOBALS['TL_LANG']['tl_module']['map_dtltemplate']['1']="Haupttemplate der Detailansicht";
$GLOBALS['TL_LANG']['tl_module']['map_infdtltemplate']['0']="Infotemplate";
$GLOBALS['TL_LANG']['tl_module']['map_infdtltemplate']['1']="Template der Bubble";

$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster']['0']="Marker zusammenfassen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster']['1']="Nah zusammenliege Marker zusammenfassen (Clustering)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_gridsize']['0']="Clustergröße";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_gridsize']['1']="Je größer das Raster in dem Marker zusammengefasst werden.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_maxzoom']['0']="Größte Zoomstufe";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_maxzoom']['1']="Zoomstufe, bis zu der Marker zusammengefasst werden.";
$GLOBALS['TL_LANG']['tl_module']['cm_email_nameRequired']['0']="Angabe des Namens erforderlich";
$GLOBALS['TL_LANG']['tl_module']['cm_email_nameRequired']['1']="Sofern ein Formular zur E-Mail-Benachrichtigung des Mitglieds angezeigt wird, legen Sie hier fest, ob neben der E-Mail-Adresse auch der Name im Formular angegeben werden muss.";

$GLOBALS['TL_LANG']['tl_module']['cm_gc_acceptance_required']['0']="Bestätigung Datenschutz Geocoding";
$GLOBALS['TL_LANG']['tl_module']['cm_gc_acceptance_required']['1']="Bestätigung, dass die angegebene Adresse für das Geocoding an Google gesendet wird.";
$GLOBALS['TL_LANG']['tl_module']['cm_gc_acceptance_label']['0']="Label für die Checkbox";
$GLOBALS['TL_LANG']['tl_module']['cm_gc_acceptance_label']['1']="Label für die Checkbox";

$GLOBALS['TL_LANG']['tl_module']['cm_gm_accepance_legend'] = "Datenschutz";
$GLOBALS['TL_LANG']['tl_module']['cm_gm_acceptance_required']['0']="Datenschutz-Bestätigung";
$GLOBALS['TL_LANG']['tl_module']['cm_gm_acceptance_required']['1']="Verhindert die Datenübertragung zu Google bis zur Bestätigung";
$GLOBALS['TL_LANG']['tl_module']['cm_gm_acceptance_text']['0']="Datenschutzhinweis";
$GLOBALS['TL_LANG']['tl_module']['cm_gm_acceptance_text']['1']="Datenschutzhinweis - Insert-Tags werden unterstützt.";


