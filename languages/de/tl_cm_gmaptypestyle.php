<?php
/**
 * 
 * Contao extension: cm_membermaps
 * 
 * Copyright : &copy; 2020 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Christian Münster (ChrMue) 
 * 
 */
 
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['administrative'] = 'Verwaltungsgebiete';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['administrative.country'] = 'Länder';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['administrative.land_parcel'] = 'Landgrundstücke';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['administrative.locality'] = 'Orte';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['administrative.neighborhood'] = 'Nahbereiche/Stadtviertel';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['administrative.province'] = 'Bundesländer/Provinzen';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all'] = 'alle Selektortypen';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['landscape'] = 'Landschaften';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['landscape.man_made'] = 'von Menschen geschaffene Strukturen';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['landscape.natural'] = 'natürliche Elemente';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi'] = 'interessante Orte';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi.attraction'] = 'Touristenattraktionen';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi.business'] = 'Unternehmen';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi.government'] = 'Regierungsgebäude';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi.medical'] = 'Notdienste an (Krankenhäuser, Apotheken, Polizei, Ärzte us';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi.park'] = 'Parks';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi.place_of_worship'] = 'religiöse Orte an, wie Kirchen, Tempel oder Mosch';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi.school'] = 'Schulen und andere Bildungseinrichtungen';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi.sports_complex'] = 'Sportanlagen';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['road'] = 'alle Straßen';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['road.arterial'] = 'Straßen mit hohem Verkehrsaufkommen';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['road.highway'] = 'Autobahnen';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['road.local'] = 'örtliche Straßen';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['transit'] = 'alle Stationen und Linien der öffentlichen Verkehrsmittel';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['transit.line'] = 'Linien der öffentlichen Verkehrsmittel';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['transit.station'] = 'alle Stationen der öffentlichen Verkehrsmittel';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['transit.station.airport'] = 'Flughäfen';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['transit.station.bus'] = 'Bushaltestellen';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['transit.station.rail'] = 'Bahnhöfe';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['water'] = 'Gewässer';

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_setvisibility'] = array('Sichtbarkeit festlegen','Aktivieren Sie diese Kontrollfeld um die Sichtbarkeit des Elements festzulegen');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_invert_lightness'] = array('Helligkeit invertieren','Aktivieren Sie diese Kontrollfeld um die Helligkeit des Elements umzukehren');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_sethue'] = array('Farbton festlegen','Aktivieren Sie diese Kontrollfeld um den Farbton des Elements festzulegen');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_setsaturation'] = array('Sättigung festlegen','Aktivieren Sie diese Kontrollfeld um die Sättigung des Elements festzulegen');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_setlightness'] = array('Helligkeit festlegen','Aktivieren Sie diese Kontrollfeld um die Helligkeit des Elements festzulegen');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_setgamma'] = array('Gammawert festlegen','Aktivieren Sie diese Kontrollfeld um den Gammawert des Elements festzulegen');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_setvisibility'] = array('Sichtbarkeit festlegen','Aktivieren Sie diese Kontrollfeld um ');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_invert_lightness'] = array('Helligkeit invertieren','Aktivieren Sie diese Kontrollfeld um die Helligkeit des Elements umzukehren');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_sethue'] = array('Farbton festlegen','Aktivieren Sie diese Kontrollfeld um den Farbton der Geometrie festzulegen');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_setsaturation'] = array('Sättigung festlegen','Aktivieren Sie diese Kontrollfeld um die Sättigung der Geometrie festzulegen');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_setlightness'] = array('Helligkeit festlegen','Aktivieren Sie diese Kontrollfeld um die Helligkeit der Geometrie festzulegen');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_setgamma'] = array('Gammawert festlegen','Aktivieren Sie diese Kontrollfeld um den Gammawert der Geometrie festzulegen');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_setvisibility'] = array('Sichtbarkeit festlegen','Aktivieren Sie diese Kontrollfeld um die Sichtbarkeit der Beschriftung festzulegen');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_invert_lightness'] = array('Helligkeit invertieren','Aktivieren Sie diese Kontrollfeld um die Helligkeit des Elements umzukehren');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_sethue'] = array('Farbton festlegen','Aktivieren Sie diese Kontrollfeld um den Farbton der Beschriftung festzulegen');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_setsaturation'] = array('Sättigung festlegen','Aktivieren Sie diese Kontrollfeld um die Sättigung der Beschriftung festzulegen');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_setlightness'] = array('Helligkeit festlegen','Aktivieren Sie diese Kontrollfeld um die Helligkeit der Beschriftung festzulegen');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_setgamma'] = array('Gammawert festlegen','Aktivieren Sie diese Kontrollfeld um den Gammawert der Beschriftung festzulegen');

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_visibility'] = array('Sichtbarkeit','Wählen Sie den gewünschten Eintrag.');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_hue'] = array('Farbton','Wählen Sie einen Farbton aus.');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_saturation'] = array('Sättigung','Geben Sie die Sättigung im Bereich von -100 bis 100 ein.');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_lightness'] = array('Helligkeit','Geben Sie die Helligkeit im Bereich von -100 bis 100 ein.');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_gamma'] = array('Gammawert','Geben Sie den gewünschten Gammawert ein.');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_visibility'] = array('Sichtbarkeit','Wählen Sie den gewünschten Eintrag.');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_hue'] = array('Farbton','Wählen Sie einen Farbton aus.');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_saturation'] = array('Sättigung','Geben Sie die Sättigung im Bereich von -100 bis 100 ein.');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_lightness'] = array('Helligkeit','Geben Sie die Helligkeit im Bereich von -100 bis 100 ein.');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_gamma'] = array('Gammawert','Geben Sie den gewünschten Gammawert ein.');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_visibility'] = array('Sichtbarkeit','Wählen Sie den gewünschten Eintrag.');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_hue'] = array('Farbton','Wählen Sie einen Farbton aus.');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_saturation'] = array('Sättigung','Geben Sie die Sättigung im Bereich von -100 bis 100 ein.');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_lightness'] = array('Helligkeit','Geben Sie die Helligkeit im Bereich von -100 bis 100 ein.');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_gamma'] = array('Gammawert','Geben Sie den gewünschten Gammawert ein.');

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['feature'] = array('Kartenelement','Wählen Sie das Kartenelement, das Sie formatieren möchten.');

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['cm_all_legend'] = 'Gesamtes Element';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['cm_gty_legend'] = 'Geometrie (Darstellung)';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['cm_lbl_legend'] = 'Beschriftung (Label)';

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['new']  = array('Neue Styledefinition', 'Eine neue Styledefinition erstellen'); 
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['edit'] = array('Styledefinition bearbeiten', 'Styledefinition ID %s bearbeiten');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['editheader'] =array('Styledefinition bearbeiten', 'Die Styledefinition-Einstellungen bearbeiten');

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['copy'] = array('Styledefinition duplizieren', 'Styledefinition ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['cut'] = array('Styledefinition verschieben ', 'Styledefinition ID %s verschieben');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['show'] = array('Styledefinition-Details','Details des Styledefinition ID %s anzeigen');

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['delete']     = array('Styledefinition löschen', 'Styledefinition ID %s löschen');

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['pasteafter'] = array('Oben einfügen', 'Nach der Styledefinition ID %s einfügen');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['pastenew']   = array('Neue Styledefinition oben erstellen', 'Neues Element nach der Styledefinition ID %s erstellen');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['toggle']     = array('Sichtbarkeit ändern', 'Die Sichtbarkeit der Styledefinition ID %s ändern');

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['mapelement_legend'] = 'Kartenelement';

