<?php
/**
 * TL_ROOT/system/modules/cm_membergooglemaps/languages/fr/default.php 
 * 
 * Contao extension: cm_membergooglemaps 1.3.0 rc2 
 * French translation file 
 * 
 * Copyright : &copy; 2010 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Jérôme DENIS (irongomme), http://www.sweb-developpement.com 
 * 
 * This file was created automatically be the Contao extension repository translation module.
 * Do not edit this file manually. Contact the author or translator for this module to establish 
 * permanent text corrections which are update-safe. 
 */
 
$GLOBALS['TL_LANG']['MSC']['cm_map_error'] = ">>> La vue de la carte est actuellement indisponible. <<<";
$GLOBALS['TL_LANG']['MSC']['list_perPage'] = "Résultats par page";
$GLOBALS['TL_LANG']['MSC']['cm_map_getroute'] = "Obtenir itinéraire";
$GLOBALS['TL_LANG']['MSC']['cm_map_getroutelist'] = "Obtenir itinéraire";
$GLOBALS['TL_LANG']['MSC']['cm_map_topos'] = "à";
$GLOBALS['TL_LANG']['MSC']['cm_map_frompos'] = "De";
$GLOBALS['TL_LANG']['MSC']['cm_map_fromaddr'] = "Départ";
$GLOBALS['TL_LANG']['MSC']['cm_map_toaddr'] = "Arrivée";

$GLOBALS['TL_LANG']['MSC']['cm_radius_search'] = "Umkreissuche";
$GLOBALS['TL_LANG']['MSC']['cm_lbl_location'] = "Adresse:";
$GLOBALS['TL_LANG']['MSC']['cm_lbl_country'] = "Land:";
$GLOBALS['TL_LANG']['MSC']['cm_lbl_max_dist'] = "Entfernung:";
$GLOBALS['TL_LANG']['MSC']['cm_distitem'] = "km";
$GLOBALS['TL_LANG']['MSC']['cm_distsearch_label'] = "Suchen";

?>
