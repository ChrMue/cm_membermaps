<?php
/**
 * TL_ROOT/system/modules/cm_membergooglemaps/languages/en/tl_cm_gmaplayout.php 
 * 
 * Contao extension: cm_membergooglemaps
 * 
 * Copyright : &copy; 2013 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Dave Doyle 
 * 
 */
$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['new']  = array('New card layout', 'Create a new card layout');
 
$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['edit'] = array('Edit map layout', 'Board Layout ID %s');
$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['editheader'] =array('Edit map layout settings', 'ID %s settings of the map layout');

$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['copy'] = array('Duplicate card layout', 'Board Layout ID %s duplicate');
$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['cut'] = array('Move map layout', 'Move map layout ID %s');
$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['show'] = array('Card Layout Details','Details of the card layouts show ID %s');

$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['delete']     = array('Clear map layout', 'Board Layout ID %s delete');
$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['title_legend']='Board Layout';
$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['tstamp']=array('Date Modified','');
$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['name']=array('Layout name','');
?>
