<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');
/**
 * TL_ROOT/system/modules/cm_membergooglemaps/languages/de/tl_module.php 
 * 
 * Contao extension: cm_membergooglemaps 1.3.0 rc2 
 * 
 * Copyright : © 2013 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Dave Doyle 
 * 
 */

$GLOBALS['TL_LANG']['tl_module']['cm_map_coorderr'] = "Field %s must contain its empty or Geo Coordinates (without teaching Drawing).";
$GLOBALS['TL_LANG']['tl_module']['cm_map_intvalsanddefaulterr'] = "The values ​​in the %s field must be entered without spaces and separated by commas. in value must be included as a default value in [].";

$GLOBALS['TL_LANG']['tl_module']['cm_field'] = "Field name";
$GLOBALS['TL_LANG']['tl_module']['cm_order'] = "Order";

$GLOBALS['TL_LANG']['tl_module']['sort_order'] = "Sorting list";
$GLOBALS['TL_LANG']['tl_module']['cm_search_legend'] = "Search legend";
$GLOBALS['TL_LANG']['tl_module']['cm_allowsearch_legend'] = "Allow search requests";
$GLOBALS['TL_LANG']['tl_module']['cm_emailSettings_legend'] = "eMail settings ";

$GLOBALS['TL_LANG']['tl_module']['cm_usetags']['0']= "Use tags";
$GLOBALS['TL_LANG']['tl_module']['cm_usetags']['1']= "Use tags for the search";

//$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_multifieldsearch']['1']="The keyword entered by the visitor is searched in several fields.";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_fieldsearch']['0'] = 'Search in fields';
//$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_multifieldsearch']['1'] = 'Der vom Besucher eingebene Suchbegriff wird in mehrerer Feldern gesucht.';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_nofieldsearch']['0'] = 'no';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_multifieldsearch']['0'] = 'Multi-field search';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_singlefieldsearch']['0'] = 'Single-field search';
//$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_multifieldsearch']['1'] = 'Der vom Besucher eingebene Suchbegriff wird in mehrerer Feldern gesucht.';


$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_searchfieldslist']['0']="Scanned fields";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_searchfieldslist']['1']="Select the fields to be searched.";

$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_showmapdetail'] ="Map display - Detail Page";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_showmaplist'] ="Map view - List view";

$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_showmap'] = "Map";

$GLOBALS['TL_LANG']['tl_module']['cm_map_showcircle']['0'] = "Show radius";
$GLOBALS['TL_LANG']['tl_module']['cm_map_showcircle']['1'] = "If the check box is activated, a circle is drawn with a radius specified by the visitor";
$GLOBALS['TL_LANG']['tl_module']['cm_map_circlecolor']['0'] = "Color";
$GLOBALS['TL_LANG']['tl_module']['cm_map_circlecolor']['1'] = "Set the color of the perimeter (Default is Red)";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_sortandorder']['0'] = "Sorting the list of members";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_sortandorder']['1'] = "Specify which fields you want to sort in the table.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ondetail']['0'] = "Map in detail view";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ondetail']['1'] = "If the check box is activated, a map is displayed in the detail view, and in the position of a marker";
$GLOBALS['TL_LANG']['tl_module']['cm_map_posdetail']['0'] = "Placement of the map";
$GLOBALS['TL_LANG']['tl_module']['cm_map_posdetail']['1'] = "Select which position (relative to the detail data), where the map will be displayed.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_heightdetail']['0'] = "Height";
$GLOBALS['TL_LANG']['tl_module']['cm_map_heightdetail']['1'] = "Enter the height of the map in detail view";
$GLOBALS['TL_LANG']['tl_module']['cm_map_onlist']['0'] = "Show in list view";
$GLOBALS['TL_LANG']['tl_module']['cm_map_onlist']['1'] = "If the check box is activated, a map with markers is displayed in the list view";
$GLOBALS['TL_LANG']['tl_module']['cm_map_poslist']['0'] = "Placement of the map";
$GLOBALS['TL_LANG']['tl_module']['cm_map_poslist']['1'] = "Select which position (based on the list of members), where the map is displayed";
$GLOBALS['TL_LANG']['tl_module']['cm_map_heightlist']['0'] = "Height";
$GLOBALS['TL_LANG']['tl_module']['cm_map_heightlist']['1'] = "Enter the height of the map in the list view";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_hidetable']['0'] = "Hide list";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_hidetable']['1'] = "If the check box is activated, the list disappears";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_showtableonsearch']['0'] = "Show list for searches";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_showtableonsearch']['1'] = "If the check box is activated, the list is displayed after a search";
$GLOBALS['TL_LANG']['tl_module']['cm_map_maptypedetail']['0'] = "Map type";
$GLOBALS['TL_LANG']['tl_module']['cm_map_maptypedetail']['1'] = "Select the card types for the detail view";
$GLOBALS['TL_LANG']['tl_module']['cm_map_disablewheeldetail']['0'] = "Disable mouse wheel (zoom)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_disablewheeldetail']['1'] = "select the check box to prevent changing the zoom factor by the mouse wheel .";

$GLOBALS['TL_LANG']['tl_module']['cm_map_choosetypedetail']['0'] = "Show Map Type Panel";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosetypedetail']['1'] = "If the check box is activated, a panel for selecting the card type is displayed in the detail view";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrltypedetail']['0'] = "Shape of the card-type panels";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrltypedetail']['1'] = "Choose the shape of the card-type panels (no selection = automatic)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosenavdetail']['0'] = "Show the navigation panel";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosenavdetail']['1'] = "If the check box is activated, a panel appears to navigate the detail view";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlnavdetail']['0'] = "Shape of the navigation panel";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlnavdetail']['1'] = "Choose the shape of the navigation panel (no selection = automatic)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosezoomdetail']['0'] = "Show the zoom panel";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosezoomdetail']['1'] = "If the check box is activated, a panel appears to change the zoom factor.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlzoomdetail']['0'] = "Shape of the zoom panel";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlzoomdetail']['1'] = "Choose the shape of the zoom panel (no selection = automatic)";

$GLOBALS['TL_LANG']['tl_module']['cm_map_setstylelist']['0'] = "Set card layout";
$GLOBALS['TL_LANG']['tl_module']['cm_map_setstylelist']['1'] = "Choose a card layout that you define for the list view";

$GLOBALS['TL_LANG']['tl_module']['cm_map_setstyledetail']['0'] = "Set card layout";
$GLOBALS['TL_LANG']['tl_module']['cm_map_setstyledetail']['1'] = "Choose a card layout that you define for the detail map";

$GLOBALS['TL_LANG']['tl_module']['cm_map_styleidlist']['0'] = "Individual card layout";
$GLOBALS['TL_LANG']['tl_module']['cm_map_styleidlist']['1'] = "Select the desired card layout for the list view.";

$GLOBALS['TL_LANG']['tl_module']['cm_map_styleiddetail']['0'] = "Individual card layout";
$GLOBALS['TL_LANG']['tl_module']['cm_map_styleiddetail']['1'] = "Select the desired card layout for details.";

$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_layout'] ="Layout of the list view";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distancesearch']='Browse nearby';

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_addressform']['0']="Show address fields";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_addressform']['1']="Show the location input field";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceform']['0']="Show distance field";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceform']['1']="Enter field to enter a maximum distance";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceasdropdown']['0']="Distance field as Dropddown";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceasdropdown']['1']="Provide maximum distance with a drop down to select";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distancevalues']['0']="Values ​​of the dropdown field";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distancevalues']['1']="Enter values ​​separated by commas. A value must be enclosed in [] and thus identifies the default preset value.";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distancesort']['0']="Sort by distance (priority)";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distancesort']['1']="If the distance search is used, which will primarily be sorted by distance.";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceorder']['0']="Sort by distance";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceorder']['1']="Sort order of the distance";

$GLOBALS['TL_LANG']['tl_module']['cm_map_indivlocicon']['0']="Individual site icon";
$GLOBALS['TL_LANG']['tl_module']['cm_map_indivlocicon']['1']="Check this control to define a separate standard icon";
$GLOBALS['TL_LANG']['tl_module']['cm_map_locicon']['0']="Standortsymbol";
$GLOBALS['TL_LANG']['tl_module']['cm_map_locicon']['1']="Select the icon that appears in the distance as a location marker search is becoming";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceintab']['0']="Show distances in the table";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceintab']['0']="Show distances in the table";

$GLOBALS['TL_LANG']['tl_module']['cm_map_showfar']['0']="Distant View";
$GLOBALS['TL_LANG']['tl_module']['cm_map_showfar']['1']="If the check box is activated, entries outside the specified radius will also be displayed";

$GLOBALS['TL_LANG']['tl_module']['cm_map_nearmarker']['0']="Different markers (Near / Far)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_nearmarker']['1']="If the check box is activated, another marker for entries within the specified radius will be used";

$GLOBALS['TL_LANG']['tl_module']['cm_addresslist_tableless']['0']="Tableless layout";
$GLOBALS['TL_LANG']['tl_module']['cm_addresslist_tableless']['1']="If the check box is activated, the list display is free of tables. The format you can then specify with css";

$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_fieldslist']['0'] = "Fields of the list / table";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_fieldslist']['1'] = "You determine which fields are listed in the table and determine the order";
$GLOBALS['TL_LANG']['tl_module']['cm_shownumberofadresses']['0']='Show number';
$GLOBALS['TL_LANG']['tl_module']['cm_shownumberofadresses']['1']='Show the number of found results';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_hidedetaillink']['0']="Hide link on detail page";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_hidedetaillink']['1']="Hide link on detail page";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_hidedetaillink']['0']="Hide link on detail page";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_hidedetaillink']['01']="Hide link on detail page";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_linktowebsite']['0']="Link to Website";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_linktowebsite']['1']="Enter the name of the members website as a link";
$GLOBALS['TL_LANG']['tl_module']['cm_map_routetotable']['0'] = "Addc a column with a link to the route planning";
$GLOBALS['TL_LANG']['tl_module']['cm_map_routetotable']['1'] = "Select the check box to add a column with a link to the Google route planning";

$GLOBALS['TL_LANG']['tl_module']['cm_map_linktowebsite']['0']="Link to Website";
$GLOBALS['TL_LANG']['tl_module']['cm_map_linktowebsite']['1']="Enter the name of the members website as a link";
$GLOBALS['TL_LANG']['tl_module']['cm_map_staybubbleopened']['0']="Leave open bubble";
$GLOBALS['TL_LANG']['tl_module']['cm_map_staybubbleopened']['1']="After clicking on the speech bubble it remains open until another marker is clicked or the bubble is closed via the close box";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_asc']='Ascending';
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_desc']='Descending';

$GLOBALS['TL_LANG']['tl_module']['cm_map_maptypelist']['0'] = "Map type";
$GLOBALS['TL_LANG']['tl_module']['cm_map_maptypelist']['1'] = "Select the card type for the list view";
$GLOBALS['TL_LANG']['tl_module']['cm_map_disablewheellist']['0'] = "Disable mouse wheel (zoom)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_disablewheellist']['1'] = "select the check box to prevent changing the zoom factor by the mouse wheel .";

$GLOBALS['TL_LANG']['tl_module']['cm_map_choosetypelist']['0'] = "Show Map Type Panel";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosetypelist']['1'] = "If the check box is activated, a panel for selecting the card type is displayed in the list view";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrltypelist']['0'] = "Form of card-type panels";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrltypelist']['1'] = "Choose the shape of the card-type panels (no selection = automatic)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosenavlist']['0'] = "Show the navigation panel";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosenavlist']['1'] = "If the check box is activated, a panel appears to navigate the list view";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlnavlist']['0'] = "Shape of the navigation panel";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlnavlist']['1'] = "Choose the shape of the navigation panel (no selection = automatic)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosezoomlist']['0'] = "Show the zoom panel";
$GLOBALS['TL_LANG']['tl_module']['cm_map_choosezoomlist']['1'] = "If the check box is activated, a panel appears to change the zoom factor.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlzoomlist']['0'] = "Shape of the zoom panel";
$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlzoomlist']['1'] = "Choose the shape of the zoom panel (no selection = automatic)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_routetolist']['0'] = "Display the route planning form";
$GLOBALS['TL_LANG']['tl_module']['cm_map_routetolist']['1'] = "Select the check box to display a route planning form";

$GLOBALS['TL_LANG']['tl_module']['cm_map_zoomsm']='small';
$GLOBALS['TL_LANG']['tl_module']['cm_map_zoomlg']='large';
$GLOBALS['TL_LANG']['tl_module']['cm_map_zoomdfld']='default';

$GLOBALS['TL_LANG']['tl_module']['cm_map_stdzoom']['0'] = "Default zoom factor";
$GLOBALS['TL_LANG']['tl_module']['cm_map_stdzoom']['1'] = "Select the zoom factor to be used, if no individual factor is set for the Member";
$GLOBALS['TL_LANG']['tl_module']['cm_map_indivcenterlist']['0'] = "Individual Arrangement";
$GLOBALS['TL_LANG']['tl_module']['cm_map_indivcenterlist']['1'] = "Select this check box if you want to set the zoom factor and the center of the map manually.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_zoomlist']['0'] = "Zoom factor";
$GLOBALS['TL_LANG']['tl_module']['cm_map_zoomlist']['1'] = "Set the zoom factor for the map in the fixed list view";
$GLOBALS['TL_LANG']['tl_module']['cm_map_centerlist']['0'] = "Map Centre";
$GLOBALS['TL_LANG']['tl_module']['cm_map_centerlist']['1'] = "Enter the coordinates of the map center in the list view.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_zoompos']['0'] = "Zoom factor";
$GLOBALS['TL_LANG']['tl_module']['cm_map_zoompos']['1'] = "Set the zoom factor for the map in the fixed list view";
$GLOBALS['TL_LANG']['tl_module']['cm_map_indivcenterpos']['0'] = "Individual Map Centre";
$GLOBALS['TL_LANG']['tl_module']['cm_map_indivcenterpos']['1'] = "Select the check box to define a custom card center";
$GLOBALS['TL_LANG']['tl_module']['cm_map_centerpos']['0'] = "Map Centre";
$GLOBALS['TL_LANG']['tl_module']['cm_map_centerpos']['1'] = "Enter the coordinates of the map center in the list view";

$GLOBALS['TL_LANG']['tl_module']['cm_map_viewnorm'] = "Map";
$GLOBALS['TL_LANG']['tl_module']['cm_map_viewsat'] = "Satellite view";
$GLOBALS['TL_LANG']['tl_module']['cm_map_viewhyb'] = "Satellite View and labels";
$GLOBALS['TL_LANG']['tl_module']['cm_map_viewphys'] = "Grounds";

$GLOBALS['TL_LANG']['tl_module']['cm_map_typhor_bar'] = "Horizontal";
$GLOBALS['TL_LANG']['tl_module']['cm_map_typdpdwn_menu'] = "DropDown";
$GLOBALS['TL_LANG']['tl_module']['cm_map_typdfld'] = "Standard";

$GLOBALS['TL_LANG']['tl_module']['cm_map_navsm'] = "SMALL";
$GLOBALS['TL_LANG']['tl_module']['cm_map_navzo'] = "ZOOM_PAN";
$GLOBALS['TL_LANG']['tl_module']['cm_map_navand'] = "ANDROID";
$GLOBALS['TL_LANG']['tl_module']['cm_map_navdfld'] = "DEFAULT";

$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_member']['0'] = "Member";
$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_member']['1'] = "Select the member whose position will be the address shown on the map.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_infotextdetail']['0'] = "Balloon text";
$GLOBALS['TL_LANG']['tl_module']['cm_map_infotextdetail']['1'] = "Customize the text that will appear in the bubble";
$GLOBALS['TL_LANG']['tl_module']['cm_map_route'] = "Route planning";
$GLOBALS['TL_LANG']['tl_module']['cm_map_routetodetail']['0'] = "Display the route planning form";
$GLOBALS['TL_LANG']['tl_module']['cm_map_routetodetail']['1'] = "Select the check box to display a route planning form";
$GLOBALS['TL_LANG']['tl_module']['cm_map_infoShowOnload']['0'] = "Show balloon immediately";
$GLOBALS['TL_LANG']['tl_module']['cm_map_infoShowOnload']['1'] = "Select the check box if the balloon is to be displayed immediately when the page loads";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_pg']['0'] = "Search result page";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_pg']['1'] = "Select the page on which the module 'Memberlist with Google Map' is integrated.";

$GLOBALS['TL_LANG']['tl_module']['cm_map_country']['0'] = "Country (default)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_country']['1'] = "Default for the country input field / country selection";

$GLOBALS['TL_LANG']['tl_module']['cm_map_country_as_select']['0'] = "Countries as a field selection";
$GLOBALS['TL_LANG']['tl_module']['cm_map_country_as_select']['1'] = "Provides the Country field as a selection rather than as an input field";

$GLOBALS['TL_LANG']['tl_module']['cm_map_distance_as_select']['0'] = "Distance field as selection";
$GLOBALS['TL_LANG']['tl_module']['cm_map_distance_as_select']['1'] = "Provides the distance field as a choice rather than as an input field";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_fieldsearch']['0'] = "Search in specific fields";
$GLOBALS['TL_LANG']['tl_module']['cm_map_distance_as_select']['1'] = "Provides a list to select the field, and an input field for the search term";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_plzsearch']['0'] = "Zip Code search";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_plzsearch']['1'] = "Provides a search box for entering the first few digits of a zip code";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_plznumberdigits']['0'] = "Number of digits";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_plznumberdigits']['1'] = "Number of digits that must be entered at least";

$GLOBALS['TL_LANG']['tl_module']['cm_map_clicktype']['0'] = "Bubble / Detail";
$GLOBALS['TL_LANG']['tl_module']['cm_map_clicktype']['1'] = "Action to see the bubble or open the detail view";

$GLOBALS['TL_LANG']['tl_module']['cm_mouseover_click'] = "Show / click";
$GLOBALS['TL_LANG']['tl_module']['cm_click_dblclick'] = "Double / Click";
$GLOBALS['TL_LANG']['tl_module']['cm_map_empty_legend'] = "Settings on empty search result";
$GLOBALS['TL_LANG']['tl_module']['cm_map_showmaponempty']['0'] = "Show map";
$GLOBALS['TL_LANG']['tl_module']['cm_map_showmaponempty']['1'] = "Select the check box, if you want to show a map although the search result is empty.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_zoomempty']['0'] = "Zoom factor";
$GLOBALS['TL_LANG']['tl_module']['cm_map_zoomempty']['1'] = "Set the zoom factor of the default map.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_centerempty']['0'] = "Map center";
$GLOBALS['TL_LANG']['tl_module']['cm_map_centerempty']['1'] = "Set the map center of the default map.";

$GLOBALS['TL_LANG']['tl_module']['cm_memberdetail_pg']['0'] = "Page of the detail view";
$GLOBALS['TL_LANG']['tl_module']['cm_memberdetail_pg']['1'] = "Select the detail page.";

$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_pg']['0'] = "Page of the Listview";
$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_pg']['1'] = "Select the member list page.";

$GLOBALS['TL_LANG']['tl_module']['cm_register_showonmap']['0'] = "Activat the map view.";
$GLOBALS['TL_LANG']['tl_module']['cm_register_showonmap']['1'] = "Select the check box if you want ta activate the map view automativally on registration.";
$GLOBALS['TL_LANG']['tl_module']['cm_register_autocoord']['0'] = "Automatically determine coordinates";
$GLOBALS['TL_LANG']['tl_module']['cm_register_autocoord']['1'] = "Select the check box if the coordinates should be determined automativally by the adress data on registration.";

$GLOBALS['TL_LANG']['tl_module']['templatelst_legend']= "Templates of the list view";
$GLOBALS['TL_LANG']['tl_module']['templatedtl_legend']= "Templates of the detail view";
$GLOBALS['TL_LANG']['tl_module']['map_lsttemplate']['0']="Main termplate";
$GLOBALS['TL_LANG']['tl_module']['map_lsttemplate']['1']="Main termplate of the list view";
$GLOBALS['TL_LANG']['tl_module']['map_tbltemplate']['0']="Template of the list ";
$GLOBALS['TL_LANG']['tl_module']['map_tbltemplate']['1']="Template of the member list (table or tableless)";
$GLOBALS['TL_LANG']['tl_module']['map_inftemplate']['0']="Info template";
$GLOBALS['TL_LANG']['tl_module']['map_inftemplate']['1']="Template of the bubble";
$GLOBALS['TL_LANG']['tl_module']['map_dtltemplate']['0']="Main termplate";
$GLOBALS['TL_LANG']['tl_module']['map_dtltemplate']['1']="Main termplate of the detail view";
$GLOBALS['TL_LANG']['tl_module']['map_infdtltemplate']['0']="Info template";
$GLOBALS['TL_LANG']['tl_module']['map_infdtltemplate']['1']="Template of the bubble";

$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster']['0']="Cluster Markers";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster']['1']="Cluster Markers which are closed together";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_gridsize']['0']="Cluster size";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_gridsize']['1']="Enlarge this value will cluster more markers.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_maxzoom']['0']="max. zoom factor";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_maxzoom']['1']="markers will bes clustered up to this zoom factor only.";
$GLOBALS['TL_LANG']['tl_module']['cm_email_nameRequired']['0']="The name is required";
$GLOBALS['TL_LANG']['tl_module']['cm_email_nameRequired']['1']="If the form to send an email ist visible, the user must fill out a name field to send a mail to the selected member.";

?>
