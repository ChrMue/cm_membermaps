<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');
/**
 * TL_ROOT/system/modules/cm_membergooglemaps/languages/en/modules.php
 * 
 * Contao extension: cm_membergooglemaps
 * 
 * Copyright : &copy; 2013 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Dave Doyle 
 * 
 */
 
$GLOBALS['TL_LANG']['MOD']['cm_membergooglemaps']['0'] = "Memberlist with Google Maps";
$GLOBALS['TL_LANG']['MOD']['cm_membergooglemaps']['1'] = "With this module you can use the member table in the frontend with a map detail (Google Map) View.";
$GLOBALS['TL_LANG']['MOD']['cm_membergooglemapspos']['0'] = "Googlemap a member";
$GLOBALS['TL_LANG']['MOD']['cm_membergooglemapspos']['1'] = "With this module you can put in front of a Member's position in a map section (Google Map) display as Where to find us...";
$GLOBALS['TL_LANG']['FMD']['cm_membergooglemaps']['0'] = "Members with Google Maps";
$GLOBALS['TL_LANG']['FMD']['cm_membergooglemaps']['1'] = "Use this module to display information to the members of the location in a map section (Google Map).";
$GLOBALS['TL_LANG']['FMD']['cm_membergooglemapspos']['0'] = "Googlemap a member";
$GLOBALS['TL_LANG']['FMD']['cm_membergooglemapspos']['1'] = "Use this module to view a member's position in a map section (Google Map) eg Where to find us...";

$GLOBALS['TL_LANG']['FMD']['cm_membergooglemapsList'][0] = "List view - Memberlist with Google Maps";
$GLOBALS['TL_LANG']['FMD']['cm_membergooglemapsList'][1] = "Mit diesem Modul können Sie die Mitgliedertabelle im Frontend mit einem Kartenausschnitt (Google Map) anzeigen.";
$GLOBALS['TL_LANG']['FMD']['cm_membergooglemapsReader'][0] = "Detail view (Reader) - Map a member";
$GLOBALS['TL_LANG']['FMD']['cm_membergooglemapsReader'][1] = "";

$GLOBALS['TL_LANG']['MOD']['cm_mapstyles']['0'] = "Board Layout";
$GLOBALS['TL_LANG']['MOD']['cm_mapstyles']['1'] = "Define custom card layouts for use in the expansion cm_membermaps.";
 
?>
