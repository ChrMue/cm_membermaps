<?php
/**
 * Contao extension: cm_membermaps
 * 
 * Copyright : &copy; 2021 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Dave Doyle and CM
 * 
 */
 
$GLOBALS['TL_LANG']['MSC']['cm_map_error'] = ">>> A map View is currently not possible. <<<";
$GLOBALS['TL_LANG']['MSC']['list_perPage'] = "Results per page";
$GLOBALS['TL_LANG']['MSC']['cm_map_getroute'] = "Get directions";
$GLOBALS['TL_LANG']['MSC']['cm_map_getroutetable'] = "Direction";
$GLOBALS['TL_LANG']['MSC']['cm_map_getroutelist'] = "Get directions";
$GLOBALS['TL_LANG']['MSC']['cm_map_topos'] = "Here";
$GLOBALS['TL_LANG']['MSC']['cm_map_frompos'] = "From here";
$GLOBALS['TL_LANG']['MSC']['cm_map_fromaddr'] = "Start address";
$GLOBALS['TL_LANG']['MSC']['cm_map_toaddr'] = "Destination address";
$GLOBALS['TL_LANG']['MSC']['cm_radius_search'] = "Browse nearby";
$GLOBALS['TL_LANG']['MSC']['cm_lbl_location'] = "Address:";
$GLOBALS['TL_LANG']['MSC']['cm_lbl_country'] = "Country:";
$GLOBALS['TL_LANG']['MSC']['cm_lbl_max_dist'] = "Distance:";
$GLOBALS['TL_LANG']['MSC']['cm_lbl_max_dist_drdn'] = "Select radius:";
$GLOBALS['TL_LANG']['MSC']['cm_distitem'] = "km";
$GLOBALS['TL_LANG']['MSC']['cm_distsearch_label'] = "Search";
$GLOBALS['TL_LANG']['MSC']['cm_distance'] = "km";
$GLOBALS['TL_LANG']['MSC']['cm_email'] = "E-Mail:";
$GLOBALS['TL_LANG']['MSC']['cm_captcha'] = "Spam protection:";
$GLOBALS['TL_LANG']['MSC']['cm_website'] = "Website:";
$GLOBALS['TL_LANG']['MSC']['plzarea'] = "Postal code area:";
$GLOBALS['TL_LANG']['MSC']['cm_plz_search'] = "Postal code search";
$GLOBALS['TL_LANG']['MSC']['coordspicker'] ="Select a position";
$GLOBALS['TL_LANG']['MSC']['cm_mapLng'] = "Longitude";
$GLOBALS['TL_LANG']['MSC']['cm_mapLat'] = "Latitude";
$GLOBALS['TL_LANG']['MSC']['cm_map_visitorlocation'] = "Location";
$GLOBALS['TL_LANG']['MSC']['cm_name']="Your name";
$GLOBALS['TL_LANG']['MSC']['cm_numberofmembers']='%s members found';

