<?php
/**
 * TL_ROOT/system/modules/cm_membergooglemaps/languages/en/tl_settings.php 
 * 
 * Contao extension: cm_membergooglemaps
 * 
 * Copyright : &copy; 2010 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Dave Doyle 
 * 
 */
 
$GLOBALS['TL_LANG']['tl_settings']['cm_membergooglemaps_ml'] = "Memberlist with Google maps";
$GLOBALS['TL_LANG']['tl_settings']['cm_membergooglemaps_key']['0'] = "Google API Key";
$GLOBALS['TL_LANG']['tl_settings']['cm_membergooglemaps_key']['1'] = "Please enter the key you have received by Google <a href=\"http://code.google.com/apis/maps/signup.html\">http://code.google.com/apis/maps/signup.html</a>";
 
?>
