<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');
/**
 * TL_ROOT/system/modules/cm_membergooglemaps/languages/en/tl_member.php 
 * 
 * Contao extension: cm_membergooglemaps
 * 
 * Copyright : &copy; 2013 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Dave Doyle
 * 
 */
 
$GLOBALS['TL_LANG']['tl_member']['cm_coords']['0'] = "Coordinates of the marker";
$GLOBALS['TL_LANG']['tl_member']['cm_coords']['1'] = "If the check box is enabled when you save, then the coordinates are calculated automatically, otherwise you can edit the coordinates manually.";
$GLOBALS['TL_LANG']['tl_member']['cm_autocoords']['0'] = "Automatically determine coordinates";
$GLOBALS['TL_LANG']['tl_member']['cm_autocoords']['1'] = "If the check box is activated, the coordinates are calculated automatically when you save.";
$GLOBALS['TL_LANG']['tl_member']['cm_lat']['0'] = "Latitude (degrees, decimal)";
$GLOBALS['TL_LANG']['tl_member']['cm_lat']['1'] = "If the check box is enabled when you save, the coordinates are calculated automatically, otherwise you can edit the coordinates manually.";
$GLOBALS['TL_LANG']['tl_member']['cm_lng']['0'] = "Longitude (degrees, decimal)";
$GLOBALS['TL_LANG']['tl_member']['cm_lng']['1'] = "If the check box is enabled when you save, the coordinates are calculated automatically, otherwise you can edit the coordinates manually.";
$GLOBALS['TL_LANG']['tl_member']['cm_gc_requestcount'][0] = "Count of tries to determine the cordinates";
$GLOBALS['TL_LANG']['tl_member']['cm_gc_requestcount'][1] = "Count of Google requests to determine the cordinates - will be reset on saving a member settings.";
$GLOBALS['TL_LANG']['tl_member']['cm_membergooglemaps_legend'] = "Map display";
$GLOBALS['TL_LANG']['tl_member']['cm_allowmap']['0'] = "Allow map display";
$GLOBALS['TL_LANG']['tl_member']['cm_map_indivcenter']['0'] = "Individual Map Centre";
$GLOBALS['TL_LANG']['tl_member']['cm_map_indivcenter']['1'] = "Check the festival to an individual center of the map set";
$GLOBALS['TL_LANG']['tl_member']['cm_map_indivzoom']['0'] = "Individual cards enlargement";
$GLOBALS['TL_LANG']['tl_member']['cm_map_indivzoom']['1'] = "Select the check box to enlarge the map to define individual";
$GLOBALS['TL_LANG']['tl_member']['cm_map_center']['0'] = "Map Centre";
$GLOBALS['TL_LANG']['tl_member']['cm_map_center']['1'] = "Enter the coordinates for the center of the map to individualy fixed";
$GLOBALS['TL_LANG']['tl_member']['cm_map_zoom']['0'] = "Enlarge the map";
$GLOBALS['TL_LANG']['tl_member']['cm_map_zoom']['1'] = "Set the zoom factor for the map display to fixed"; 

$GLOBALS['TL_LANG']['tl_member']['alias_legend'] = "Alias";
$GLOBALS['TL_LANG']['tl_member']['alias']['0'] = "Member alias";
$GLOBALS['TL_LANG']['tl_member']['alias']['1'] = "The member alias is a unique reference, which can be used instead of the member id. If you do not enter an alias it will be generated ba the fisrtname-lastname-city. if thse city field is empty, the concerning part is missing."; 

$GLOBALS['TL_LANG']['tl_member']['updCoords'] = "Complete missing coordinates";

?>