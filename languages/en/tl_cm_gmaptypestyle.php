<?php
/**
 * TL_ROOT/system/modules/cm_membergooglemaps/languages/en/tl_cm_gmaptypestyle.php
 * 
 * Contao extension: cm_membergooglemaps
 * 
 * Copyright : &copy; 2013 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: DAve Doyle 
 * 
 */
 
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['administrative'] = 'Management Areas';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['administrative.country'] = 'Countries';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['administrative.land_parcel'] = 'Country Land';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['administrative.locality'] = 'Places';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['administrative.neighborhood'] = 'Vicinities / Suburb';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['administrative.province'] = 'States / Provinces';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all'] = 'All selection types';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['landscape'] = 'Landscapes';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['landscape.man_made'] = 'Man-made Structures';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['landscape.natural'] = 'Natural Elements';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi'] = 'Interesting Places';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi.attraction'] = 'Tourist Attractions';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi.business'] = 'Business';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi.government'] = 'Government Buildings';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi.medical'] = 'Emergency Services (hospitals, pharmacies, police, doctors)';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi.park'] = 'Parks';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi.place_of_worship'] = 'Places of Worship';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi.school'] = 'Schools and other educational institutions';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['poi.sports_complex'] = 'Sports Complex';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['road'] = 'All Roads';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['road.arterial'] = 'Roads with high traffic';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['road.highway'] = 'Highways';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['road.local'] = 'Local Roads';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['transit'] = 'All stations and public transport lines';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['transit.line'] = 'Lines of public transport';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['transit.station'] = 'All public transport stations';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['transit.station.airport'] = 'Airports';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['transit.station.bus'] = 'Bus stations';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['transit.station.rail'] = 'stations';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['water'] = 'Waters';

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_setvisibility'] = array('Set Visibility','Activate this check box to define the visibility of the element');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_invert_lightness'] = array('Invert Brightness','Activate this check box to reverse the brightness of the element');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_sethue'] = array('Specify Color','Activate this check box to set the color of the element');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_setsaturation'] = array('Set Saturation','Activate this check box to define the saturation of the element');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_setlightness'] = array('Set Brightness','Activate this check box to define the brightness of the element');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_setgamma'] = array('Set gamma value','Activate this check box to set the gamma value of the element');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_setvisibility'] = array('Set visibility','Activate this check box to set the visibility of the element');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_invert_lightness'] = array('Invert brightness','Activate this check box to reverse the brightness of the element');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_sethue'] = array('Specify color','Activate this check box to set the color of the geometry');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_setsaturation'] = array('Set saturation','Activate this check box to define the geometry of the saturation');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_setlightness'] = array('Set brightness','Activate this check box to define the brightness of the geometry');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_setgamma'] = array('Set gamma value','Activate this check box to set the gamma value of the geometry');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_setvisibility'] = array('Set visibility','Activate this check box to define the visibility of the label');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_invert_lightness'] = array('Invert brightness','Activate this check box to reverse the brightness of the element');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_sethue'] = array('Specify color','Activate this check box to set the color of the label');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_setsaturation'] = array('Set saturation','Activate this check box to define the saturation of the label');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_setlightness'] = array('Set brightness','Activate this check box to define the brightness of the label');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_setgamma'] = array('Set gamma value','Activate this check box to set the gamma value of the label');

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_visibility'] = array('Visibility','Select the desired entry.');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_hue'] = array('Hue','Choose a color from.');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_saturation'] = array('Saturation','Enter the saturation in the range of -100 to 100');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_lightness'] = array('Brightness','Enter the brightness in the range -100 through 100');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_gamma'] = array('Gamma','Enter the desired gamma value');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_visibility'] = array('Visibility','Select the desired entry');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_hue'] = array('Hue','Choose a color');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_saturation'] = array('Saturation','Enter the saturation in the range -100 through 100');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_lightness'] = array('Brightness','Enter the brightness in the range -100 through 100');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_gamma'] = array('Gamma','Enter the desired gamma value');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_visibility'] = array('Visibility','Select the desired entry');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_hue'] = array('Hue','Choose a color');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_saturation'] = array('Saturation','Enter the saturation in the range -100 through 100');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_lightness'] = array('Brightness','Enter the brightness in the range -100 through 100');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_gamma'] = array('Gamma','Enter the desired gamma value');

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['feature'] = array('Map Element','Select the map object that you want to format');

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['cm_all_legend'] = 'Entire Element';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['cm_gty_legend'] = 'Geometry (Presentation)';
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['cm_lbl_legend'] = 'Label';

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['new']  = array('New style definition', 'Create a new style definition'); 
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['edit'] = array('Edit style definition', 'Style definition ID %s to work on');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['editheader'] =array('Edit style definition', 'Edit the style definition settings');

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['copy'] = array('Duplicate style definition', 'Style definition, duplicate ID %s');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['cut'] = array('Move style definition', 'Style Definition move ID %s');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['show'] = array('Style definition details','Details of the Style Definition ID %s');

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['delete']     = array('Delete style definition', 'Style definition ID %s delete');

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['pasteafter'] = array('Insert Above', 'Insert after the style definition ID %s');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['pastenew']   = array('Create a new style definition above', 'Create a new element to the style definition ID %s');
$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['toggle']     = array('Change visibility', 'ID %s to change the visibility of the style definition');

$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['mapelement_legend'] = 'Map Element';

?>
