<?php

/**
 * Dynamically add the permission check and parent table
 */
if (Input::get('do') == 'member')
{
	$GLOBALS['TL_DCA']['tl_content']['config']['ptable'] = 'tl_member';
	$GLOBALS['TL_DCA']['tl_content']['config']['onload_callback'][] = array('tl_content_member', 'checkPermission');
        $GLOBALS['TL_DCA']['tl_content']['list']['sorting']['headerFields'] = array('lastname','firstname','postal','city','email');
}


/**
 * Provide miscellaneous methods that are used by the data configuration array.
 *
 * @author Leo Feyer <https://github.com/leofeyer>
 */
class tl_content_member extends Backend
{

	/**
	 * Import the back end user object
	 */
	public function __construct()
	{
		parent::__construct();
		$this->import('BackendUser', 'User');
	}


	/**
	 * Check permissions to edit table tl_content
	 */
	public function checkPermission()
	{
		if ($this->User->isAdmin)
		{
			return;
		}

		// Set the root IDs
		if (empty($this->User->member) || !is_array($this->User->member))
		{
			$root = array(0);
		}
		else
		{
			$root = $this->User->member;
		}

		// Check the current action
		switch (Input::get('act'))
		{
			case '': // empty
			case 'paste':
			case 'create':
			case 'select':
				// Check access to the news item
			    $this->checkAccessToElement(CURRENT_ID, $root, true);
// 				if (!$this->checkAccessToElement(CURRENT_ID, $root, true))
// 				{
// 					$this->redirect('contao/main.php?act=error');
// 				}
				break;

			case 'editAll':
			case 'deleteAll':
			case 'overrideAll':
			case 'cutAll':
			case 'copyAll':
				// Check access to the parent element if a content element is moved
				if (Input::get('act') == 'cutAll' || Input::get('act') == 'copyAll') 
				{
				    $this->checkAccessToElement(Input::get('pid'), $root, (Input::get('mode') == 2));
				}
// 				&& !$this->checkAccessToElement(Input::get('pid'), $root, (Input::get('mode') == 2)))
// 				{
// 					$this->redirect('contao/main.php?act=error');
// 				}

				$objCes = Database::getInstance()->prepare("SELECT id FROM tl_content WHERE ptable='tl_member' AND pid=?")
										 ->execute(CURRENT_ID);

				$session = Session::getData();
				$session['CURRENT']['IDS'] = array_intersect((array) $session['CURRENT']['IDS'], $objCes->fetchEach('id'));
				Session::setData($session);
				break;

			case 'cut':
			case 'copy':
				// Check access to the parent element if a content element is moved
				$this->checkAccessToElement(Input::get('pid'), $root, (Input::get('mode') == 2));
				// 				if (!$this->checkAccessToElement(Input::get('pid'), $root, (Input::get('mode') == 2)))
// 				{
// 					$this->redirect('contao/main.php?act=error');
// 				}
				// NO BREAK STATEMENT HERE

			default:
				// Check access to the content element
			    $this->checkAccessToElement(Input::get('id'), $root);
// 			    if (!$this->checkAccessToElement(Input::get('id'), $root))
// 				{
// 					$this->redirect('contao/main.php?act=error');
// 				}
				break;
		}
	}


	/**
	 * Check access to a particular content element
	 *
	 * @param integer $id
	 * @param array   $root
	 * @param boolean $blnIsPid
	 *
	 * @return boolean
	 */
	protected function checkAccessToElement($id, $root, $blnIsPid=false)
	{
		if ($blnIsPid)
		{
			$objMember = $this->Database->prepare("SELECT n.id AS nid FROM tl_member n WHERE n.id=?")
										  ->limit(1)
										  ->execute($id);
		}
		else
		{
			$objMember = $this->Database->prepare("SELECT n.id AS nid FROM tl_content c, tl_member n WHERE c.id=? AND c.pid=n.id")
										  ->limit(1)
										  ->execute($id);
		}

		// Invalid ID
		if ($objMember->numRows < 1)
		{
		    $this->log('Invalid member content element ID ' . $id, __METHOD__, TL_ERROR);

			return false;
		}

		return true;
	}


}
