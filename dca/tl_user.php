<?php

/*
 * This file is part of Contao.
 *
 * (c) Leo Feyer
 *
 * @license LGPL-3.0-or-later
 */

// Extend the default palettes
$GLOBALS['TL_DCA']['tl_user']['palettes']['extend'] = str_replace('fop;', 'fop;{member_legend},member;', $GLOBALS['TL_DCA']['tl_user']['palettes']['extend']);
$GLOBALS['TL_DCA']['tl_user']['palettes']['custom'] = str_replace('fop;', 'fop;{member_legend},member;', $GLOBALS['TL_DCA']['tl_user']['palettes']['custom']);


/**
 * Add fields to tl_user_group
 */
// Add fields to tl_user_group
$GLOBALS['TL_DCA']['tl_user']['fields']['member'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_user']['member'],
	'exclude'                 => true,
	'inputType'               => 'checkbox',
	'foreignKey'              => 'tl_member.title',
	'eval'                    => array('multiple'=>true),
	'sql'                     => "blob NULL"
);
