<?php

/**
 * PHP version 5
 * @copyright  Christian Muenster 2020 
 * @author     Christian Muenster 
 * @package    CM_MemberMaps
 * @license    LGPL 
 */

/**
 * Add palettes to tl_member
 */
$GLOBALS['TL_DCA']['tl_member']['config']['ctable'] = array('tl_content'); 

$GLOBALS['TL_DCA']['tl_member']['list']['operations']['edit'] = array
(
	'label'               => &$GLOBALS['TL_LANG']['tl_member']['edit'],
	'href'                => 'table=tl_content',
	'icon'                => 'edit.gif'
);

$GLOBALS['TL_DCA']['tl_member']['list']['operations']['editheader'] = array
(
	'label'               => &$GLOBALS['TL_LANG']['tl_member']['editmeta'],
	'href'                => 'act=edit',
	'icon'                => 'header.gif'
);

$GLOBALS['TL_DCA']['tl_member']['config']['sql']['keys']['alias'] ='index';
 
$GLOBALS['TL_DCA']['tl_member']['list']['label']['label_callback']=array('tl_cm_memberlist','addGMstatus');

array_insert($GLOBALS['TL_DCA']['tl_member']['list']['label']['fields'], 1, ($GLOBALS['TL_LANG']['tl_member']['cm_gmstatus']??'status'));

$GLOBALS['TL_DCA']['tl_member']['list']['global_operations']['updCoords'] = array
			(
        'label'               => &$GLOBALS['TL_LANG']['tl_member']['updCoords'],
        'href'                => 'key=updCoords',
        'class'               => 'header_updCoords'
	);

$GLOBALS['TL_DCA']['tl_member']['palettes']['default']=
str_replace(';{homedir_legend',
   ';{alias_legend},alias;{homedir_legend'
,$GLOBALS['TL_DCA']['tl_member']['palettes']['default']);

$GLOBALS['TL_DCA']['tl_member']['palettes']['default']=
str_replace(';{homedir_legend',  
   ';{cm_membermaps_legend},cm_allowmap,'
  .'cm_autocoords,cm_gc_requestcount,cm_coords,'
  .'cm_lat,cm_lng,'
  .'cm_map_indivcenter,cm_map_indivzoom;{homedir_legend'
,$GLOBALS['TL_DCA']['tl_member']['palettes']['default']);
  
$GLOBALS['TL_DCA']['tl_member']['palettes']['__selector__'][]  = 
  'cm_map_indivcenter';
$GLOBALS['TL_DCA']['tl_member']['palettes']['__selector__'][]  = 
  'cm_map_indivzoom';

$GLOBALS['TL_DCA']['tl_member']['subpalettes']['cm_map_indivcenter'] =
     'cm_map_center';
$GLOBALS['TL_DCA']['tl_member']['subpalettes']['cm_map_indivzoom'] =
     'cm_map_zoom';


if (TL_MODE != 'BE') {
    $GLOBALS['TL_DCA']['tl_member']['config']['onsubmit_callback'][] = array('cm_MemberMaps\memberHelper', 'getGeoDataFE');
    $GLOBALS['TL_DCA']['tl_member']['config']['onsubmit_callback'][] = array('cm_MemberMaps\memberHelper', 'resetCounterFE');
}

/**
 * Add fields to tl_member
 */
$GLOBALS['TL_DCA']['tl_member']['fields']['alias'] = array
(
		'label'         => &$GLOBALS['TL_LANG']['tl_member']['alias'],
		'exclude'       => true,
		'search'        => true,
		'inputType'     => 'text',
		'eval'          => array('rgxp'=>'alias', 'unique'=>true, 'maxlength'=>128, 'tl_class'=>'w50'),
		'save_callback' => array
		(
			array('tl_cm_memberlist', 'generateAlias')
		),
		'sql'           => "varchar(255) BINARY NOT NULL default ''"
); 
$GLOBALS['TL_DCA']['tl_member']['fields']['cm_allowmap'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_allowmap'],
	'inputType'          => 'checkbox',
	'search'             => false,
	'eval'               => array('mandatory'=>false, 'feEditable'=>true, 
                                'feViewable'=>false, 'feGroup'=>'contact'),
	'sql'				=> "char(1) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_member']['fields']['cm_autocoords'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_autocoords'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'feEditable'=>true, 
                                'feViewable'=>false, 'feGroup'=>'contact',
                                'tl_class'=>'w50'),
	'sql'				=> "char(1) NOT NULL default '1'"
);
$GLOBALS['TL_DCA']['tl_member']['fields']['cm_gc_requestcount'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_gc_requestcount'],
	'inputType'          => 'text',
	'eval'               => array('mandatory'=>false, 'feEditable'=>false, 
                                'feViewable'=>false,'rgxp'=>'natural', 
                                'readonly'=>true,'disabled'=>false,'tl_class'=>'w50'), 
	'search'             => false,
	'save_callback' => array
						(
							array('cm_MemberMaps\memberHelper', 'resetCounterBE')
						),
	'sql'				=> "int(10) NOT NULL default '0'"
);
$GLOBALS['TL_DCA']['tl_member']['fields']['cm_coords'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_coords'],
	'inputType'          => 'cm_LatLng', //'text',
	'search'             => false,
	'eval'               => array('mandatory'=>false, 'feEditable'=>true, 
                                'feViewable'=>true, 'feGroup'=>'contact',
                                'tl_class'=>'w50', 'maxlength'=>64,'rgxp'=>'geocoords'),
	'save_callback' => array
						(
							array('cm_MemberMaps\memberHelper', 'getGeoDataBE')
						),
	'sql'				=> "varchar(64) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member']['fields']['cm_lat'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_lat'],
	'inputType'          => 'text',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'feEditable'=>true,
                                'feViewable'=>true, 'feGroup'=>'contact',
                                'tl_class'=>'w50 clr', 'maxlength'=>32,
                                'readonly'=>true,'disabled'=>true),
	'sql'				=> "varchar(32) NULL default ''"
);
$GLOBALS['TL_DCA']['tl_member']['fields']['cm_lng'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_lng'],
	'inputType'          => 'text',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'feEditable'=>true,
                                'feViewable'=>true, 'feGroup'=>'contact',
                                'tl_class'=>'w50', 'maxlength'=>32,
                                'readonly'=>true,'disabled'=>true ),
	'sql'				=> "varchar(32) NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member']['fields']['cm_map_indivcenter'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_map_indivcenter'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'feEditable'=>true, 
                                'tl_class'=>'w50 m12 clr','feViewable'=>false, 'feGroup'=>'contact',
                                'submitOnChange'=>true),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member']['fields']['cm_map_center'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_map_center'],
	'inputType'          => 'cm_LatLng', //'text',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'feEditable'=>true, 
                                'feViewable'=>true, 'feGroup'=>'contact',
                                'maxlength'=>64,'rgxp'=>'geocoords',
                                'tl_class'=>'w50'),
	'sql'				=> "varchar(64) NOT NULL default ''"	                                
);

$GLOBALS['TL_DCA']['tl_member']['fields']['cm_map_indivzoom'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_map_indivzoom'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'feEditable'=>true, 
                                'tl_class'=>'w50 m12 clr','feViewable'=>false, 'feGroup'=>'contact',
                                'submitOnChange'=>true),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member']['fields']['cm_map_zoom'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member']['cm_map_zoom'],
	'inputType'          => 'select',
	'options'            => array
                          (
                            0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
                          ),
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'feEditable'=>true, 
                                'feViewable'=>true, 'feGroup'=>'contact',
                                'tl_class'=>'w50'),
	'sql'				=> "int(2) NOT NULL default '15'"	                                
);

$GLOBALS['TL_DCA']['tl_member']['fields']['password']['eval']['feViewable']=false;
$GLOBALS['TL_DCA']['tl_member']['fields']['login']['eval']['feViewable']=false;
$GLOBALS['TL_DCA']['tl_member']['fields']['newsletter']['eval']['feViewable']=false;
$GLOBALS['TL_DCA']['tl_member']['fields']['publicFields']['eval']['feViewable']=false;
$GLOBALS['TL_DCA']['tl_member']['fields']['allowEmail']['eval']['feViewable']=false;
$GLOBALS['TL_DCA']['tl_member']['fields']['locked']['eval']['feViewable']=false;
$GLOBALS['TL_DCA']['tl_member']['fields']['secret']['eval']['feViewable']=false;
$GLOBALS['TL_DCA']['tl_member']['fields']['session']['eval']['feViewable']=false;
$GLOBALS['TL_DCA']['tl_member']['fields']['loginAttempts']['eval']['feViewable']=false;
$GLOBALS['TL_DCA']['tl_member']['fields']['backupCodes']['eval']['feViewable']=false;
$GLOBALS['TL_DCA']['tl_member']['fields']['trustedTokenVersion']['eval']['feViewable']=false;
$GLOBALS['TL_DCA']['tl_member']['fields']['tstamp']['eval']['feViewable']=false;


class tl_cm_memberlist extends tl_member
{

	/**
	 * Auto-generate the meber alias if it has not been set yet
	 * @param mixed
	 * @param \DataContainer
	 * @return string
	 * @throws \Exception
	 */
	public function generateAlias($varValue, DataContainer $dc)
	{
		$autoAlias = false;

		// Generate alias if there is none
		if ($varValue == '')
		{
			$autoAlias = true;
			$alias = sprintf('%s-%s',$dc->activeRecord->firstname,$dc->activeRecord->lastname);
			if ($dc->activeRecord->city)
			{
			 $alias .='-'.$dc->activeRecord->city;
			}
			if (!version_compare(VERSION, '3.5', '<'))
			{
				$varValue = standardize(StringUtil::restoreBasicEntities($alias));
			}
			else {
				$varValue = standardize(String::restoreBasicEntities($alias));
			}
		}

		$objAlias = $this->Database->prepare("SELECT id FROM tl_member WHERE alias=?")
								   ->execute($varValue);

		// Check whether the news alias exists
		if ($objAlias->numRows > 1 && !$autoAlias)
		{
			throw new Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
		}

		// Add ID to alias
		if ($objAlias->numRows && $autoAlias)
		{
			$varValue .= '-' . $dc->id;
		}

		return $varValue;
	} 

	/**
	 * Return all editable fields of table tl_member
	 * @return array
	 */

	public function getViewableMemberProperties()
	{
		$return = array();

		$this->loadLanguageFile('tl_member');
		$this->loadDataContainer('tl_member');

		foreach ($GLOBALS['TL_DCA']['tl_member']['fields'] as $k=>$v)
		{
			if ($k == 'username' || $k == 'password' || $k == 'newsletter' 
            || $k == 'publicFields' || $k == 'allowEmail')
			{
				continue;
			}

			if ($v['eval']['feViewable'])
			{
				$return[$k] = $GLOBALS['TL_DCA']['tl_member']['fields'][$k]['label'][0];
			}
		}

		return $return;
	}


  /**
   * use hook createNewUser to add google map coordinates
   */
  public function createNewUser($intId, $arrData,$mod)
  {
  
	if ($mod instanceof Contao\ModuleRegistration)
	{
		$arr=array();
    	$arr['cm_autocoords']=$mod->cm_register_autocoord ? 1 : 0;
        $arrData['cm_autocoords']=$mod->cm_register_autocoord ? 1 : 0;

    	$arr['cm_allowmap']=$mod->cm_register_showonmap ? 1 : 0;
        $arrData['cm_allowmap']=$mod->cm_register_showonmap ? 1 : 0;
        $this->Database->prepare("UPDATE tl_member %s WHERE id=?")->set($arr)->execute($intId);
	}
    $arrData['id']= $intId;    
    $helper = new ChrMue\cm_MemberMaps\memberHelper();
    $coords = $helper->getGeoDataFE((object)$arrData);
  }

  private function validateCoords($coords) {
    return !($coords=="," || $coords=="");
  }

/*  
  public function listMember($row, $label, \DataContainer $xx,$htmlContent=null) {
  if (!$row["cm_autocoords"])
    $img= "flag_yellow";
   else
    $img=$this->validateCoords($row["cm_coords"])?"flag_green":"flag_red";
                                      
   $imghtml='<img class="coords" src="system/modules/cm_membergooglemaps/html/'.$img.'.png" alt="" /> ';

    if ($htmlContent==null)
      return parent::addIcon($row, $imghtml.$label);
    else
    {
      $ic=parent::addIcon($row, $imghtml.$label,$xx,$htmlContent);

      $ic[0]='<div style="float:right;padding-left:10px;">'.$imghtml.'</div>'.$ic[0];
      return $ic;
    }
  }
*/

  public function addGMstatus($row, $label, \DataContainer $dc, $args) {
      
    $args = $this->addIcon($row, $label, $dc, $args);
  
    $flags = array (
        'indiv'    => 'flag_yellow',
        'ok'       => 'flag_green',
        'failed'   => 'flag_red',
        'limited'  => 'flag_limit'
    );

        
    $img='';
    $alt='';
    $limit = \Config::get('cm_requestlimit');
    if (!$limit) $limit=3;
   
    if (!$row["cm_autocoords"])
    {
        $status= "indiv";
    }
    elseif ($this->validateCoords($row["cm_coords"]))
    {
        $status = "ok";
    }
    else
    {
        $status = ($row["cm_gc_requestcount"]>$limit)? "limited":"failed";
    }
    $alt=$status;
    $img=$flags[$status];
                                        
    $imghtml='<div><img class="coords" src="system/modules/cm_maps/assets/'.$img.'.png" alt="'.$img.'" /></div>';

    $args[1] = $imghtml;
    return $args;
  }

}
