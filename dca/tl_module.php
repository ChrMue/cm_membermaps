<?php 
/**
* Extension for the Contao Open Source CMS
 *
 * PHP version 5
 * @copyright  Christian Muenster 2009-2022 
 * @author     Christian Muenster 
 * @package    CM_MemberMaps
 * @license    LGPL 
 * @filesource based on original memberlist from Leo Feyer
 */

/**
 * Add palettes to tl_module - selectors
 */
 
$GLOBALS['TL_DCA']['tl_module']['fields']['ml_fields']['options_callback'] = array('tl_module_cm_membergooglemaps', 'getViewableMemberProperties'); 

// $GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_ondetail';
// $GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_onlist';

//$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_memberlist_addressform';
//$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_memberlist_distanceform';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_memberlist_distancesort';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_showcircle';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_showfar';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_memberlist_distanceasdropdown';
//$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_autozoomlist';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_indivcenterlist';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_showmaponempty';

$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_membergooglemaps_hidetable';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_indivcenterpos';
//$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_memberlist_multifieldsearch';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_memberlist_fieldsearch';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_memberlist_plzsearch';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_country_as_select';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_setstylelist';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_setstyledetail';

$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_choosetypedetail';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_choosezoomdetail';


$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_choosetypelist';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_choosenavlist';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_choosezoomlist';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_indivlocicon';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_staybubbleopened';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_cluster';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_gc_acceptance_required';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_gm_acceptance_required';


$GLOBALS['TL_DCA']['tl_module']['palettes']['registration'] = 
	str_replace(';{account_legend}',';{account_legend},cm_register_autocoord,cm_register_showonmap',$GLOBALS['TL_DCA']['tl_module']['palettes']['registration']);

/**
 * Add palettes to tl_module - main palettes
 */

$GLOBALS['TL_DCA']['tl_module']['palettes']['cm_membergooglemapspos'] = 
     '{title_legend},name,headline,type;'
    .'{config_legend},cm_membergooglemaps_member;'
    .'{cm_membergooglemaps_showmap},cm_map_heightdetail,'
    .'cm_map_maptypedetail,cm_map_disablewheeldetail,cm_map_choosetypedetail,'
    .'cm_map_indivcenterpos,cm_map_zoompos,'
    .'cm_map_choosenavdetail,'
    .'cm_map_infotextdetail;{cm_map_route},cm_map_routetodetail;'
    .'cm_map_infoShowOnload;'
    .'{cm_gm_accepance_legend:hide},cm_gm_acceptance_required;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';

$GLOBALS['TL_DCA']['tl_module']['palettes']['cm_memberfinder']  =
      '{title_legend},name,headline,type;'
     .'{config_legend},cm_memberlist_pg,ml_fields,cm_membergooglemaps_fieldslist;'
//     .'cm_memberlist_distanceform,cm_map_distance_as_select;'
     .'{cm_search_legend},cm_usetags,cm_memberlist_fieldsearch,'/*cm_memberlist_multifieldsearch,*/
     .'cm_memberlist_plzsearch;'
     .'{cm_memberlist_distancesearch},cm_map_locationfixed,cm_memberlist_addressform,cm_map_country,cm_map_country_as_select,cm_gc_acceptance_required,'
     .'cm_memberlist_distanceform,cm_memberlist_distanceasdropdown;'
     .'{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';

$GLOBALS['TL_DCA']['tl_module']['palettes']['cm_membergooglemaps'] = 
     '{title_legend},name,headline,type;'
    .'{config_legend},'
//-----------------------------
    .'ml_groups,ml_fields,perPage;'
    .'{cm_membergooglemaps_layout},'//'cm_membergooglemaps_fieldsdetail,'
    .'cm_addresslist_tableless,cm_membergooglemaps_fieldslist,cm_shownumberofadresses,cm_memberlist_hidedetaillink,cm_membergooglemaps_linktowebsite,cm_map_routetotable;'
    .'{cm_search_legend},cm_usetags,cm_memberlist_fieldsearch,'/*cm_memberlist_multifieldsearch,*/
    .'cm_memberlist_plzsearch;'
    .'{sort_order},cm_membergooglemaps_sortandorder;'
    
    .'{cm_memberlist_distancesearch},cm_map_locationfixed,cm_memberlist_addressform,cm_map_country,cm_map_country_as_select,cm_gc_acceptance_required,cm_memberlist_distanceintab,'
    .'cm_memberlist_distancesort,'
    .'cm_memberlist_distanceform,cm_memberlist_distanceasdropdown,cm_map_showfar,'
    .'cm_map_indivlocicon,cm_map_showcircle;'

// templates
    .'{templatelst_legend:hide},map_lsttemplate,'
    .'map_tbltemplate,'
    .'map_inftemplate;,'
    .'{templatedtl_legend:hide},map_dtltemplate,'
    .'map_infdtltemplate;'
    // list view
    .'{cm_membergooglemaps_showmaplist},cm_map_onlist,'

    .'cm_map_poslist,cm_map_heightlist,'
    .'cm_map_maptypelist,cm_map_disablewheellist,cm_map_setstylelist,'
    .'cm_map_choosetypelist,'
//    .'cm_map_choosenavlist,'
    .'cm_map_choosezoomlist,'
    .'cm_fitToCircle,'
    .'cm_map_indivcenterlist,cm_map_zoomlist,cm_map_cluster,'
    .'cm_map_routetolist,'
    .'cm_membergooglemaps_hidetable,cm_membergooglemaps_hidedetaillink,cm_map_linktowebsite,cm_map_staybubbleopened;'
    .'{cm_map_empty_legend},cm_memberlist_notfound,cm_map_showmaponempty;'
    
// detail view
    .'{cm_membergooglemaps_showmapdetail},cm_map_ondetail,'

    .'cm_map_posdetail,cm_map_heightdetail,'
    .'cm_map_maptypedetail,cm_map_disablewheeldetail,cm_map_stdzoom,cm_map_setstyledetail,'
    .'cm_map_choosetypedetail,'
//    .'cm_map_choosenavdetail,'
    .'cm_map_choosezoomdetail,'
    .'cm_map_routetodetail,'
    .'cm_map_infoShowOnload;'
	.'{cm_emailSettings_legend},cm_email_nameRequired;'
    .'{cm_gm_accepance_legend:hide},cm_gm_acceptance_required;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';

$GLOBALS['TL_DCA']['tl_module']['palettes']['cm_membergooglemapsList'] = 
     '{title_legend},name,headline,type;'
    .'{config_legend},cm_memberdetail_pg;'
//-----------------------------
    .'ml_groups,ml_fields,perPage;'
    .'{cm_membergooglemaps_layout},'//'cm_membergooglemaps_fieldsdetail,'
    .'cm_addresslist_tableless,cm_membergooglemaps_fieldslist,cm_shownumberofadresses,cm_memberlist_hidedetaillink,cm_membergooglemaps_linktowebsite,cm_map_routetotable;'
    .'{cm_allowsearch_legend},cm_usetags,cm_memberlist_fieldsearch,'/*cm_memberlist_multifieldsearch,*/
    .'cm_memberlist_plzsearch;'    
    .'{sort_order},cm_membergooglemaps_sortandorder;'
    .'{cm_memberlist_distancesearch},cm_map_locationfixed,cm_memberlist_addressform,cm_map_country,cm_map_country_as_select,cm_gc_acceptance_required,'
    .'cm_memberlist_distanceintab,cm_memberlist_distancesort,'
    .'cm_memberlist_distanceform,cm_memberlist_distanceasdropdown,cm_map_showfar,'
    .'cm_map_indivlocicon,cm_map_showcircle;'
// templates
    .'{templatelst_legend:hide},map_lsttemplate,'
    .'map_tbltemplate,'
    .'map_inftemplate;'
        // list view
    .'{cm_membergooglemaps_showmaplist},cm_map_onlist,'

    .'cm_map_poslist,cm_map_heightlist,'
    .'cm_map_maptypelist,cm_map_disablewheellist,cm_map_setstylelist,'
    .'cm_map_choosetypelist,'
//    .'cm_map_choosenavlist,'
    .'cm_map_choosezoomlist,'
    .'cm_fitToCircle,'
    .'cm_map_routetolist,'
    .'cm_map_indivcenterlist,cm_map_zoomlist,cm_map_cluster,'
    .'cm_membergooglemaps_hidetable,cm_membergooglemaps_hidedetaillink,cm_map_linktowebsite,cm_map_staybubbleopened;'
    .'{cm_map_empty_legend},cm_memberlist_notfound,cm_map_showmaponempty;'
//-----------------------------
    .'{cm_gm_accepance_legend:hide},cm_gm_acceptance_required;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';
 
$GLOBALS['TL_DCA']['tl_module']['palettes']['cm_membergooglemapsReader'] = 
     '{title_legend},name,headline,type;'
    .'{config_legend},cm_memberlist_pg,'
//-----------------------------
    .'ml_groups,ml_fields,perPage;'
// templates
    .'{templatedtl_legend:hide},map_dtltemplate,'
    .'map_infdtltemplate;'
 // detail view
    .'{cm_membergooglemaps_showmapdetail},cm_map_ondetail,'

    .'cm_map_posdetail,cm_map_heightdetail,'
    .'cm_map_maptypedetail,cm_map_disablewheeldetail,cm_map_stdzoom,cm_map_setstyledetail,'
    .'cm_map_choosetypedetail,'
//    .'cm_map_choosenavdetail,'
    .'cm_map_choosezoomdetail,'
    .'cm_map_routetodetail,'
    .'cm_map_infoShowOnload;'
    .'{cm_emailSettings_legend},cm_email_nameRequired;'
    .'{cm_gm_accepance_legend:hide},cm_gm_acceptance_required;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';
 

/*
$GLOBALS['TL_DCA']['tl_module']['palettes']['cm_membergooglemaps'] =
     '{title_legend},name,headline,type;'
    .'{config_legend},ml_groups,ml_fields,perPage;'
    .'{cm_membergooglemaps_layout},'//'cm_membergooglemaps_fieldsdetail,'
    .'cm_addresslist_tableless,cm_membergooglemaps_fieldslist;'
    .'{sort_order},cm_membergooglemaps_sortandorder;'
    .'{cm_memberlist_distancesearch},cm_memberlist_addressform;'
    .'{cm_membergooglemaps_showmapdetail},cm_map_ondetail;'
    .'{cm_membergooglemaps_showmaplist},cm_map_onlist;'
    .'{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';

*/

/**
 * Add palettes to tl_module - subpalettes
 */
/*

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_ondetail'] =
     'cm_map_posdetail,cm_map_heightdetail,'
    .'cm_map_stdzoom,'
    .'cm_map_maptypedetail,cm_map_choosetypedetail,'
    .'cm_map_choosenavdetail,'
    .'cm_map_routetodetail';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_onlist'] =
     'cm_map_poslist,cm_map_heightlist,'
    .'cm_map_showtableonsearch,cm_map_autozoomlist,'
    .'cm_map_maptypelist,cm_map_choosetypelist,'
    .'cm_map_choosenavlist';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_memberlist_distanceform'] =
     'cm_map_showfar';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_memberlist_addressform'] =
     'cm_map_indivlocicon,cm_memberlist_distanceintab,cm_memberlist_distanceform';

*/
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_memberlist_fieldsearch_no'] ='';
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_memberlist_fieldsearch_single'] =
'cm_memberlist_searchfieldslist';
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_country_as_select'] =
',cm_map_country_list';
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_memberlist_fieldsearch_multi'] =
'cm_memberlist_searchfieldslist';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_indivlocicon'] =
     'cm_map_locicon,cm_map_locicon_anchor,cm_map_lociconpopup_anchor';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_memberlist_distancesort'] =
     'cm_memberlist_distanceorder';
     
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_memberlist_distanceasdropdown'] =
     'cm_memberlist_distancevalues';
     
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_showfar'] =
     'cm_map_nearmarker';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_showcircle'] =
     'cm_map_circlecolor';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_indivcenterlist'] = 
//     'cm_map_zoomlist,
     'cm_map_centerlist';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_showmaponempty'] = 
     'cm_map_centerempty,cm_map_zoomempty';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_setstylelist'] = 
     'cm_map_styleidlist';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_setstyledetail'] = 
     'cm_map_styleiddetail';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_membergooglemaps_hidetable'] = 
     'cm_membergooglemaps_showtableonsearch';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_indivcenterpos'] = 
     'cm_map_centerpos';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_choosetypedetail'] = 
     'cm_map_ctrltypedetail';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_choosenavdetail'] = 
     'cm_map_ctrlnavdetail';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_choosezoomdetail'] = 
     'cm_map_ctrlzoomdetail';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_choosetypelist'] = 
     'cm_map_ctrltypelist';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_choosenavlist'] = 
     'cm_map_ctrlnavlist';
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_choosezoomlist'] = 
     'cm_map_ctrlzoomlist';
// $GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_memberlist_multifieldsearch'] =
//      'cm_memberlist_searchfieldslist';
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_staybubbleopened'] =
     'cm_map_clicktype';
     
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_memberlist_plzsearch'] =
     'cm_memberlist_plznumberdigits';
     
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_cluster'] =
     'cm_map_cluster_gridsize,cm_map_cluster_maxzoom,'
     .'cm_map_clusterlayoutid';
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_gm_acceptance_required_on'] =
    'cm_gm_acceptance_text';
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_gm_acceptance_required_page'] = 
    '';
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_gm_acceptance_required_off'] = 
    '';
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_gc_acceptance_required'] =
'cm_gc_acceptance_label';


/**
 * Add fields to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_usetags'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_usetags'],
	'inputType'			=> 'checkbox',
	'default'			=> false,
	'search'			=> false,
	'sql'				=> "char(1) NOT NULL default ''"
);

// $GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_fieldsearch'] = array
// (
// 	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_fieldsearch'],
// 	'inputType'			=> 'checkbox',
// 	'default'			=> false,
// 	'search'			=> false,
// 	'sql'				=> "char(1) NOT NULL default '1'"
// );
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_fieldsearch'] = array
(
    'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_fieldsearch'],
    'inputType'			=> 'select',
    'default'			=> false,
    'options'           => array('no'=>'cm_memberlist_nofieldsearch','single'=>'cm_memberlist_singlefieldsearch','multi'=>'cm_memberlist_multifieldsearch'),
    'reference'			=> &$GLOBALS['TL_LANG']['tl_module'],
    'search'			=> false,
    'eval'				=> array('submitOnChange'=>true, 'tl_class'=>'w50'),
    'sql'				=> "varchar(10) NOT NULL default 'no'"
);

// $GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_singlefieldsearch'] = array
// (
//     'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_singlefieldsearch'],
//     'inputType'			=> 'checkbox',
//     'default'			=> false,
//     'search'			=> false,
//     'sql'				=> "char(1) NOT NULL default ''"
// );
// $GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_multifieldsearch'] = array
// (
// 	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_multifieldsearch'],
// 	'inputType'			=> 'checkbox',
// 	'default'			=> false,
// 	'search'			=> false,
// 	'sql'				=> "char(1) NOT NULL default ''"
// );

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_searchfieldslist'] = array
(
    'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_searchfieldslist'],
    'inputType'			=> 'checkbox',
    'options_callback'	=> array('tl_module_cm_membergooglemaps', 'getViewableMemberProperties'),
    'eval'				=> array('multiple'=>true, 'tl_class'=>'clr'),
    'sql'				=> "blob NULL"
);


$GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_plzsearch'] = array
(
	'label'             => &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_plzsearch'],
	'inputType'         => 'checkbox',
	'default'			=> false,
	'search'			=> false,
	'eval'				=> array('submitOnChange'=>true,'tl_class'=>'clr'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_plznumberdigits'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_plznumberdigits'],
	'inputType'			=> 'text',
	'search'			=> false,
	'eval'				=> array('mandatory'=>false,  'rgxp'=>'digit', 'tl_class'=>'w50'),
	'sql'				=> "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_notfound'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_notfound'],
	'exclude'            => true,
	'search'             => true,
	'inputType'          => 'textarea',
	'eval'               => array('rte'=>'tinyMCE', 'helpwizard'=>true),
	'explanation'        => 'insertTags',
	'sql'				=> "text NULL"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_pg'] = array
(
	'label'         	=> &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_pg'],
	'exclude'       	=> true,
	'inputType'     	=> 'pageTree',
	'eval'          	=> array('fieldType'=>'radio'),
	'sql'				=> "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberdetail_pg'] = array
(
	'label'         	=> &$GLOBALS['TL_LANG']['tl_module']['cm_memberdetail_pg'],
	'exclude'       	=> true,
	'inputType'     	=> 'pageTree',
	'eval'          	=> array('fieldType'=>'radio'),
	'sql'				=> "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_country'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_country'],
	'inputType'			=> 'text',
	'search'			=> false,
	'eval'				=> array('mandatory'=>false, 'tl_class'=>'w50 clr'),
	'sql'				=> "varchar(5) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_country_as_select'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_country_as_select'],
	'inputType'			=> 'checkbox',
	'search'			=> false,
	'eval'				=> array('submitOnChange'=>true, 'tl_class'=>'w50 m12'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_country_list'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_country_list'],
	'inputType'			=> 'text',
	'search'			=> false,
	'eval'				=> array('mandatory'=>false, 'tl_class'=>'w50 clr'),
	'sql'				=> "varchar(255) NOT NULL default ''"
);

/*
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_distance_as_select'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_distance_as_select'],
	'inputType'			=> 'checkbox',
	'search'			=> false,
	'eval'				=> array('tl_class'=>'w50'),
//  'eval'				=> array('submitOnChange'=>true),
	'sql'				=> "char(1) NOT NULL default ''"
);
*/

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_indivlocicon'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_indivlocicon'],
	'inputType'			=> 'checkbox',
	'search'			=> false,
	'eval'				=> array('submitOnChange'=>true, 'mandatory'=>false, 'tl_class'=>'clr w50'),
	'sql'				=> "char(1) NOT NULL default ''"
);


$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_locicon'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_locicon'],
	'inputType'			=> 'fileTree',
	'eval'				=> array(
								'fieldType'	=>'radio',
                            	'files'		=>true, 
                            	'filesOnly'	=>true,
                            	'multiple'	=>false,
								'tl_class'=>'clr w50'),
	'sql'				=> (version_compare(VERSION, '3.2', '<')) ? "varchar(255) NOT NULL default ''" : "binary(16) NULL" 
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_locicon_anchor'] = array
(
    'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_locicon_anchor'],
    'inputType'          => 'cm_AnchorField',
    'eval'               => array('rgxp'=>'digit_auto_inherit', 'tl_class'=>'clr w50'),
    'sql'                     => "varchar(128) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_lociconpopup_anchor'] = array
(
    'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_lociconpopup_anchor'],
    'inputType'          => 'cm_AnchorField',
    'eval'                    => array('rgxp'=>'digit_auto_inherit', 'tl_class'=>'w50'),
    'sql'                     => "varchar(128) NOT NULL default ''"
);


$GLOBALS['TL_DCA']['tl_module']['fields']['cm_membergooglemaps_sortandorder'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_sortandorder'],
	'inputType'			=> 'cm_SortWizard',
	'options_callback'	=> array('tl_module_cm_membergooglemaps', 'getViewableMemberProperties'),
	'sql'				=> "blob NULL"
);

/*
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_membergooglemaps_fieldsdetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_fieldsdetail'],
	'inputType'          => 'cm_SortWizard',
	'options_callback'   => array('tl_module_memberlist', 'getViewableMemberProperties')
);
*/
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_membergooglemaps_fieldslist'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_fieldslist'],
	'inputType'			=> 'cm_ListWizard',
	'options_callback'	=> array('tl_module_cm_membergooglemaps', 'getViewableMemberProperties'),
	'sql'				=> "blob NULL"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_addressform'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_addressform'],
	'inputType'			=> 'checkbox',
	'search'			=> false,
	'default'			=> true,
	'eval'				=> array('tl_class'=>'w50 clr'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_distanceform'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceform'],
	'inputType'			=> 'checkbox',
	'search'			=> false,
	'eval'				=> array('tl_class'=>'w50 clr'),
//  'eval'               => array('submitOnChange'=>true),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_distanceasdropdown'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceasdropdown'],
	'inputType'			=> 'checkbox',
	'search'			=> false,
	'eval'				=> array('submitOnChange'=>true,'tl_class'=>'w50 clr'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_distancevalues'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distancevalues'],
	'inputType'			=> 'text',
	'search'			=> false,
	'eval'				=> array('mandatory'=>false, 'tl_class'=>'w50','rgxp'=>'intvalsanddefault'),
	'sql'				=> "varchar(255) NOT NULL default ''"
);


$GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_distancesort'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distancesort'],
	'inputType'			=> 'checkbox',
	'search'			=> false,
	'eval'				=> array('submitOnChange'=>true, 'mandatory'=>false, 'tl_class'=>'w50 clr'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_distanceorder'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceorder'],
	'inputType'			=> 'select',
	'options'			=> array
                          (
                            'asc'=>'cm_memberlist_asc',
                            'desc'=>'cm_memberlist_desc'
                          ),
	'reference'			=> &$GLOBALS['TL_LANG']['tl_module'],
	'search'			=> false,
	'eval'				=> array('includeBlankOption'=>false,'tl_class'=>'w50'),
	'sql'				=> "varchar(5) NOT NULL default ''"
);
/*
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster'],
	'inputType'			=> 'checkbox',
	'search'			=> false,
  	'eval'              => array('submitOnChange'=>true,'tl_class'=>'clr') ,
    'sql'				=> "char(1) NOT NULL default ''"
);
*/
/*
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_clusterlayoutid'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_clusterlayoutid'],
	'inputType'          => 'select',
	'foreignKey'         => 'tl_cm_gmapclusterlayout.name',
 	'eval'               => array('includeBlankOption'=>true, 'tl_class'=>'w50'),
	'sql'				=> "int(10) unsigned NOT NULL default '0'"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster_gridsize'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_gridsize'],
	'inputType'			=> 'text',
    'default'			=> 20,
	'search'			=> false,
	'eval'				=> array('mandatory'=>false,  'rgxp'=>'digit', 'tl_class'=>'w50'),
    'sql'				=> "int(10) unsigned NOT NULL default '20'"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster_maxzoom'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_maxzoom'],
	'inputType'          => 'select',
    'default'			=> 15,
	'options'            => array
                          (
                            0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
                          ),
	'search'             => false,
	'eval'               => array('mandatory'=>false, 'tl_class'=>'w50'),
    'sql'				=> "int(2) NOT NULL default '15'"
);
*/

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_addresslist_tableless'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_addresslist_tableless'],
	'inputType'			=> 'checkbox',
	'search'			=> false,
	'eval'				=> array('mandatory'=>false),
	'sql'				=> "char(1) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_shownumberofadresses'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_shownumberofadresses'],
	'inputType'			=> 'checkbox',
	'search'			=> false,
	'eval'				=> array('mandatory'=>false),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_hidedetaillink'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_hidedetaillink'],
	'inputType'			=> 'checkbox',
	'default'			=> false,
	'search'			=> false,
	'eval'				=> array('mandatory'=>false,'tl_class'=>'clr w50'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_membergooglemaps_hidedetaillink'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_hidedetaillink'],
	'inputType'			=> 'checkbox',
	'default'			=> false,
	'search'			=> false,
	'eval'				=> array('mandatory'=>false,'tl_class'=>'clr w50'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_staybubbleopened'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_staybubbleopened'],
	'inputType'			=> 'checkbox',
	'default'			=> false,
	'search'			=> false,
	'eval'				=> array('submitOnChange'=>true,'mandatory'=>false,'tl_class'=>'clr w50'),
	'sql'				=> "char(1) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_clicktype'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_clicktype'],
	'inputType'          => 'select',
	'options'            => array
                          (
                            1=>'cm_mouseover_click',
                            2=>'cm_click_dblclick'
                          ),
	'reference'          => &$GLOBALS['TL_LANG']['tl_module'],
	'search'             => false,
	'eval'               => array('includeBlankOption'=>false,'tl_class'=>'w50'),
	'sql'				=> "int(2) NOT NULL default '1'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_membergooglemaps_linktowebsite'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_linktowebsite'],
	'inputType'          => 'checkbox',
  'default'            => false,
	'search'             => false,
  'eval'               => array('mandatory'=>false,'tl_class'=>'clr w50'),
	'sql'				=> "char(1) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_linktowebsite'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_linktowebsite'],
	'inputType'          => 'checkbox',
  'default'            => false,
	'search'             => false,
  'eval'               => array('mandatory'=>false,'tl_class'=>'clr w50'),
	'sql'				=> "char(1) NOT NULL default ''"
);
/*
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_membergooglemaps_filterform'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_filterform'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('mandatory'=>false,'tl_class'=>'w50'),
	'sql'				=> ""
);
*/
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_memberlist_distanceintab'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_memberlist_distanceintab'],
	'inputType'          => 'checkbox',
	'search'             => false,
  	'eval'               => array('mandatory'=>false,'tl_class'=>'w50 clr'),
	'sql'                => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_showfar'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_showfar'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('submitOnChange'=>true,'tl_class'=>'w50 clr'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_nearmarker'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_nearmarker'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'tl_class'=>'w50'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_showcircle'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_showcircle'],
	'inputType'          => 'checkbox',
	'search'             => false,
    'eval'               => array('submitOnChange'=>true, 'mandatory'=>false, 'tl_class'=>'clr'),
	'sql'				=> "char(1) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_circlecolor'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_circlecolor'],
	'inputType'          => 'text',
	'eval'               => array('maxlength'=>6, 'multiple'=>false, 'size'=>1, 'isHexColor'=>true, 'colorpicker'=>true, 'decodeEntities'=>true, 'tl_class'=>'w50 wizard'),
/*	
 'wizard'             => array
	(
		array('tl_module_cm_membergooglemaps', 'colorPicker')
	),
 */
  	'sql'				=> "varchar(64) NOT NULL default ''"

);
/*
 * templates
 */
$GLOBALS['TL_DCA']['tl_module']['fields']['map_lsttemplate'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['map_lsttemplate'],
	'default'                 => 'mod_cm_memberlist_googlemaps',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'        => array('tl_module_cm_membergooglemaps', 'getLstTemplates'),
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(50) NOT NULL default 'mod_cm_memberlist_googlemaps'"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['map_tbltemplate'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['map_tbltemplate'],
	'default'                 => 'mod_cm_memberlist_googlemaps_table',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'        => array('tl_module_cm_membergooglemaps', 'getTblTemplates'),
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(50) NOT NULL default 'mod_cm_memberlist_googlemaps_table'"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['map_inftemplate'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['map_inftemplate'],
	'default'                 => 'info_cm_membergooglemaps_list',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'        => array('tl_module_cm_membergooglemaps', 'getInfTemplates'),
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(50) NOT NULL default 'info_cm_membergooglemaps_list'"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['map_dtltemplate'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['map_dtltemplate'],
	'default'                 => 'mod_cm_memberlist_googlemaps_detail',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'        => array('tl_module_cm_membergooglemaps', 'getDtlTemplates'),
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(50) NOT NULL default 'mod_cm_memberlist_googlemaps_detail'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['map_infdtltemplate'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['map_infdtltemplate'],
	'default'                 => 'info_cm_membergooglemaps',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'        => array('tl_module_cm_membergooglemaps', 'getInfDtlTemplates'),
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(50) NOT NULL default 'info_cm_membergooglemaps'"
);
 

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_ondetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_ondetail'],
	'inputType'          => 'checkbox',
	'search'             => false,
	'default'            => true,
//  'eval'               => array('submitOnChange'=>true),
	'sql'				=> "char(1) NOT NULL default '1'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_onlist'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_onlist'],
	'inputType'          => 'checkbox',
	'search'             => false,
	'default'            => true,
//  'eval'               => array('submitOnChange'=>true),
	'sql'				=> "char(1) NOT NULL default '1'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_heightdetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_heightdetail'],
	'inputType'          => 'inputUnit',
	'options'            => array('px', '%', 'em', 'pt', 'pc', 'in', 'cm', 'mm'),
	'eval'               => array('includeBlankOption'=>true, 'rgxp'=>'alnum', 'tl_class'=>'w50'),
	'sql'				=> "varchar(64) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_setstyledetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_setstyledetail'],
	'inputType'          => 'checkbox',
	'search'             => false,
	'eval'               => array('submitOnChange'=>true, 'mandatory'=>false, 'tl_class'=>'w50 clr'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_styleiddetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_styleiddetail'],
	'inputType'          => 'select',
	'foreignKey'         => 'tl_cm_gmaplayout.name',
 	'eval'               => array('includeBlankOption'=>true, 'tl_class'=>'w50'),
	'sql'				=> "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_setstylelist'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_setstylelist'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('submitOnChange'=>true, 'mandatory'=>false, 'tl_class'=>'w50 clr'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_styleidlist'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_styleidlist'],
	'inputType'          => 'select',
	'foreignKey'         => 'tl_cm_gmaplayout.name',
 	'eval'               => array('includeBlankOption'=>true, 'tl_class'=>'w50'),
	'sql'				=> "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_posdetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_posdetail'],
	'exclude'            => true,
	'inputType'          => 'radioTable',
	'options'            => array('above', 'below'),
	'eval'               => array('cols'=>2, 'tl_class'=>'w50'),
	'sql'				=> "varchar(64) NOT NULL default ''"
);


$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_maptypedetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_maptypedetail'],
	'inputType'          => 'select',
	'options'            => array
                          (
                            'n'=>'cm_map_viewnorm',
                            's'=>'cm_map_viewsat',
                            'h'=>'cm_map_viewhyb',
                            'p'=>'cm_map_viewphys'
                          ),
	'reference'          => &$GLOBALS['TL_LANG']['tl_module'],
	'search'             => false,
	'eval'               => array('mandatory'=>true, 'tl_class'=>'w50 clr'),
	'sql'				=> "char(1) NOT NULL default 'n'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_disablewheeldetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_disablewheeldetail'],
	'inputType'          => 'checkbox',
  'default'            => false,
	'search'             => false,
	'eval'               => array('tl_class'=>'w50 m12'),
	'sql'				=> "char(1) NOT NULL default ''"
);


//---------------------------------------------------------------------------------------------------
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_choosetypedetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_choosetypedetail'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('submitOnChange'=>true, 'mandatory'=>false, 'tl_class'=>'w50 clr'),
	'sql'				=> "char(1) NOT NULL default '1'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_ctrltypedetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrltypedetail'],
	'inputType'          => 'select',
	'options'            => array
                          (
                            'hb'=>'cm_map_typhor_bar',
                            'dm'=>'cm_map_typdpdwn_menu',
                            'df'=>'cm_map_typdfld'
                          ),
	'reference'          => &$GLOBALS['TL_LANG']['tl_module'],
	'search'             => false,
	'eval'               => array('includeBlankOption'=>true,'tl_class'=>'w50'),
	'sql'				=> "char(2) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_choosenavdetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_choosenavdetail'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('submitOnChange'=>true, 'mandatory'=>false, 'tl_class'=>'w50 clr'),
	'sql'				=> "char(1) NOT NULL default '1'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_ctrlnavdetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlnavdetail'],
	'inputType'          => 'select',
	'options'            => array
                          (
                            'sm'=>'cm_map_navsm',
                            'zo'=>'cm_map_navzo',
                            'an'=>'cm_map_navand',
                            'df'=>'cm_map_navdfld'
                          ),
	'reference'          => &$GLOBALS['TL_LANG']['tl_module'],
	'search'             => false,
	'eval'               => array('includeBlankOption'=>true,'tl_class'=>'w50'),
	'sql'				=> "char(2) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_choosezoomdetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_choosezoomdetail'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('submitOnChange'=>true, 'mandatory'=>false, 'tl_class'=>'w50 clr'),
	'sql'				=> "char(1) NOT NULL default '1'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_ctrlzoomdetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlzoomdetail'],
	'inputType'          => 'select',
	'options'            => array
                          (
                            'sm'=>'cm_map_zoomsm',
                            'la'=>'cm_map_zoomlg',
                            'df'=>'cm_map_zoomdfld'
                          ),
	'reference'          => &$GLOBALS['TL_LANG']['tl_module'],
	'search'             => false,
	'eval'               => array('includeBlankOption'=>true,'tl_class'=>'w50'),
	'sql'				=> "char(2) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_stdzoom'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_stdzoom'],
	'inputType'          => 'select',
	'options'            => array
                          (
                            0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
                          ),
	'search'             => false,
	'eval'               => array('mandatory'=>false, 'tl_class'=>'w50'),
	'sql'				=> "int(2) NOT NULL default '15'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_indivcenterlist'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_indivcenterlist'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('submitOnChange'=>true, 'tl_class'=>'clr w50'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_zoomlist'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_zoomlist'],
	'inputType'          => 'select',
	'default'			 => 0,
	'options'            => array
                          (
                            0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
                          ),
	'search'             => false,
	'eval'               => array('mandatory'=>false, 'tl_class'=>'clr w50'),
	'sql'				=> "int(2) NOT NULL default '0'"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_centerlist'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_centerlist'],
	'inputType'          => 'cm_LatLng', //'text',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'tl_class'=>'w50','rgxp'=>'geocoords'),
	'sql'				=> "varchar(64) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_showmaponempty'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_showmaponempty'],
	'inputType'          => 'checkbox',
	'search'             => false,
    'eval'               => array('submitOnChange'=>true, 'mandatory'=>false,'tl_class'=>'clr w50'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_zoomempty'] = array
(
	'label'             => &$GLOBALS['TL_LANG']['tl_module']['cm_map_zoomempty'],
	'inputType'         => 'select',
	'options'           => array
                          (
                            0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
                          ),
	'search'            => false,
	'eval'              => array('mandatory'=>false, 'tl_class'=>'w50'),
	'sql'				=> "int(2) NOT NULL default '0'"
	
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_centerempty'] = array
(
	'label'             => &$GLOBALS['TL_LANG']['tl_module']['cm_map_centerempty'],
	'inputType'         => 'cm_LatLng', //'text',
	'search'            => false,
  	'eval'              => array('mandatory'=>false, 'tl_class'=>'clr w50','rgxp'=>'geocoords'),
	'sql'				=> "varchar(64) NOT NULL default ''"
);


$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_heightlist'] = array
(
	'label'             => &$GLOBALS['TL_LANG']['tl_module']['cm_map_heightlist'],	
  'inputType'         	=> 'inputUnit',
	'options'           => array('px', '%', 'em', 'pt', 'pc', 'in', 'cm', 'mm'),
	'eval'              => array('includeBlankOption'=>true, 'rgxp'=>'digit', 'tl_class'=>'w50'),
	'sql'				=> "varchar(64) NOT NULL default ''" 	
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_membergooglemaps_hidetable'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_hidetable'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('submitOnChange'=>true, 'mandatory'=>false,'tl_class'=>'clr w50'),
	'sql'				=> "char(1) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_membergooglemaps_showtableonsearch'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_showtableonsearch'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('mandatory'=>false,'tl_class'=>'w50'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_poslist'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_poslist'],
	'exclude'            => true,
	'inputType'          => 'radioTable',
	'options'            => array('above', 'below'),
	'eval'               => array('cols'=>2, 'tl_class'=>'w50'),
	'sql'				=> "varchar(64) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_maptypelist'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_maptypelist'],
	'inputType'          => 'select',
	'options'            => array
                          (
                            'n'=>'cm_map_viewnorm',
                            's'=>'cm_map_viewsat',
                            'h'=>'cm_map_viewhyb',
                            'p'=>'cm_map_viewphys'
                          ),
	'reference'          => &$GLOBALS['TL_LANG']['tl_module'],
	'search'             => false,
	'eval'               => array('mandatory'=>true, 'tl_class'=>'w50'),
	'sql'				=> "char(1) NOT NULL default 'n'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_disablewheellist'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_disablewheellist'],
	'inputType'          => 'checkbox',
  'default'            => false,
	'search'             => false,
	'eval'               => array('tl_class'=>'w50 m12'),
	'sql'				=> "char(1) NOT NULL default ''"
);


$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_choosetypelist'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_choosetypelist'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('submitOnChange'=>true, 'mandatory'=>false, 'tl_class'=>'w50 clr'),
	'sql'				=> "char(1) NOT NULL default '1'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_ctrltypelist'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrltypelist'],
	'inputType'          => 'select',
	'options'            => array
                          (
                            'hb'=>'cm_map_typhor_bar',
                            'dm'=>'cm_map_typdpdwn_menu',
                            'df'=>'cm_map_typdfld'
                          ),
	'reference'          => &$GLOBALS['TL_LANG']['tl_module'],
	'search'             => false,
	'eval'               => array('includeBlankOption'=>true,'tl_class'=>'w50'),
	'sql'				=> "char(2) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_choosenavlist'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_choosenavlist'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('submitOnChange'=>true, 'mandatory'=>false, 'tl_class'=>'w50 clr'),
	'sql'				=> "char(1) NOT NULL default '1'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_ctrlnavlist'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlnavlist'],
	'inputType'          => 'select',
	'options'            => array
                          (
                            'sm'=>'cm_map_navsm',
                            'zo'=>'cm_map_navzo',
                            'an'=>'cm_map_navand',
                            'df'=>'cm_map_navdfld'
                          ),
	'reference'          => &$GLOBALS['TL_LANG']['tl_module'],
	'search'             => false,
	'eval'               => array('includeBlankOption'=>true,'tl_class'=>'w50'),
	'sql'				=> "char(2) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_choosezoomlist'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_choosezoomlist'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('submitOnChange'=>true, 'mandatory'=>false, 'tl_class'=>'w50 clr'),
	'sql'				=> "char(1) NOT NULL default '1'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_ctrlzoomlist'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_ctrlzoomlist'],
	'inputType'          => 'select',
	'options'            => array
                          (
                            'sm'=>'cm_map_zoomsm',
                            'lg'=>'cm_map_zoomlg',
                            'df'=>'cm_map_zoomdfld'
                          ),
	'reference'          => &$GLOBALS['TL_LANG']['tl_module'],
	'search'             => false,
	'eval'               => array('includeBlankOption'=>true,'tl_class'=>'w50'),
	'sql'				=> "char(2) NOT NULL default ''"
);


$GLOBALS['TL_DCA']['tl_module']['fields']['cm_membergooglemaps_member'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_member'],
	'inputType'          => 'select',
	'foreignKey'         => 'tl_member.concat(lastname,", ",firstname," [",id,"]")',
 	'eval'               => array('mandatory'=>true, 'tl_class'=>'w50'),
	'sql'				=> "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_zoompos'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_zoompos'],
	'inputType'          => 'select',
	'options'            => array
                          (
                            0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
                          ),
	'search'             => false,
    'eval'               => array('mandatory'=>false, 'tl_class'=>'w50 clr'),
	'sql'				=> "int(2) NOT NULL default '15'"
);

/**
 * Special for palette cm_membergooglemapspos
 */

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_indivcenterpos'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_indivcenterpos'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('mandatory'=>false,'submitOnChange'=>true, 
                            'tl_class'=>'w50 clr'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_centerpos'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_centerpos'],
	'inputType'          => 'cm_LatLng', //'text',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'tl_class'=>'w50','rgxp'=>'geocoords'),
	'sql'				=> "varchar(64) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_infoShowOnload'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_infoShowOnload'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'tl_class'=>'clr'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_email_nameRequired'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_email_nameRequired'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'tl_class'=>'clr'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_infotextdetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_infotextdetail'],
	'exclude'            => true,
	'search'             => true,
	'inputType'          => 'textarea',
	'eval'               => array('rte'=>'tinyMCE', 'helpwizard'=>true, 'tl_class' => 'clr'),
	'explanation'        => 'insertTags',
	'sql'                => "text NULL"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_routetotable'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_routetotable'],
	'inputType'          => 'checkbox',
	'search'             => false,
        'eval'               => array('mandatory'=>false,'tl_class'=>'clr'),
	'sql'                => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_routetolist'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_routetolist'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('mandatory'=>false,'tl_class'=>'clr'),
	'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_routetodetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_routetodetail'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('mandatory'=>false,'tl_class'=>'clr'),
	'sql'				=> "char(1) NOT NULL default ''"
);
/*
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_membergooglemaps_routefromdetail'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_membergooglemaps_routefromdetail'],
	'inputType'          => 'checkbox',
	'search'             => false,
  'eval'               => array('mandatory'=>false, 'tl_class'=>'w50'),
	'sql'				=> ""
);
*/

/**
 * Add fields to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_register_showonmap'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_register_showonmap'],
	'inputType'			=> 'checkbox',
	'default'			=> false,
	'search'			=> false,
	'eval'				=> array('mandatory'=>false, 'tl_class'=>''),
	'sql'				=> "char(1) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_register_autocoord'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_register_autocoord'],
	'inputType'			=> 'checkbox',
	'default'			=> false,
	'search'			=> false,
	'eval'				=> array('mandatory'=>false, 'tl_class'=>''),
	'sql'				=> "char(1) NOT NULL default ''"
);
// dsgvo
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_gc_acceptance_required'] = array
(
    'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_gc_acceptance_required'],
    'inputType'          => 'checkbox',
    'search'             => false,
    'eval'               => array('mandatory'=>false,'submitOnChange'=>true,'tl_class'=>'w50 m12 clr'),
    'sql'				=> "char(1) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_gc_acceptance_label'] = array
(    'label'       => &$GLOBALS['TL_LANG']['tl_module']['cm_gc_acceptance_label'],
    
    'inputType'			=> 'text',
    'search'			=> false,
    'eval'				=> array('mandatory'=>false, 'tl_class' => 'clr'),
    'sql'				=> "varchar(255) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_gm_acceptance_required'] = array
(
    'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_gm_acceptance_required'],
    'inputType'			=> 'select',
    'options'			=> array(
                                    'page'=>'Seiteneinstellung',
                                    'on'=>'aktiviert',
                                    'off'=>'deaktiviert'
                                ),
    'search'			=> false,
    'eval'				=> array('submitOnChange' => true, 'includeBlankOption'=>false,'tl_class'=>'w50'),
    'sql'				=> "varchar(5) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_gm_acceptance_text'] = array
(    'label'       => &$GLOBALS['TL_LANG']['tl_module']['cm_gm_acceptance_text'],
    'exclude'     => true,
    'search'      => true,
    'inputType'   => 'textarea',
    'eval'        => array('rte' => 'tinyMCE', 'helpwizard' => true, 'tl_class' => 'clr'),
    'explanation' => 'insertTags',
    'sql'         => "text NULL",
);




$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster_url1'] = array
(
    'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_url1'],
    'inputType'			=> 'text',
    'search'			=> false,
    'eval'				=> array('mandatory'=>false, 'tl_class'=>'w50'),
    'sql'				=> "varchar(100) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster_col1'] = array
(
    'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_col1'],
    'inputType'			=> 'text',
    'search'			=> false,
    'eval'				=> array('mandatory'=>false, 'tl_class'=>'w50'),
    'sql'				=> "varchar(10) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster_h1'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_h1'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('rgxp'=>'natural', 'nospace'=>false, 'tl_class'=>'w50'),
    'sql'                     => "int(10) NOT NULL default '50'"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster_w1'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_w1'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('rgxp'=>'natural', 'nospace'=>false, 'tl_class'=>'w50'),
    'sql'                     => "int(10) NOT NULL default '50'"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster_url2'] = array
(
    'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_url2'],
    'inputType'			=> 'text',
    'search'			=> false,
    'eval'				=> array('mandatory'=>false, 'tl_class'=>'w50'),
    'sql'				=> "varchar(100) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster_col2'] = array
(
    'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_col2'],
    'inputType'			=> 'text',
    'search'			=> false,
    'eval'				=> array('mandatory'=>false, 'tl_class'=>'w50'),
    'sql'				=> "varchar(10) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster_h2'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_h2'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('rgxp'=>'natural', 'nospace'=>false, 'tl_class'=>'w50'),
    'sql'                     => "int(10) NOT NULL default '50'"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster_w2'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_w2'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('rgxp'=>'natural', 'nospace'=>false, 'tl_class'=>'w50'),
    'sql'                     => "int(10) NOT NULL default '50'"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster_url3'] = array
(
    'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_url3'],
    'inputType'			=> 'text',
    'search'			=> false,
    'eval'				=> array('mandatory'=>false, 'tl_class'=>'w50'),
    'sql'				=> "varchar(100) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster_col3'] = array
(
    'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_col3'],
    'inputType'			=> 'text',
    'search'			=> false,
    'eval'				=> array('mandatory'=>false, 'tl_class'=>'w50'),
    'sql'				=> "varchar(10) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster_h3'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_h3'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('rgxp'=>'natural', 'nospace'=>false, 'tl_class'=>'w50'),
    'sql'                     => "int(10) NOT NULL default '50'"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster_w3'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_w3'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('rgxp'=>'natural', 'nospace'=>false, 'tl_class'=>'w50'),
    'sql'                     => "int(10) NOT NULL default '50'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['ml_'] = array
(
);

class tl_module_cm_membergooglemaps extends Backend
{

	public function __construct()
	{
		parent::__construct();
		$this->import('BackendUser', 'User');
	}


  public function checkPermission(){
		if ($this->User->isAdmin)
		{
			return;
		}
// 		if (!$this->User->hasAccess('xxx', 'xxxxxxx'))
// 		{
// 			$this->log('Not enough permissions to access the map type style module',
//         'tl_cm_gmaptypestyle checkPermission', TL_ERROR);
// 			$this->redirect('contao/main.php?act=error');
// 		}

   }
	/**
	 * Return all event templates as array
	 * @return array
	 */
	public function getLstTemplates()
	{
		return $this->getTemplateGroup('mod_cm_memberlist_googlemaps');
	}
	public function getTblTemplates()
	{
		return $this->getTemplateGroup('mod_cm_memberlist_googlemaps');
	}
	public function getInfTemplates()
	{
		return $this->getTemplateGroup('info_cm_membergooglemaps');
	}
	public function getDtlTemplates()
	{
		return $this->getTemplateGroup('mod_cm_memberlist_googlemaps');
	}
	public function getInfDtlTemplates()
	{
		return $this->getTemplateGroup('info_cm_membergooglemaps');
	}

	/**
	 * Return the color picker wizard
	 * @param DataContainer
	 * @return string
	public function colorPicker(\DataContainer $dc)
	{
		return ' ' . $this->generateImage('pickcolor.gif', $GLOBALS['TL_LANG']['MSC']['colorpicker'], 'style="vertical-align:top;cursor:pointer" id="moo_'.$dc->field.'"') . '
  <script>
  new MooRainbow("moo_'.$dc->field.'", {
    id:"ctrl_' . $dc->field . '",
    startColor:((cl = $("ctrl_' . $dc->field . '").value.hexToRgb(true)) ? cl : [255, 0, 0]),
    imgPath:"plugins/colorpicker/images/",
    onComplete: function(color) {
      $("ctrl_' . $dc->field . '").value = color.hex.replace("#", "");
    }
  });
  </script>';
	}
	 */
	public function getViewableMemberProperties()
	{
		$this->loadLanguageFile('tl_member');

		$return = array();

		$this->loadLanguageFile('tl_member');
		$this->loadDataContainer('tl_member');

		foreach ($GLOBALS['TL_DCA']['tl_member']['fields'] as $k=>$v)
		{
			if ($k == 'password' || $k == 'newsletter' || $k == 'publicFields' || $k == 'allowEmail' )
            // || $k == 'locked' || 
            //    $k == 'secret' || $k == 'session' || $k == 'loginAttempts' || $k == 'backupCodes'|| $k == 'trustedTokenVersion'|| $k == 'id'|| $k == 'tstamp')
			{
				continue;
			}
			if (!($v['eval']['feViewable'] ?? true))
            {
                continue;
            }
			$return[$k] = $GLOBALS['TL_DCA']['tl_member']['fields'][$k]['label'][0] ?? $k;
		}

		return $return;
	}

}

