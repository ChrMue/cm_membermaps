<?php

/**
 * PHP version 5
 * @copyright  Christian Muenster 2020 
 * @author     Christian Muenster 
 * @package    CM_MemberMaps
 * @license    LGPL 
 * @filesource
 */

/**
 * palette for tl_member_group
 */
$GLOBALS['TL_DCA']['tl_member_group']['palettes']['default'] = 
str_replace('name;', 
  'name;{cm_membergooglemaps_mapview:hide},cm_map_iconstd,cm_map_iconstd_anchor,cm_map_iconstdpopup_anchor,'
  .'cm_map_iconnear,cm_map_iconnear_anchor,cm_map_iconnearpopup_anchor;', 
  $GLOBALS['TL_DCA']['tl_member_group']['palettes']['default']); 
/**
 * Add fields to tl_member_group
 */
$GLOBALS['TL_DCA']['tl_member_group']['fields']['cm_map_iconstd'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconstd'],
	'inputType'          => 'fileTree',
  	'eval'               => array('fieldType'=>'radio',
                            'files'=>true, 'filesOnly'=>true,
  	                        'multiple'=>false, 'tl_class'=>'clr w50'),
	'sql'				=> (version_compare(VERSION, '3.2', '<')) ? "varchar(255) NOT NULL default ''" : "binary(16) NULL" 
);
$GLOBALS['TL_DCA']['tl_member_group']['fields']['cm_map_iconstd_anchor'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconstd_anchor'],
	'inputType'          => 'cm_AnchorField',
    'eval'                    => array('rgxp'=>'digit_auto_inherit', 'tl_class'=>'clr w50'),
    'sql'                     => "varchar(128) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_member_group']['fields']['cm_map_iconstdpopup_anchor'] = array
(
    'label'              => &$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconstdpopup_anchor'],
    'inputType'          => 'cm_AnchorField',
    'eval'                    => array('rgxp'=>'digit_auto_inherit', 'tl_class'=>'w50'),
    'sql'                     => "varchar(128) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member_group']['fields']['cm_map_iconnear'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconnear'],
	'inputType'          => 'fileTree',
  'eval'               => array('fieldType'=>'radio',
                            'files'=>true, 'filesOnly'=>true,
                            'multiple'=>false, 'tl_class'=>'clr w50'),
	'sql'				=> (version_compare(VERSION, '3.2', '<')) ? "varchar(255) NOT NULL default ''" : "binary(16) NULL" 
);
$GLOBALS['TL_DCA']['tl_member_group']['fields']['cm_map_iconnear_anchor'] = array
(
    'label'              => &$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconnear_anchor'],
    'inputType'          => 'cm_AnchorField',
    'eval'                    => array('rgxp'=>'digit_auto_inherit', 'tl_class'=>'clr w50'),
    'sql'                     => "varchar(128) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_member_group']['fields']['cm_map_iconnearpopup_anchor'] = array
(
    'label'              => &$GLOBALS['TL_LANG']['tl_member_group']['cm_map_iconnearpopup_anchor'],
    'inputType'          => 'cm_AnchorField',
    'eval'                    => array('rgxp'=>'digit_auto_inherit', 'tl_class'=>'w50'),
    'sql'                     => "varchar(128) NOT NULL default ''"
);
