<?php

/**
 * PHP version 7
 * @copyright  Christian Muenster 2009-2023
 * @author     Christian Muenster
 * @package    cm_membermaps
 * @license    LGPL
 * @filesource
 */

namespace ChrMue\cm_MemberMaps;


/**
 * Class ModuleMemberMapsTags
 *
 * @copyright  Christian Muenster 2023
 * @author     Christian Muenster
 * @package    Module
 *
 * based on ModuleMemberlist by Leo Feyer
 */
class ModuleMemberGoogleMapsTags extends \Contao\ModuleMemberlist
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate_list = 'mod_cm_memberlist_googlemaps';
    protected $strTemplate_table = 'mod_cm_memberlist_googlemaps_table';
    protected $strTemplate_tabless = 'mod_cm_memberlist_googlemaps_tabless';
    protected $detailParam = 'show';	
    /**
     * details view
     */
    protected $strTemplateDetail = 'mod_cm_memberlist_googlemaps_detail';
    /**
     * bubble content view
     */
    protected $strTemplateInfo = 'info_cm_membergooglemaps_list';

    protected $strTemplateInfoDetail = 'info_cm_membergooglemaps';
    /**
     * use cunstructor form the base class ModuleMemberlist
     */
    function __construct($param)
    {
        parent::__construct($param);
    }

    /**
     * Groups
     * @var array
     */
    protected $arrMlGroups = array();
    protected $arrMlFieldsList = array();
    private $fieldsInList = array();
    private $groupInList = array();
    protected $arrSearchFields = array();
    protected $useTags;
    protected $useTagsBundle;
    private $useSSL = false;

    /**
     * position of the map - standard: below the list
     */
    protected $mappos = 'below';

    private $factor = null;

    /**
     * Fields
     * @var array
     */
    private $gcapi;
    private $gcapiKey;
    private $api;
    private $apiKey;    
    
    private function getApiAndKey()
    {
        $root_id = self::getRootPageFromUrl()->id;
        $root_details =  \PageModel::findWithDetails($root_id);
        
        $gcapi = $root_details->cm_gc_api;
        if (!$gcapi) { $gcapi=\Config::get('cm_gc_api'); }
        if (!$gcapi) throw new \Exception("Api not selected");
        $gcapi_type=explode("_",  $gcapi);
        $gcapiprop="cm_map_apikey_".strtolower($gcapi_type[0]);
        
        $gcapiKey = $root_details->$gcapiprop;
        if (!$gcapiKey) { $gcapiKey=\Config::get($gcapiprop); }
        
        $api = $root_details->cm_map_api;
        if (!$api) { $api=\Config::get('cm_map_api'); }
        if (!$api) throw new \Exception("Api not selected");
        $apiprop="cm_map_apikey_".strtolower($api);
        $apiKey = $root_details->$apiprop;
        if (!$apiKey) { $apiKey=\Config::get($apiprop); }
        
        $this->gcapi=$gcapi;
        $this->gcapiKey=$gcapiKey;
        $this->api=$api;
        $this->apiKey=$apiKey;        
    }
        
    //protected $arrMlFields = array();

    /**
     * Display a wildcard in the back end
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### MEMBERLIST with Map ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }        
        if (!version_compare(VERSION, '4', '<'))
        {
            $this->useTags = $this->cm_usetags && \in_array('tags', \ModuleLoader::getActive()) && !\in_array('tl_member',explode(', ', \Config::get('disabledTagObjects')));
        }
        else
        {
            $this->useTags = $this->cm_usetags && \in_array('tags_members', \ModuleLoader::getActive()) && \in_array('tags', \ModuleLoader::getActive());
        }
        $this->useTagsBundle = $this->cm_usetags && \in_array('CodefogTagsBundle', \ModuleLoader::getActive());
        
        $this->arrMlGroups = \StringUtil::deserialize($this->ml_groups, true);
        $this->arrMlFields = \StringUtil::deserialize($this->ml_fields, true);

        $arrFieldsList = \StringUtil::deserialize($this->cm_membergooglemaps_fieldslist, true);
        $this->fieldsInList = $arrFieldsList;

        $this->arrSearchFields = \StringUtil::deserialize($this->cm_memberlist_searchfieldslist, true);
 
        if ($this->cm_memberlist_fieldsearch=='no')
        {
            $this->arrSearchFields=array();
        }
//        $this->arrSearchFields = array_intersect($this->arrSearchFields, array_merge($arrFieldsList,$this->arrMlFields));

        $this->arrMlFieldsList = null;
        if ($arrFieldsList)
        {
            foreach ($arrFieldsList as $listField)
            {
                $this->arrMlFieldsList[] = $listField["field"];
            }
        }
        $this->arrSearchFields = array_intersect($this->arrSearchFields, array_merge($this->arrMlFieldsList,$this->arrMlFields));

        if (count($this->arrMlGroups) < 1 || count($this->arrMlFieldsList) < 1 || count($this->arrMlFields) < 1)
        {
            return '';
        }
        return parent::generate();
    }

    /**
     * Generate module
     */
    protected function compile()
    {
        $this->useSSL=\Config::get('cm_request_gm_ssl') || \Environment::get('ssl');        
        // Set the item from the auto_item parameter
        if (!isset($_GET[$this->detailParam]) && \Config::get('useAutoItem') && isset($_GET['auto_item']))
	{
            \Input::setGet($this->detailParam, \Input::get('auto_item'));
	}
	$this->getApiAndKey();
        if ($this->api=='OSM')
        {
            $GLOBALS['TL_CSS'][] = 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css';
        }
        parent::compile();
   }

    /**
     * List all members
     */
    protected function listAllMembers()
    {
        \Contao\Controller::loadDataContainer('tl_member');
        if ($this->cm_addresslist_tableless)
        {
            if ($this->map_tbltemplate) $this->strTemplate_tabless= $this->map_tbltemplate;
        } 
        else
        {
            if ($this->map_tbltemplate) $this->strTemplate_table= $this ->map_tbltemplate;
        }
	if ($this->map_lsttemplate) 
        {
            $this->strTemplate_list = $this->map_lsttemplate;
        }
		
        global $objPage;
        $language = $objPage->language;

        $showCircle = false;
        $latitude = null;
        $longitude = null;

        $this->Template = new \FrontendTemplate($this->strTemplate_list);
	$this->Template->allInOne = true;        

        if ($this->cm_addresslist_tableless)
        {
            $this->ElementTemplate = new \FrontendTemplate($this->strTemplate_tabless);
        } 
        else
        {
            $this->ElementTemplate = new \FrontendTemplate($this->strTemplate_table);
        }

        if (!array_search("cm_coords", $this->arrMlFieldsList))
        {
            array_push($this->arrMlFieldsList, "cm_coords");
        }
        if (!array_search("cm_allowmap", $this->arrMlFieldsList))
        {
            array_push($this->arrMlFieldsList, "cm_allowmap");
        }

        $arrValidMembers = '';
        //---fuer Tags ... ---------------------------------------------------
        if ($this->useTags) 
        {
            if (strlen(\Input::get('tag')))
            {
                $relatedlist = (strlen(\Input::get('related'))) ? preg_split("/,/", \Input::get('related')) : array();
                $alltags = array_merge(array(\Input::get('tag')), $relatedlist);
    
                $tagids = MemberGoogleMapsTagModel::getTagIds($alltags);
                $arrValidMembers = $tagids;
                if (count($arrValidMembers) == 0)
                {
                    $this->Template->thead = array();
                    $this->Template->tbody = array();
    
                    // Pagination
                    $objPagination = new \Pagination($objTotal->count, $per_page);
                    $this->Template->pagination = $objPagination->generate("\n  ");
                    $this->Template->per_page = $per_page;
    
                    // Template variables
                    $this->Template->action = ampersand(\Environment::get('request'));
                    $this->Template->search_label = specialchars($GLOBALS['TL_LANG']['MSC']['search']);
                    $this->Template->per_page_label = specialchars($GLOBALS['TL_LANG']['MSC']['list_perPage']);
                    $this->Template->search = \Input::get('search');
                    $this->Template->for = \Input::get('for');
                    $this->Template->order_by = \Input::get('order_by');
                    $this->Template->sort = \Input::get('sort');
                    $this->Template->plzarea = \Input::get('plzarea');
                    return;
                }
            }
        }
        else if ($this->useTagsBundle)
        {
            $this->setCfgTagWidgets();
            $widgets = $this->generateCfgTagWidgets();
            $arrValidMembers = $this->preselectMemberByTags();
            $this->Template->cfgTagsWidgets = $widgets;
        }

        //------------------------------------------------------
        $location = \Input::get('cm_location');

        $geocoding_required = !$this->cm_map_locationfixed && $location != "";
        $cm_show_gc_privacyError =false;
        if ($geocoding_required && $this->cm_gc_acceptance_required 
                && \Input::get('cm_gc_privacy')!='accepted') 
        {
            $geocoding_required=false;
            $cm_show_gc_privacyError =true;
            $this->Template->gc_privacyError=$GLOBALS['TL_LANG']['MSC']['cm_gc_privacyerror'];
        }
        $this->Template->show_gc_privacyError=$cm_show_gc_privacyError;
        if ($this->cm_gc_acceptance_required)
        {
            $this->Template->lbl_cm_gc_privacy = $this->cm_gc_acceptance_label;
        }
        
        $time = time();
        $arrFieldsList = $this->arrMlFieldsList;
        $searchObj = array(
            'where' => '',
            'values'=> array()
        );

        $fieldsearch = $this->cm_memberlist_fieldsearch=='single'; 
        $multifieldsearch = $this->cm_memberlist_fieldsearch=='multi';         
        
        $plzsearch = $this->cm_memberlist_plzsearch;
        $plzareasize = $this->cm_memberlist_plznumberdigits;

        $this->Template->fieldsearch = $fieldsearch;
        $this->Template->multifieldsearch = $multifieldsearch;
        $this->Template->plzsearch = $plzsearch;
        $this->Template->cm_map_locationfixed= $this->cm_map_locationfixed;

        // Search query
        $searchStr=\Input::get('search');
        $forStr=\Input::get('for');
        $searchObj = self::getTextSearchOptions($searchObj,$multifieldsearch,$fieldsearch,$this->arrSearchFields,$searchStr,$forStr);

        /// ----- plz search  ----------------------------------
        $plzArea = \Input::get('plzarea');
        $searchObj = self::getPlzSearchOptions($searchObj,$plzsearch,$plzareasize,$plzArea);

        //------------------------------------------------
        $isGroupRequested = null;
        $requestedGroups = array();
        if (\Input::get('gf') != '')
        {
            $requestedGroups = \Input::get('gf');
            foreach ($requestedGroups as $group)
            {
                $isGroupRequested[$group] = true;
            }
        }
        //----------------------------

        $country = \Input::get('cm_country');
        if (!$country)
        {
            $country = $this->cm_map_country;
        }
        $max_dist = null;
        if (\Input::get('cm_max_dist'))
        {
            $max_dist = \Input::get('cm_max_dist');
        }

        $std_dist = null;

        $this->Template->distanceasdropdown = $this->cm_memberlist_distanceasdropdown;
        //init with Std
        
        if ($this->cm_memberlist_distanceasdropdown)
        {
            $this->Template->distvalues = \cm_Maps\cm_Map_lib::getDistanceOptionsHTML(
				$this->cm_memberlist_distancevalues, $max_dist
			);
        }

        $hasLocal = false;
        $searchArr = array();
        if ($geocoding_required)
        {
            $visitorsLocation = str_replace(' ', '+', ($location));

            // execute geocoding
            
            try
            {
                //echo $gcapiprop.' '.$gcapiKey;
                $data = \cm_Maps\cm_Map_lib::getGoogleMapsGeoData($visitorsLocation, $this->useSSL ,$country, $this->gcapi, $this->gcapiKey);
                //print_r($data);die("xxx");
                //check geocoding result
                if (!$data || $data["status"]!="OK")
                    throw new \Exception("Adresse nicht gefunden");
                $latitude = $data["lat"];
                $longitude = $data["lng"];
                //list($latitude, $longitude, $altitude) = explode(",", $data);
                if (($longitude == "") || ($latitude == ""))
                    throw new \Exception("Adresse nicht gefunden");
                $hasLocal = true;
                $showCircle = $this->cm_map_showcircle && ($max_dist > 0);

                // show distance:
                $this->fieldsInList[] = array('field' => 'dist');
                $searchArr = array('location' => $location,
                				   'country' => $country, 
                				   'max_dist' => $max_dist);
    	    } 
    	    catch (\Exception $e)
            {
    	        
                //print_r($e->getMessage());die("xxx");
                $this->Template->Error = "Fehler: " . $GLOBALS['TL_LANG']['MSC']['cm_memberlist_notfound'];
                $hasLocal = false;
            }
        }
        if ($this->cm_map_locationfixed)
        {
            $location="";
            $showCircle = $this->cm_map_showcircle && ($max_dist > 0);
            $hasLocal = true;
            $this->fieldsInList[] = array('field' => 'dist');
            $searchArr = array('location' => $location, 
                               'country' => $country, 
                               'max_dist' => $max_dist);
            list($latitude, $longitude, $altitude) = explode(',', $this->cm_map_fixlocation);
        }
        $this->Template->searchArr = $searchArr;
        $lat = $latitude;
        $lng = $longitude;
        //$this->Template->Error .="Standort: ".($hasLocal?'yes':'no')." L:".$longitude." B:".$latitude;
        
        $this->Template->cm_map_locationfixed= $this->cm_map_locationfixed;
        
        if ($this->cm_memberlist_addressform || $this->cm_map_locationfixed)
        {
            $this->Template->distanceform = $this->cm_memberlist_distanceform;
            $this->Template->lbl_location = $GLOBALS['TL_LANG']['MSC']['cm_lbl_location'];
            $this->Template->lbl_country = $GLOBALS['TL_LANG']['MSC']['cm_lbl_country'];
            $this->Template->lbl_max_dist = $GLOBALS['TL_LANG']['MSC']['cm_lbl_max_dist'];
            $this->Template->lbl_max_dist_drdn = $GLOBALS['TL_LANG']['MSC']['cm_lbl_max_dist_drdn'];

            $this->Template->cm_distitem = $GLOBALS['TL_LANG']['MSC']['cm_distitem'];

            $this->Template->distsearch_label = specialchars($GLOBALS['TL_LANG']['MSC']['cm_distsearch_label']);
            $this->Template->radius_search = specialchars($GLOBALS['TL_LANG']['MSC']['cm_radius_search']);

            $this->Template->radiusform = $this->cm_memberlist_addressform || $this->cm_map_locationfixed;
            //      $this->Template->cm_distsearch_label=$GLOBALS['TL_LANG']['MSC']['cm_memberlist_distsearch'];
            $this->Template->cm_gc_acceptance_required =  $this->cm_gc_acceptance_required;
        }
        //    $this->Template->listdistance=$this->cm_memberlist_distanceintab && $hasLocal;
        $this->ElementTemplate->listdistance = $this->cm_memberlist_distanceintab && $hasLocal;
        $this->Template->visitorlocation = $location;

        $this->Template->country = $country;

        if ($this->cm_map_country_as_select)
        {
            $countries = trim($this->cm_map_country_list);
            $countrylist = $countries ? explode( ',', $countries) : null;
            
            $visitorcountryobj = '<select name="cm_country" class="cm_country" >';

            foreach ($this->getCountries() as $countryCode => $countryName)
            {
                if (!$countrylist || \in_array($countryCode,$countrylist))
                {
                    $visitorcountryobj .= '<option value="' . strtolower($countryCode) . '" ' 
                        .(strtolower($countryCode) == strtolower($country) ? ' selected="selected"' : '') . '>' . $countryName . '</option>';
                }
            }
            $visitorcountryobj .= '</select>';
        } 
        else
        {
            $visitorcountryobj = '<input class="cm_country" type="text" name="cm_country" value="' . $country . '" />';
        }

        $this->Template->visitorcountry = $visitorcountryobj;

        $this->Template->max_dist = $max_dist;

        $strOptions = '';
        $arrSortedFields = array();

        // Sort fields
        foreach ($this->arrSearchFields as $field)
        {
            $arrSortedFields[$field] = $GLOBALS['TL_DCA']['tl_member']['fields'][$field]['label'][0];
        }
        natcasesort($this->arrSearchFields);
        // Add searchable fields to drop-down menu
        foreach ($arrSortedFields as $k => $v)
        {
            if (($k != 'cm_coords') && ($k != 'cm_allowmap'))
            {
                $strOptions .= '  <option value="' . $k . '"' . (($k == \Input::get('search')) ? ' selected="selected"' : '') . '>' . $v . '</option>' . "\n";
            }
        }

        $this->Template->search_fields = $strOptions;
        //---------------------------------------
        // Read group names and  groupe icons
        //    $objGroupNames=$this->getGroupNamesAndIcons();
        $objGroupNames = MemberGoogleMapsMemberGroupModel::getGroupNamesAndIcons($this->arrMlGroups);
	$searchObj = self::getGroupFilterSearchOptions($searchObj, $requestedGroups);
		
        //---------------------------------------
        // Build Whereclause - Groupfilter

        // Filter groups
        $searchObj = self::getGroupSearchOptions($searchObj,$this->arrMlGroups);
        
	$searchObj = self::getActiveMemberFilterOptions($searchObj,\in_array('username', $arrFieldsList), $time);
		
        // Split results
        $page = \Input::get('page') ? \Input::get('page') : 1;
        $per_page = \Input::get('per_page') ? \Input::get('per_page') : $this->perPage;

        //Sort list
        $sortorder = "";
        $firstitem = true;

        if ($this->cm_memberlist_distancesort && $hasLocal)
        {
            $sortorder = 'dist ' . $this->cm_memberlist_distanceorder;
        } 
        else
        {
            if ($this->cm_membergooglemaps_sortandorder)
            {
                foreach (\StringUtil::deserialize($this->cm_membergooglemaps_sortandorder) as $orderitems)
                {
                    if ($firstitem)
                    {
                        $sortorder .= " ";
                        $firstitem = false;
                    } else
                    {
                        $sortorder .= ",";
                    }
                    $sortorder .= $orderitems["field"] . " " . $orderitems["order"];
                }
            }
        }
        //    if (!($this->Input->get('order_by')=='dist' && !($this->Input->get('cm_max_dist') && $max_dist!=0)))
        if (!(\Input::get('order_by') == 'dist' && !$hasLocal))
        {

            $order_by = (\Input::get('order_by')) ? 
                    \Input::get('order_by') . ' ' . \Input::get('sort') : 
                    $sortorder;
        }
        // Begin query

        $distFormula = \cm_Maps\cm_Map_lib::getDistanceFormula($lat, $lng);
        // Get total number of members
        
        if ($hasLocal)
        {

            $objTotal = \Database::getInstance()
            				->prepare("SELECT COUNT(*) AS count FROM (SELECT id, " . $distFormula . " AS dist from tl_member WHERE " 
            						. ($arrValidMembers ? "id IN (" . join(",", $arrValidMembers) 
            						. ") AND " : "") 
            						. $searchObj['where'] 
            						. ") as T WHERE " 
            						. ((\Input::get('cm_max_dist') && $max_dist) ? " dist<=" . $max_dist : "1")) 
            				->execute($searchObj['values']);

            $objMemberStmt =\Database::getInstance()
            				->prepare("SELECT id, alias,username, publicFields, `groups`" 
            						. (count($arrFieldsList) > 0 ? ", " . implode(", ", $arrFieldsList) : " ") 
            						. ", dist"
            						. " FROM (select *, " . $distFormula . " AS dist FROM tl_member WHERE " 
            						. ($arrValidMembers ? "id IN (" . join(',', $arrValidMembers) . ") AND " : "") 
            						. $searchObj['where'] 
            						. " ) AS T WHERE " 
            						. ((!$this->cm_map_showfar && \Input::get('cm_max_dist') && $max_dist) ? " dist<=" . $max_dist : "1") 
            						. ($order_by ? " ORDER BY " . $order_by : ""));
        } else
        {
            $objTotal = \Database::getInstance()
            				->prepare("SELECT COUNT(*) AS count FROM tl_member WHERE " 
            						. ($arrValidMembers ? "id IN (" . join(",", $arrValidMembers) . ") AND " : "") 
            						. $searchObj['where']) 
            				->execute($searchObj['values']);

            $objMemberStmt = \Database::getInstance()
            				->prepare("SELECT id, alias,username, publicFields, `groups`" 
            						. (count($arrFieldsList) > 0 ? ", " . implode(", ", $arrFieldsList) : " ")
            						. " FROM tl_member WHERE " 
            						. ($arrValidMembers ? "id IN (" . join(',', $arrValidMembers) . ") AND " : "") 
            						. $searchObj['where'] 
            						. ($order_by ? " ORDER BY " . $order_by : ""));
        }
        // Limit
        
        if ($per_page)
        {
            $objMemberStmt->limit($per_page, (($page - 1) * $per_page));
        }

        $memberCollection = $objMemberStmt->execute($searchObj['values']);
        // Prepare URL
        $strUrl = preg_replace('/\?.*$/', '', \Environment::get('request'));
        $this->Template->url = $strUrl;

        $this->Template->baseurl = $strUrl;

        $blnQuery = false;

        // Add GET parameters
        foreach (preg_split('/&(amp;)?/', $_SERVER['QUERY_STRING']) as $fragment)
        {
            if (strlen($fragment) && strncasecmp($fragment, 'order_by', 8) !== 0 && strncasecmp($fragment, 'sort', 4) !== 0 && strncasecmp($fragment, 'page', 4) !== 0)
            {
                $strUrl .= (!$blnQuery ? '?' : '&amp;') . $fragment;
                $blnQuery = true;
            }
        }

        $strVarConnector = $blnQuery ? '&amp;' : '?';

        // Prepare table
        $arrTh = array();
        $arrTd = array();
        $arrItem = array();

        // THEAD

        //Tabellenheader erzeugen

        $tabfields = $this->fieldsInList;
        for ($i = 0; $i < count($tabfields); $i++)
        {
            $thisField = $tabfields[$i]['field'];
            if ($thisField != "cm_allowmap")
            {
                $class = '';
                $sort = 'asc';
                if ($thisField == "dist")
                {
                    $strField = $GLOBALS['TL_LANG']['MSC']['cm_distance'];
                } else
                {
                    $strField = strlen($label = $GLOBALS['TL_DCA']['tl_member']['fields'][$thisField]['label'][0]) ? $label : $thisField;
                }

                if (\Input::get('order_by') == $thisField)
                {
                    $sort = (\Input::get('sort') == 'asc') ? 'desc' : 'asc';
                    $class = ' sorted ' . \Input::get('sort');
                }

                $arrTh[] = array(
                    'link' => $strField, 
                    'href' => (ampersand($strUrl) . $strVarConnector . 'order_by=' . $thisField) . '&amp;sort=' . $sort, 
                    'title' => specialchars(sprintf($GLOBALS['TL_LANG']['MSC']['list_orderBy'], $strField)), 
                    'class' => $class . 'col_' . $i . (($i == 0) ? ' col_first' : ''));
            }
        }
        //---------------------------------------
        $start = -1;
        $limit = $memberCollection->count();
        // TBODY
        while ($memberCollection->next())
        {
            $publicFields = \StringUtil::deserialize($memberCollection->publicFields, true);
            $allowAlias = \in_array('firstname', $publicFields)
                && \in_array('lastname', $publicFields)
                && \in_array('city', $publicFields);
			
            $detailUrlPart = ((\Config::get('useAutoItem') && !\Config::get('disableAlias')&&$memberCollection->alias&&$allowAlias) ?  '/'. $memberCollection->alias:'/'.$this->detailParam.'/'.$memberCollection->id );	
                
            //$detailUrl = $this->Template->url . '?show=' . $row["id"];
            $detailUrl = $this->generateFrontendUrl($objPage->row(), $detailUrlPart, $objPage->language);

            //---------------------------------------
            $groupids = \StringUtil::deserialize($memberCollection->groups, true);
            $iconstd = null;
            
            $iconnear = null;
            unset($groups);
            foreach ($groupids as $i => $gid)
            {
                if ($objGroupNames[$gid])
                    $groups[] = $objGroupNames[$gid]["name"];
                if (!$iconstd && $objGroupNames[$gid]["iconstd"])
                {
                    $iconstd=array();
                    $iconstd['url'] = \cm_Maps\cm_Map_lib::getMarkerIcon($objGroupNames[$gid]["iconstd"]);
                	$iconstd['icon_anchor'] = $objGroupNames[$gid]["iconstd_anchor"];
                	$iconstd['popup_anchor'] = $objGroupNames[$gid]["iconstdpopup_anchor"];
                }
                if ($this->cm_map_nearmarker && !$iconnear)
                {
                    $iconnear=array();
                    $iconnear['url'] = \cm_Maps\cm_Map_lib::getMarkerIcon($objGroupNames[$gid]["iconnear"]);
                    $iconnear['icon_anchor'] = $objGroupNames[$gid]["iconnear_anchor"];
                    $iconnear['popup_anchor'] = $objGroupNames[$gid]["iconnearpopup_anchor"];
                }
            }
            //die($this->cm_map_nearmarker?"true":"false");
            if (!$this->cm_map_nearmarker)
            {
                $iconnear=$iconstd;
            }
            $groupsstr = implode(", ", $groups);
            //---------------------------------------

            $x = $memberCollection->row();
            $dist = "";
            $diststr = "-";

            $near = $hasLocal && ((int)$max_dist>0) && ($memberCollection->dist == "-" 
            || $memberCollection->dist <= $max_dist);

            if ($hasLocal)
            {
                if ($memberCollection->dist != "-")
                {
                    $memberCollection->dist = round($memberCollection->dist, ($dist < 10 ? 1 : 0));
                    $memberCollection->dist = number_format($memberCollection->dist, $memberCollection->dist < 10 ? 1 : 0, $GLOBALS['TL_LANG']['MSC']['decimalSeparator'], $GLOBALS['TL_LANG']['MSC']['thousandsSeparator']);
                }
            }

            $class = 'row_' . ++$start . (($start == 0) ? ' row_first' : '') . ((($start + 1) == $limit) ? ' row_last' : '') . ((($start % 2) == 0) ? ' even' : ' odd') . ($near ? ' near' : ' ');
            if (!$hasLocal || !$max_dist || $this->cm_map_showfar || $near)
            {
                $arrData = $memberCollection->row();
                //---------------------------------------
                //use new array
                $tabfields = $this->fieldsInList;
                foreach ($tabfields as $k => $m)
                {
                    $v = $m['field'];
                    if ($v=="website" && ($memberCollection->$v))
                    {
                        if (preg_match('@^(https://)@i', $memberCollection->$v))	
                        {
                            $memberCollection->$v = substr($memberCollection->$v,8);
                        }
                        elseif (preg_match('@^(http://)@i', $memberCollection->$v)) {
                            $memberCollection->$v = substr($memberCollection->$v,7);
                        }
                    }					
                    if (($v != "cm_coords") && ($v != "cm_allowmap"))
                    {
                        $value = '-';

                        if ($v == 'username' || \in_array($v, $publicFields))
                        {
                            $value = $this->formatValue($v, $memberCollection->$v);
                        }
                        if ($v == 'dist')
                        {
                            $value = $memberCollection->$v;
                        }
                        //  				$arrData = $objMember->row();
                        unset($arrData['publicFields']);
						
                        $arrTd[$class][$k] = array(
                            'raw' => $arrData, 
                            'groups' => $groupsstr, 
                            'iconstd' => $iconstd, 
                            'content' => $value, 
                            'class' => 'col_' . $k . (($k == 0) ? ' col_first' : ''), 
                            'id' => $memberCollection->id, 
                            'field' => $v,
                            'url' => $detailUrl,
                        //'dist' => $diststr,
                            'near' => $near);
                    }
                }

                //data for Google map etc
                //-------------------------------

                foreach ($arrFieldsList as $k => $v)
                {
                    if (($v != "cm_coords") && ($v != "cm_allowmap"))
                    {
                        $value = '-';

                        if ($v == 'username' || \in_array($v, $publicFields))
                        {
                            $value = $this->formatValue($v, $memberCollection->$v);
                        }

                        //  				$arrData = $objMember->row();

                        unset($arrData['publicFields']);
                        $arrItem[$class][$k] = array(
                            'raw' => $arrData, 
                            'groups' => $groupsstr, 
                            'iconstd' => $iconstd,
                            'iconnear' => $iconnear,
                            'content' => $value, 
                            'class' => 'col_' . $k . (($k == 0) ? ' col_first' : ''), 
                            'id' => $memberCollection->id, 
                            'field' => $v,
                            'allowAlias' =>$allowAlias,
                        	                        //'dist' => $diststr,
                            'near' => $near);
                    }
                }

                //-------------------------------
            }
        }

        $this->Template->notFound = ($limit == 0);  
        
        $routeToTextPattern = '<a onclick="window.open(this.href); return false;" '
                     .'href="'.($this->useSSL?'https://':'http://').'maps.google.com/maps?saddr=&ie=UTF8&hl=de&daddr='
                        .'%s">'.$GLOBALS['TL_LANG']['MSC']['cm_map_getroutetable'].'</a>';

        $this->Template->detailParam = $this->detailParam;

        $this->Template->tbody = $arrTd;
        $this->Template->tbody = $arrItem;
        $this->ElementTemplate->col_last = 'col_' . ++$k;
        $this->ElementTemplate->thead = $arrTh;
        $this->ElementTemplate->tbody = $arrTd;
        $this->ElementTemplate->cm_memberlist_hidedetaillink = $this->cm_memberlist_hidedetaillink;
        $this->ElementTemplate->cm_map_routetotable = $this->cm_map_routetotable;

        $this->ElementTemplate->url = $this->Template->url;
        $this->ElementTemplate->tdDist = $GLOBALS['TL_LANG']['MSC']['cm_distance'];
        $this->ElementTemplate->routeToTextPattern=$routeToTextPattern;

        // Pagination
        $objPagination = new \Pagination($objTotal->count, $per_page);
        $this->Template->pagination = $objPagination->generate("\n  ");
        $this->Template->per_page = $per_page;
        if ($this->cm_shownumberofadresses)
        {
            $this->Template->numberOfAddresses = sprintf(specialchars($GLOBALS['TL_LANG']['MSC']['cm_numberofmembers']),$objTotal->count);
        }
        $this->ElementTemplate->numberOfAddresses = sprintf(specialchars($GLOBALS['TL_LANG']['MSC']['cm_numberofmembers']),$objTotal->count);

        // Template variables
        $this->Template->action = \Environment::get('indexFreeRequest');
        $this->Template->search_label = specialchars($GLOBALS['TL_LANG']['MSC']['search']);
        $this->Template->per_page_label = specialchars($GLOBALS['TL_LANG']['MSC']['list_perPage']);
        $this->Template->fields_label = $GLOBALS['TL_LANG']['MSC']['all_fields'][0];
        $this->Template->keywords_label = $GLOBALS['TL_LANG']['MSC']['keywords'];
        $this->Template->plz_search = $GLOBALS['TL_LANG']['MSC']['cm_plz_search'];
        $this->Template->plzarea_label = $GLOBALS['TL_LANG']['MSC']['plzarea'];

        $this->Template->search = \Input::get('search');
        $this->Template->for = \Input::get('for');
        $this->Template->order_by = \Input::get('order_by');
        $this->Template->sort = \Input::get('sort');
        $this->Template->tdDist = $GLOBALS['TL_LANG']['MSC']['cm_distance'];
        $this->Template->textOnEmpty = $this->cm_memberlist_notfound;

        $this->Template->showtable = true;

        $infoTemplate = new \FrontendTemplate($this->strTemplateInfo);
        // store googlemaps coordinates

        if ($this->cm_map_onlist)
        {
            try
            {
                $googleMapCoords = array();
				
                foreach ($this->Template->tbody as $class => $row)
                {
                   if ((count($row) > 0) && $row[0]["raw"]["cm_allowmap"] && $row[0]["raw"]["cm_coords"] && $row[0]["raw"]["cm_coords"] != "0,0")
                    {
                        $rawtmp[]= array();
                        $rawtmp['id'] = $row[0]["raw"]['id'];
                        foreach ($this->arrMlFields as $key) {
                          $rawtmp[$key] = $row[0]["raw"][$key];	
                        }
                        array_push($googleMapCoords, array(
                            "raw" => $rawtmp, // $row[0]["raw"], 
                            "name" => $row[0]["raw"]["lastname"],
                            "groups" => $row[0]["groups"], 
                            'iconstd' => $row[0]["iconstd"],
                            'iconnear' => $row[0]["iconnear"],
                            "coords" => $row[0]["raw"]["cm_coords"],
                            "id" => $row[0]["raw"]["id"],
                            "alias" => $row[0]["raw"]["alias"],
                            "allowAlias" => $row[0]['allowAlias'],
                            "near" => $row[0]["near"]
                        ));
                    }
                }
                if ($this->cm_map_setstylelist && $this->cm_map_styleidlist)
                {
                    $objMapStyles = \Database::getInstance()
                    	->prepare("SELECT * FROM tl_cm_gmaplayout WHERE id = ?") 
                    	->execute($this->cm_map_styleidlist);

                    $objMapStyles->next;
                    $GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/cm_maps/assets/' . $objMapStyles->name . '.mst';
                    $this->cm_map_styleListName = 'cmMapStyle_' . $objMapStyles->id;
                }
       //         if (count($googleMapCoords) == 0) throw new \Exception();
																			    
                if (count($googleMapCoords)==0 && !$this->cm_map_showmaponempty) throw new \Exception();
 
                //-map generieren
                switch ($this->cm_map_poslist)
                {
                    case 'above' :
                    case 'left' :
                    case 'right' :
                        $this->mappos = 'above';
                        break;
                    default :
                        $this->mappos = 'below';
                        break;
                }
                $mapID = "cm_map" . $this->mappos . "_" . $this->id;
                $this->ElementTemplate->mapID=$mapID;
//                $indiv = $this->cm_map_autozoomlist;
                $indiv = $this->cm_map_indivcenterlist;
                if ($indiv)
                {
                    $center = $this->cm_map_centerlist;
                }
                $zoom = $this->cm_map_zoomlist;
				

                if (count($googleMapCoords)==0) 
                {
                    $center=$this->cm_map_centerempty;
                    $zoom=$this->cm_map_zoomempty;
                    $indiv=true;
                    $this->Template->notFound = true;
                }
                $clusterStyles='';
		if ($this->cm_map_cluster) 
                {
                    $clusterGridSize=($this->cm_map_cluster_gridsize?$this->cm_map_cluster_gridsize:20);
                    $clusterMaxZoom=($this->cm_map_cluster_maxzoom?$this->cm_map_cluster_maxzoom:15);

                    if ($this->cm_map_clusterlayoutid && $this->cm_map_clusterlayoutid>0)
                    {
                        $clusterLayout = new \cm_Maps\ClusterLayout();
                        $clusterStyles = $clusterLayout->compileDefinition($this->cm_map_clusterlayoutid);
                    }
                }  

                $googleMap = $this->getGoogleMapCodeList( $googleMapCoords, $indiv, $zoom, $center, $mapID, $this->cm_map_disablewheellist, $max_dist, $hasLocal,$lat, $lng, $showCircle,
                $this->cm_map_cluster,$clusterGridSize,$clusterMaxZoom,$clusterStyles, null,$this->api,$this->apiKey);

                $this->Template->cookiebarInstalled=$googleMap->settings['cookiebarInstalled'];
                $this->Template->acceptanceButtontext=$googleMap->settings['cm_acceptance_buttontext'];
                $this->Template->acceptanceText=$googleMap->settings['cookiebarInstalled'] > 0 ? $googleMap->settings['cm_acceptance_text'] : $googleMap->acceptanceText;
                $this->Template->acceptanceRequired=$googleMap->acceptanceRequired;
                $this->Template->BaseScriptCode =$googleMap->BaseJsScript;
                $this->Template->clusterMarkers =$this->cm_map_cluster==true;
		if ($this->cm_map_cluster) 
                {
                    $markerClusterJs =\cm_Maps\cm_Map_lib::getMarkerClusterScript($this->useSSL,$language,$this->api,$this->apiKey); 
                    $this->Template->MarkerClusterScriptCode=$markerClusterJs;
                    $googleMap->MarkerClusterJsScript = $markerClusterJs;
//                    $markerClusterCss=\cm_Maps\cm_Map_lib::getMarkerClusterCss($this->useSSL,$language,$this->api,$this->apiKey);
//                    $this->Template->MarkerClusterCssCode=$markerClusterCss;
//                    $markerClusterCssScript='';
//                    foreach ($markerClusterCss as $cssUrl) {
//                        $markerClusterCssScript .= "$('<link>').appendTo('head').attr({type: 'text/css',rel: 'stylesheet',href: '".$cssUrl."'";
//                    }
//                    $googleMap->MarkerClusterCssScript = $markerClusterCssScript;
                    //$this->Template->ClusterScriptCode =$googleMap->ClusterJsScript;
                }
                $this->Template->GoogleMapCode = $googleMap->MainJsScript.$googleMap->parse();
                
                //         $this->Template->showtable = !$this->cm_membergooglemaps_showtableonsearch
                //                                       ||($this->cm_membergooglemaps_showtableonsearch
                //                                       && $this->Template->search);
                $isSearch =  ($this->Template->for 
                                || $this->useTags 
                                || $this->Template->visitorlocation
                                || $this->Template->plzarea);
                $this->Template->showtable = !$this->cm_membergooglemaps_hidetable 
                                || ($this->cm_membergooglemaps_showtableonsearch && $isSearch);
                //-------------------------------
                if ($this->cm_map_heightlist)
                {
                    $heightArr = \StringUtil::deserialize($this->cm_map_heightlist);
                    if ($heightArr["value"])
                    {
                        $this->Template->mapstyle = "height:" . $heightArr["value"] . $heightArr["unit"];
                    }
                }
            } 
            catch (\Exception $e)
            {
                $mapID = "";
                $this->Template->GoogleMapCode = "";
                $this->Template->acceptanceRequired=false;
                $this->Template->acceptanceText="";
            }
        } 
        else
        {
            $mapID = "";
            $this->Template->GoogleMapCode = "";
            $this->Template->acceptanceRequired=false;
            $this->Template->acceptanceText="";
            // $this->Template->showtable=true; //set on top as default
        }
        $this->Template->mappos = $this->mappos;
        $this->Template->mapID = $mapID;
        $this->Template->listElements = preg_replace('/[\r|\n]*/', '', $this->ElementTemplate->parse());
    }

    /**
     * List a single member
     * @param integer
     */
    protected function listSingleMember($idOrAlias)
    {
        \Contao\Controller::loadDataContainer('tl_member');
        
        if ($this->map_dtltemplate) $this->strTemplateDetail = $this->map_dtltemplate;
		
        global $objPage;
        
        $time = time();
        $this->Template = new \FrontendTemplate($this->strTemplateDetail);
        $this->Template->record = array();

        // Get member
        // 		$objMember = $this->Database->prepare("SELECT * FROM tl_member WHERE id=? AND disable!=1 AND (start='' OR start<=$time) AND (stop='' OR stop>=$time)")
        // 									->limit(1)
        // 									->execute($id);
	
        $objMember = MemberGoogleMapsMemberModel::findByIdOrAlias($idOrAlias);
        //print_r($objMember);
        //    die("xxxx1");
        // No member found or group not allowed
	if (null == $objMember || count(array_intersect(\StringUtil::deserialize($objMember->groups, true), $this->arrMlGroups)) < 1)
        {
            $this->Template->invalid = $GLOBALS['TL_LANG']['MSC']['invalidUserId'];

            // Do not index the page
            $objPage->noSearch = 1;
            $objPage->cache = 0;

            // Send 404 header
            header('HTTP/1.1 404 Not Found');
            return;
        }

        //---------------------------------------
        //    $objGroupNames=$this->getGroupNamesAndIcons();
        $objGroupNames = MemberGoogleMapsMemberGroupModel::getGroupNamesAndIcons($this->arrMlGroups);

        $groupids = \StringUtil::deserialize($objMember->groups, true);
        $iconstd = null;
        unset($groups);

        foreach ($groupids as $i => $gid)
        {
            if ($objGroupNames[$gid])
                $groups[] = $objGroupNames[$gid]["name"];
            if (!$iconstd)
            {
                $iconstd['url'] = \cm_Maps\cm_Map_lib::getMarkerIcon( $objGroupNames[$gid]["iconstd"]);
                $iconstd['icon_anchor'] = $objGroupNames[$gid]["iconstd_anchor"];
                $iconstd['popup_anchor'] = $objGroupNames[$gid]["iconstdpopup_anchor"];
            }

        }

        $groupsstr = implode(", ", $groups);

        //---------------------------------------

        // Default variables
        $this->Template->action = \Environment::get('indexFreeRequest');
        $this->Template->referer = 'javascript:history.go(-1)';
        $this->Template->back = $GLOBALS['TL_LANG']['MSC']['goBack'];
        $this->Template->publicProfile = sprintf($GLOBALS['TL_LANG']['MSC']['publicProfile'], $objMember->lastname);
        $this->Template->noPublicInfo = $GLOBALS['TL_LANG']['MSC']['noPublicInfo'];
        $this->Template->sendEmail = $GLOBALS['TL_LANG']['MSC']['sendEmail'];
        $this->Template->submit = $GLOBALS['TL_LANG']['MSC']['sendMessage'];
        $this->Template->loginToSend = $GLOBALS['TL_LANG']['MSC']['loginToSend'];
        $this->Template->emailDisabled = $GLOBALS['TL_LANG']['MSC']['emailDisabled'];

        // Confirmation message
        if ($_SESSION['TL_EMAIL_SENT'])
        {
            $this->Template->confirm = $GLOBALS['TL_LANG']['MSC']['messageSent'];
            $_SESSION['TL_EMAIL_SENT'] = false;
        }

        // Check personal message settings
        switch ($objMember->allowEmail)
        {
            case 'email_all' :
                $this->Template->allowEmail = 3;
                break;

            case 'email_member' :
                $this->Template->allowEmail = FE_USER_LOGGED_IN ? 3 : 2;
                break;

            default :
                $this->Template->allowEmail = 1;
                break;
        }

        // No e-mail address given
        if (!strlen($objMember->email))
        {
            $this->Template->allowEmail = 1;
        } else
        {
            if (!version_compare(VERSION, '3.5', '<'))
            {
                $value = \StringUtil::encodeEmail($objMember->email);
            }
            else 
            {
                $value = \String::encodeEmail($objMember->email);
            }
            $value = '<a href="mailto:' . $value . '">' . $value . '</a>';
            $this->Template->emailtext = $value;
        }

        // Handle personal messages
        if ($this->Template->allowEmail > 1)
        {
            $arrFieldEmail = array(
                'name' => 'replytoemail', 
                'label' => $GLOBALS['TL_LANG']['MSC']['cm_email'], 
                'inputType' => 'text', 
                'eval' => array('mandatory' => true, 'required' => true, 'rgxp' => 'email')
            );
            $arrField = array(
                'name' => 'message', 
                'label' => $GLOBALS['TL_LANG']['MSC']['message'], //['message'],
                'inputType' => 'textarea', 
                'eval' => array('mandatory' => true,'required' => true, 'rows' => 4, 'cols' => 40, 'decodeEntities' => true)                
            );
            $arrFieldCaptcha = array(
                'name' => 'captcha', 
                'label' => $GLOBALS['TL_LANG']['MSC']['cm_captcha'], 
                'inputType' => 'captcha',
                'eval' => array('mandatory' => true)
            );

            $arrWidget = \Widget::getAttributesFromDca($arrField, $arrField['name'], '');
            $objWidget = new \FormTextArea($arrWidget);

            $arrEmailWidget = \Widget::getAttributesFromDca($arrFieldEmail, $arrFieldEmail['name'], '');
            $objEmailWidget = new \FormTextField($arrEmailWidget);
            
            $arrCaptchaWidget = \Widget::getAttributesFromDca($arrFieldCaptcha, $arrFieldCaptcha['name'], '');
            $objCaptchaWidget = new \FormCaptcha($arrCaptchaWidget);
            
            if ($this->cm_email_nameRequired)
            {
                $arrFieldName = array(
                    'name' => 'name', 
                    'label' => $GLOBALS['TL_LANG']['MSC']['cm_name'], 
                    'inputType' => 'text', 
                    'eval' => array('mandatory' => true, 'required' => true)
                );
                $arrNameWidget = \Widget::getAttributesFromDca($arrFieldName, $arrFieldName['name'], '');
                $objNameWidget = new \FormTextField($arrNameWidget);
            }
            

            // Validate widget
            if (\Input::post('FORM_SUBMIT') == 'tl_send_email')
            {
                $objWidget->validate();
                if ($this->cm_email_nameRequired){
                   $objNameWidget->validate();
                }
                else 
                {
                   $objNameWidget=null;
                }
                $objEmailWidget->validate();
                $objCaptchaWidget->validate();

                if (!$objCaptchaWidget->hasErrors() && !$objWidget->hasErrors() && (!$this->cm_email_nameRequired || !$objNameWidget->hasErrors()) && !$objEmailWidget->hasErrors())
                {
                    $this->cm_sendPersonalMessage($objMember, $objWidget, $objNameWidget, $objEmailWidget);
                }
            }

            $this->Template->widget = $objWidget;
            if ($this->cm_email_nameRequired)
            {
              $this->Template->namewidget = $objNameWidget;
            }
            $this->Template->emailwidget = $objEmailWidget;
            $this->Template->captchawidget = $objCaptchaWidget->parse();
            $this->Template->submit = $GLOBALS['TL_LANG']['MSC']['sendMessage'];
        }

        $arrFieldsTmp = \StringUtil::deserialize($objMember->publicFields);
        $arrModuleFields = $this->arrMlFields;
        $arrFields = array_intersect($this->arrMlFields, $arrFieldsTmp);

            $hasExternalMap = MemberGoogleMapsMemberModel::hasMapPlaceholder($objMember->id);
            $showMap = false;
            $mapID = false;
            $acceptanceSettings=array('required'=>false,'text'=>'undefined');
	if ($this->cm_map_ondetail && $objMember->cm_allowmap)
        {
            if (\cm_Maps\cm_Map_lib::validateCoordsExact($objMember->cm_coords))
            {
            	if ($hasExternalMap)
                {$this->mappos="ext";}
                else 
                {
                    switch ($this->cm_map_posdetail)
                    {
                        case 'above' :
                        case 'left' :
                        case 'right' :
                            $this->mappos = 'above';
                            break;
                        default :
                            $this->mappos = 'below';
                            break;
                    }
                }
                if ($this->cm_map_setstyledetail && $this->cm_map_styleiddetail)
                {
                    $objMapStyles = \Database::getInstance()
                                        ->prepare("SELECT * FROM tl_cm_gmaplayout WHERE id = ?")
                                        ->execute($this->cm_map_styleiddetail);

                    $objMapStyles->next;
                    $GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/cm_maps/assets/' . $objMapStyles->name . '.mst';
                    $this->cm_map_styleDetailName = 'cmMapStyle_' . $objMapStyles->id;
                }

                $mapID = "cm_map" . $this->mappos . "_" . $this->id;
                $center = $objMember->cm_map_indivcenter ? $objMember->cm_map_center : $objMember->cm_coords;
                $zoom = $objMember->cm_map_indivzoom ? $objMember->cm_map_zoom : $this->cm_map_stdzoom;
                if ($zoom == "")
                    $zoom = $this->cm_map_stdzoom;

                $tempCoords = $objMember->cm_lat . "," . $objMember->cm_lng;
                $member = array('raw' => $objMember, 'company' => $objMember->company, 'firstname' => $objMember->firstname, 'lastname' => $objMember->lastname, 'postal' => $objMember->postal, 'street' => $objMember->street, 'h_nr' => $objMember->h_nr, 'city' => $objMember->city, 'website' => $objMember->website, );

                //$googleMap= new \cm_Maps\Map(false, $this->api);
                $googleMap = $this->getGoogleMapCode($member, $tempCoords, $center, $zoom, $iconstd, $mapID, $this->cm_map_disablewheeldetail,$this->api,$this->apiKey);
                
                $acceptanceSettings['required']=$googleMap->acceptanceRequired;
                $acceptanceSettings['text']=$googleMap->acceptanceText;
                $showMap = true;
                $this->Template->cookiebarInstalled=$googleMap->settings['cookiebarInstalled'];
                $this->Template->acceptanceButtontext=$googleMap->settings['cm_acceptance_buttontext'];
                $this->Template->acceptanceText=$googleMap->settings['cookiebarInstalled'] > 0 ? $googleMap->settings['cm_acceptance_text'] : $googleMap->acceptanceText;
                $this->Template->acceptanceRequired=$googleMap->acceptanceRequired;
		$this->Template->BaseScriptCode = $googleMap->BaseJsScript;
		$this->Template->GoogleMapCode = $googleMap->MainJsScript.$googleMap->parse();
				
            } else
            {
                //$this->Template->GoogleMapCode = $GLOBALS['TL_LANG']['MSC']['cm_membergooglemaps_error'];
                $this->Template->GoogleMapCode = '';
                $this->Template->acceptanceRequired=false;
                $this->Template->acceptanceText="";
            }
            if ($this->cm_map_heightdetail)
            {
                $heightArr = \StringUtil::deserialize($this->cm_map_heightdetail);
                if ($heightArr["value"])
                {
                    $this->Template->mapstyle = "height:" . $heightArr["value"] . $heightArr["unit"];
                }
            }
        } else
        {
            $this->Template->GoogleMapCode = "";
            $this->Template->acceptanceRequired=false;
            $this->Template->acceptanceText="";
        }

        $this->Template ->details = '';

        $objElement = MemberGoogleMapsMemberModel::findPublishedContent($objMember->id);
        $hasExternalMap=false;
        if ($objElement !== null)
        {
            while ($objElement->next())
            {
                $element = $objElement->current();
                switch ($element->type)
                {
                    case 'cm_mapPlaceholder': 
                            if ($showMap)
                            {
                                    $element->mapid = $mapID;
                                    $element->acceptanceRequired=$acceptanceSettings['required'];
                                    $element->acceptanceText=$acceptanceSettings['text'];
                                    $renderedElement= $this->getContentElement($element);
                                    $this->Template->details .= $renderedElement;
                                    //$this->Template->details .= "--- vor diesem Eintrag soll die Karte erscheinen!";
                            }
                            else {
                                    $this->Template->details .= '<!-- no map -->';
                            }
                            $hasExternalMap=true;
                            break;
                    default: $this->Template->details .= $this->getContentElement($element); 
                            break;
                }
            }
        }

        $this->Template->mappos = $this->mappos;
        $this->Template->mapID = $mapID;

        // Add public fields
        if (\is_array($arrFields) && count($arrFields))
        {
            $count = -1;

            foreach ($arrFields as $k => $v)
            {
                if ($v=="website" && ($objMember->$v))
                {
                    if (preg_match('@^(https://)@i', $objMember->$v))	
                    {
                        $objMember->$v = substr($objMember->$v,8);
                    }
                    elseif (preg_match('@^(http://)@i', $objMember->$v)) {
                        $objMember->$v = substr($objMember->$v,7);
                    }
                }	
                $class = 'row_' . ++$count . (($count == 0) ? ' row_first' : '') . (($count >= (count($arrFields) - 1)) ? ' row_last' : '') . ((($count % 2) == 0) ? ' even' : ' odd');
				
                $arrFields[$k] = array(
                    'raw' => $objMember->row(), 
                    'groups' => $groupsstr, 
                    'iconstd' => $iconstd, 
                    'content' => $this->formatValue($v, $objMember->$v, true), 
                    'class' => $class, 
                    'label' => (strlen($label = $GLOBALS['TL_DCA']['tl_member']['fields'][$v]['label'][0]) ? $label : $v), 
                    'field' => $v);
            }
            $this->Template->record = $arrFields;
        }
		
    }

    protected function getGoogleMapCodeList($rows, $indiv, $zoom, $center, $mapID, 
            $disablewheel, $max_dist, $hasLocal, $lat, $lng, $showCircle,
            $cluster,$cluster_gridSize,$cluster_maxZoom,$clusterStyles, 
            $detailPgId,$map_api,$map_apikey)
    {
//         print_r($rows);
//         die("rows");
    	if ($this->map_inftemplate) $this->strTemplateInfo = $this->map_inftemplate;

        global $objPage;
        if (!$detailPgId || ($detailPg = \PageModel::findByPk($detailPgId)) == null) {
           $detailPg = $objPage;
        }
        $language=$detailPg->language;

        $googleMap = new \cm_Maps\Map(false, $map_api);
        $googleMap->singleView = false;
        $googleMap->layout = $this->cm_map_styleListName;

        $googleMap->language = $language;
        $googleMap->mapType = \cm_Maps\cm_Map_lib::getMapTypeStr($this->cm_map_maptypelist);
        $googleMap->showTypePanel = $this->cm_map_choosetypelist;

        //$param_refid = $this->google_api_key;
        //$root_id = $this->getRootIdFromUrl();
        $root_id = \Frontend::getRootPageFromUrl()->id;
        $root_details = \PageModel::findWithDetails($root_id);
//         $map_apikey = $root_details->cm_map_apikey;
//         if (!$map_apikey) $map_apikey=\Config::get('cm_map_apikey');
		
        $baseJsScript = \cm_maps\cm_Map_lib::getBaseScript($this->useSSL, $language,$map_api,$map_apikey);
        $clickType = $this->cm_map_clicktype;
        $mainJsScript = \cm_Maps\cm_Map_lib::getMarkerBaseScript($map_api,$this->useSSL,false, $clickType, $this->cm_map_staybubbleopened);
        $clusterCssScripts = null;
        if ($cluster) 
        {
            $googleMap->MarkerClusterJsScript =\cm_Maps\cm_Map_lib::getMarkerClusterScript($this->useSSL,$language,$this->api,$this->apiKey); 
            $markerClusterCss=\cm_Maps\cm_Map_lib::getMarkerClusterCss($this->useSSL,$language,$this->api,$this->apiKey);
            $this->Template->MarkerClusterCssCode=$markerClusterCss;
            $markerClusterCssScript='';
            foreach ($markerClusterCss as $cssUrl) {
                $markerClusterCssScript .= "$('<link>').appendTo('head').attr({type: 'text/css',rel: 'stylesheet',href: '".$cssUrl."'});";
            }
            $googleMap->MarkerClusterCssScript = $markerClusterCssScript;
        }
        $acceptanceRequired=false;
        $acceptanceText='';
        switch($this->cm_gm_acceptance_required)
        {
            case 'on':  $acceptanceRequired = true;
                        $acceptanceText=$this->cm_gm_acceptance_text;
                        break;
            case 'off': $acceptanceRequired = false;
                        $acceptanceText="";
                        break;
            case 'page':
            default:    $acceptanceRequired = $root_details->cm_gm_acceptance_required??false;
                        $acceptanceText = $root_details->cm_gm_acceptance_required?$root_details->cm_gm_acceptance_text:'';
                        break;
        }
        $googleMap->initCookieBarSettings($root_details);
        $googleMap->acceptanceRequired = $acceptanceRequired;
        $googleMap->acceptanceText = $acceptanceText;
        
        $googleMap->settings = array(
            'disableWheel' => $disablewheel,
            'center' => $center, 
            'cm_map_choosetypelist' => $this->cm_map_choosetypelist, 
            'ctrlTypeStr' => \cm_Maps\cm_Map_lib::getCtrlTypeStr($this->cm_map_ctrltypelist),
    //      'cm_map_choosenavlist' => $this->cm_map_choosenavlist,
    //      'ctrlNavStr' => cm_Map_lib::getCtrlNavStr($this->cm_map_ctrlnavlist),
            'cm_map_choosezoomlist' => $this->cm_map_choosezoomlist, 
            'ctrlZoomStr' => \cm_Maps\cm_Map_lib::getCtrlZoomStr($this->cm_map_ctrlzoomlist), 
            'mapType' => \cm_Maps\cm_Map_lib::getMapTypeStr($this->cm_map_maptypelist), 
            'zoom' => $zoom, 
            'mapID' => $mapID, 
            'showTypePanel' => $this->cm_map_choosetypelist, 
            'lat' => $lat, 
            'lng' => $lng, 
            'indiv' => $indiv,
            'fitToCircle' => $this->cm_fitToCircle,
            'hasLocal' => $hasLocal, 
            'routetodetail' => false, 
            'clusterMarkers' => $cluster,
            'clusterGridSize' => $cluster_gridSize,
            'clusterMaxZoom' => $cluster_maxZoom,
            'clusterStyles' => $clusterStyles,
            'cm_ElementOnClickRequiresResize' => str_replace('&#35;','#',$this->cm_ElementOnClickRequiresResize),
			'clickTypeEvent' => $clickType==2?'click':'mouseover'
        );		
        $googleMap->settings['cm_acceptance_text'] = \StringUtil::toHtml5($root_details->cm_gm_acceptance_text);
        $googleMap->settings['cm_acceptance_text'] = preg_replace('/[\t\n\r]+/', ' ', $googleMap->settings['cm_acceptance_text']);

        $googleMap->settings['cm_acceptance_buttontext'] = $root_details->cm_acceptance_buttontext;
        
        unset($googleMap->markerData);

        $data = array();
        foreach ($rows as $row)
        {
			//print_r($row["raw]"]); die();
            if (!\cm_Maps\cm_Map_lib::validateCoordsExact($row["coords"]))
            {
                $data = array();
            } else
            {
                $detailUrlPart = (
                    (\Config::get('useAutoItem') 
                        && !\Config::get('disableAlias')
                        && $row["alias"]
                        && $row["allowAlias"]
                    ) ?  
                    '/'. $row["alias"] : 
                    '/'.$this->detailParam.'/'.$row["id"] );	
                
                $detailUrl = $this->generateFrontendUrl($detailPg->row(), $detailUrlPart, $language);

                $infoTemplate = new \Contao\FrontendTemplate($this->strTemplateInfo);
                $infoTemplate->setData($row);
                $infoTemplate->linktowebsite = $this->cm_map_linktowebsite;
//                $infoTemplate->urldetail = $this->Template->url . ($this->cm_membergooglemaps_hidedetaillink ? '' : '?show=' . $row["id"]);
                $infoTemplate->urldetail = $this->generateFrontendUrl($detailPg->row(), ($this->cm_membergooglemaps_hidedetaillink ? '' : $detailUrlPart), $language);
                $infoText = '';
                if ($this->cm_map_routetolist)
                {
                    $infoText = '<a onclick="window.open(this.href); return false;" href="'.($this->useSSL?'https://':'http://').'google.com/maps?saddr=&ie=UTF8&hl=de&daddr='
                        .$row['coords'].'">'.$GLOBALS['TL_LANG']['MSC']['cm_map_getroutelist'].'</a>';
//                    $infoText = $GLOBALS['TL_LANG']['MSC']['cm_map_getroute'] . ": ";
                }
                $infoTemplate->routetext = $infoText;

                $infotext = preg_replace('/[\r|\n]*/', '', addslashes($infoTemplate->parse()));
                $data = array('infotext'  => $infotext,
                                'cm_coords' => $row["coords"],
                                'icon'      => $row[($this->cm_map_nearmarker && $row["near"] == 1) ? "iconnear" : "iconstd"],
                                'color'     => ($this->cm_map_nearmarker && $row["near"] == 1) ? "blue" : "",
                                'url'       => $detailUrl,
                                'data'      => $row['raw'],
                                'clickable' => !$this->cm_membergooglemaps_hidedetaillink,
                                'near'      => $row["near"]
                            );
            }
            $googleMap->markerData[] = $data;
        }

        if ($hasLocal)
        {
            $this->cm_map_locicon_anchor = \StringUtil::deserialize($this->cm_map_locicon_anchor,true);
            $indivlocIcon = array('url' => \cm_Maps\cm_Map_lib::getMarkerIcon($this->cm_map_locicon),
                'icon_anchor' => \StringUtil::deserialize($this->cm_map_locicon_anchor,true),
                'iconpopup_anchor' => \StringUtil::deserialize($this->cm_map_locicon_anchor,true)
            );
            
            $data = array(
                'infotext'  => $GLOBALS['TL_LANG']['MSC']['cm_map_visitorlocation'], 
                'cm_coords' => $lat . ',' . $lng,
                'icon'      => $this->cm_map_indivlocicon ? $indivlocIcon : null,
                'color'     => 'yellow', 
                'url'       => '', 
                'clickable' => false);
            
            $googleMap->markerData[] = $data;
        }

        if ($showCircle)
        {
            $data = array(
                'circleCoords' => $lat . ',' . $lng, 
                'radius'       => $max_dist * 1000, 
                'color'        => ($this->cm_map_circlecolor ? '#' . $this->cm_map_circlecolor : '#FF0000'), 
                'opacity'      => 0.8, 
                'weight'       => 3, 
                'fillColor'    => ($this->cm_map_circlecolor ? '#' . $this->cm_map_circlecolor : '#FF0000'), 
                'fillOpacity'  => 0.3);
            $googleMap->circleData = $data;
        }
        $googleMap->BaseJsScript=$baseJsScript;
        $googleMap->generate();
        $googleMap->MainJsScript=$mainJsScript;
        
        return $googleMap;       
    }

    protected function getGoogleMapCode($member, $coords, $center, $zoom, $icon_std, $mapID, $disablewheel,$map_api,$map_apikey)
    {
    	if ($this->map_infdtltemplate) $this->strTemplateInfoDetail = $this->map_infdtltemplate;
    	global $objPage;
    	$googleMap = new \cm_Maps\Map(true, $map_api);
        $language = $objPage->language;
        $googleMap->language = $language;
        $googleMap->mapType = \cm_Maps\cm_Map_lib::getMapTypeStr($this->cm_map_maptypedetail);
        $googleMap->showTypePanel = $this->cm_map_choosetypedetail;

        $googleMap->layout = $this->cm_map_styleDetailName;
        
        //$root_id = $this->getRootIdFromUrl();
        $root_id = \Frontend::getRootPageFromUrl()->id;
        $root_details = \PageModel::findWithDetails($root_id);
//         $map_apikey = $root_details->cm_map_apikey;
//         if (!$map_apikey) $map_apikey=\Config::get('cm_map_apikey');
        $baseJsScript = \cm_Maps\cm_Map_lib::getBaseScript($this->useSSL, $language,$map_api,$map_apikey);
        $clickType = $this->cm_map_clicktype;
        $mainJsScript = \cm_Maps\cm_Map_lib::getMarkerBaseScript($map_api,$this->useSSL,true, $clickType);
        //$markerJsScript = \cm_Maps\cm_Map_lib::getMarkerBaseScript($map_api,$this->useSSL,true, $clickType);
        
        $acceptanceRequired=false;
        $acceptanceText='';
        switch($this->cm_gm_acceptance_required)
        {
            case 'on':  
                $acceptanceRequired = true;
                $acceptanceText=$this->cm_gm_acceptance_text;
                break;
            case 'off': 
                $acceptanceRequired = false;
                $acceptanceText="";
                break;
            case 'page':
            default:    
                $acceptanceRequired = $root_details->cm_gm_acceptance_required??false;
                $acceptanceText = $root_details->cm_gm_acceptance_required?$root_details->cm_gm_acceptance_text:'';
                break;
        }
        $googleMap->initCookieBarSettings($root_details);
        $googleMap->acceptanceRequired = $acceptanceRequired;
        $googleMap->acceptanceText = $acceptanceText;
        
        $googleMap->settings = array(
            'disableWheel' => $disablewheel,
            'center' => $center, 
            'cm_map_choosetypelist' => $this->cm_map_choosetypelist, 
            'ctrlTypeStr' => \cm_Maps\cm_Map_lib::getCtrlTypeStr($this->cm_map_ctrltypelist),
    //      'cm_map_choosenavlist' => $this->cm_map_choosenavdetail,
    //      'ctrlNavStr' => cm_Map_lib::getCtrlNavStr($this->cm_map_ctrlnavlist),
            'cm_map_choosezoomlist' => $this->cm_map_choosezoomdetail, 
            'ctrlZoomStr' => \cm_Maps\cm_Map_lib::getCtrlZoomStr($this->cm_map_ctrlzoomlist), 
            'mapType' => \cm_Maps\cm_Map_lib::getMapTypeStr($this->cm_map_maptypedetail), 
            'zoom' => $zoom, 
            'mapID' => $mapID, 
            'showTypePanel' => $this->cm_map_choosetypedetail, 
            'infoShowOnload' => $this->cm_map_infoShowOnload, 
            'lat' => $lat, 
            'lng' => $lng, 
            'indiv' => $indiv, 
            'hasLocal' => $hasLocal, 
            'routeToDetail' => $this->cm_map_routetodetail,
            'clusterMarkers' => false
        );
        $googleMap->settings['cm_acceptance_text'] = \StringUtil::toHtml5($root_details->cm_gm_acceptance_text);
        $googleMap->settings['cm_acceptance_text'] = preg_replace('/[\t\n\r]+/', ' ', $googleMap->settings['cm_acceptance_text']);
        
        $googleMap->settings['cm_acceptance_buttontext'] = $root_details->cm_acceptance_buttontext;
        
        unset($googleMap->marker);
        if (preg_match('/^\-{0,1}\d+(\.\d*){0,1},\-{0,1}\d+(\.\d*){0,1}$/', $coords))
        {
            $infoTemplate = new \FrontendTemplate($this->strTemplateInfoDetail);
            $infoTemplate->setData($member);
            $infoTemplate->linktowebsite = $this->cm_map_linktowebsite;
            //$infoTemplate->urldetail = $this->Template->url . ($this->cm_membergooglemaps_hidedetaillink ? '' : '?show=' . $row["id"]);
            $infoTemplate->urldetail = $this->Template->url;
            $infoText = '';
            if ($this->cm_map_routetodetail)
            {
                $infoText = '<a onclick="window.open(this.href); return false;" href="'.($this->useSSL?'https://':'http://').'google.com/maps?saddr=&ie=UTF8&hl=de&daddr='
                    .$coords.'">'.$GLOBALS['TL_LANG']['MSC']['cm_map_getroutelist'].'</a>';
                    //                    $infoText = $GLOBALS['TL_LANG']['MSC']['cm_map_getroute'] . ": ";
            }
            $infoTemplate->routetext = $infoText;
            $infotext = preg_replace('/[\r|\n]*/', '', addslashes($infoTemplate->parse()));

            $data['cm_coords'] = $coords;
            $data['infotext'] = $infotext;
            $data['icon'] = $icon_std;
            $googleMap->marker = $data;
        }

        /*
         $code .= '
         var infoText=\''.$infoText.'\';
         var myMarker;
         var mappos = new google.maps.LatLng('.$coordinates.');';

         if ($this->cm_map_routetodetail)
         {
         $template= new FrontendTemplate('cm_googlemap_js_routedef');
         $bubbleContent = $template->parse();
         }

         $code .='// google.load("maps", "2.x");
         // Diese Funktion aufrufen, wenn die Seite geladen ist'."\n"
         . ($this->cm_map_routetodetail ? $bubbleContent : "")."\n"
         .'function initialize() {'."\n";

         if ($this->cm_map_routetodetail)
         {
         $code .='
         infoText= infoText + \'<a href="javascript:tolocation()">'
         .$GLOBALS['TL_LANG']['MSC']['cm_map_topos']
         .'<\/a> - <a href="javascript:fromlocation()">'
         .$GLOBALS['TL_LANG']['MSC']['cm_map_frompos']
         .'<\/a>\';';
         }

         //--------------------------------------------
         $code .='
         }';
         $code .='
         window.setTimeout("initialize()",500);
         </script>';
         */
        $googleMap->BaseJsScript=$baseJsScript;
       // $googleMap->MainJsScript=$mainJsScript;
       //$googleMap->MarkerClusterJsScript=$markerJsScript;

        $googleMap->generate();
        $googleMap->MainJsScript=$mainJsScript;
        //$code = $baseJsScript;
        //$code .= $mainJsScript;
        //$code .= $googleMap->parse();
        return $googleMap;
    }

    /**
     * Send a personal message
     * @param object
     * @param object
     */
    protected function cm_sendPersonalMessage(\Contao\Model\Collection $objMember, \Widget $objWidget, \Widget $objNameWidget=null, \Widget $objReplyTo)
    {
        $objEmail = new \Email();

        $objEmail->from = $GLOBALS['TL_ADMIN_EMAIL'];
        $objEmail->fromName = 'Contao mailer';
        $objEmail->text = $objWidget->value;

        if ($objNameWidget)
        {
//			$objEmail->text = sprintf($GLOBALS['TL_LANG']['MSC']['sendersProfile'],$objNameWidget->value)."\n\n---\n\n".$objEmail->text;
            $objEmail->text = $objNameWidget->value."\n\n---\n\n".$objEmail->text;
        }

        // Add reply to
        if (FE_USER_LOGGED_IN)
        {
            $this->import('FrontendUser', 'User');
            $replyTo = $this->User->email;

            // Add name
            if (strlen($this->User->firstname))
            {
                $replyTo = $this->User->firstname . ' ' . $this->User->lastname . ' <' . $replyTo . '>';
            }

            $objEmail->subject = sprintf($GLOBALS['TL_LANG']['MSC']['subjectFeUser'], $this->User->username, \Environment::get('host'));
            $objEmail->text .= "\n\n---\n\n" . sprintf($GLOBALS['TL_LANG']['MSC']['sendersProfile'], 
            				\Environment::get('base') . preg_replace('/show=[0-9]+/', 'show=' . $this->User->id, 
            				\Environment::get('request')));

            $objEmail->replyTo($replyTo);
        } 
        else
        {
            $replyTo = $objReplyTo->value;
            $objEmail->replyTo($replyTo);
            $objEmail->subject = sprintf($GLOBALS['TL_LANG']['MSC']['subjectUnknown'], \Environment::get('host'));
        }
		$objEmail->text .="\n\n---\n\nFrom ".$replyTo;
        // Send e-mail
        $objEmail->sendTo($objMember->email);
        $_SESSION['TL_EMAIL_SENT'] = true;

        $this->reload();
    }

    private static function getTextSearchOptions($searchObj,$multifieldsearch,$fieldsearch,$arrSearchFields,$searchStr,$forStr){
        if (($multifieldsearch || $fieldsearch) && $forStr != '' && $forStr != '*')
        {
            if ($multifieldsearch)
            {
                $searchObj['where'] .= 'LOWER(CONCAT(' . implode(",", $arrSearchFields) . '))' . " REGEXP LOWER(?) AND ";
                $searchObj['values'][] = $forStr;
            }
            else if ($fieldsearch)
            {
                $searchfield = $searchStr;
                if($searchfield)
                {
                    if (\in_array($searchfield,$arrSearchFields,true))
                    {
                        $searchObj['where'] .= "LOWER(".$searchStr . ") REGEXP LOWER(?) AND ";
                        $searchObj['values'][] = $forStr;
                    }
                    else
                    {
                        $searchObj['where'] .= " 1=0 AND ";
                    }
                }
            }
        }
        //print_r($arrSearchFields); echo('->'.$searchfield);
            return $searchObj;	
	}      

	private static function getPlzSearchOptions($searchObj,$plzsearch,$size,$plzArea)
	{
        if ($plzsearch)
        {
            if ($plzArea)
            {
                $area = trim($plzArea);
                if (preg_match('/^\d{' . $size . '}\d*$/', $area))
                {
                    $searchObj['where'] .= " postal REGEXP '^" . $area . ".*' AND ";
                }
            }
        }
            return $searchObj;
	}	

	private static function getGroupFilterSearchOptions($searchObj, $requestedGroups)
	{
	    if (!$requestedGroups || !\is_array($requestedGroups) || count($requestedGroups)==0) { return $searchObj; }
	    
	    $intMaxGrouptofilter = count($requestedGroups) - 1;
        // Filter groups
        for ($i = 0; $i <= $intMaxGrouptofilter; $i++)
        {
            $searchObj['where'] .= ($i == 0 ? "(" : "")."`groups` LIKE ?";

            if ($i < $intMaxGrouptofilter)
            {
                $searchObj['where'] .= " OR ";
            } else
            {
                $searchObj['where'] .= ") AND ";
            }
            $searchObj['values'][] = '%"' . $requestedGroups[$i] . '"%';
        }
            return $searchObj;
	}

	private static function getGroupSearchOptions($searchObj,$arrMlGroups)
	{
	    $intGroupLimit = (count($arrMlGroups) - 1);
        
            for ($i = 0; $i <= $intGroupLimit; $i++)
            {
                $searchObj['where'] .= ($i == 0 ? "(" : "");
                if ($i < $intGroupLimit)
                {
                    $searchObj['where'] .= "`groups` LIKE ? OR ";
                    $searchObj['values'][] = '%"' . $arrMlGroups[$i] . '"%';
                } 
                else
                {
                    $searchObj['where'] .= "`groups` LIKE ?) AND ";
                    $searchObj['values'][] = '%"' . $arrMlGroups[$i] . '"%';
                }
            }
            return $searchObj;
	}

	private static function getActiveMemberFilterOptions($searchObj,$usernameIncluded, $time)
	{
        // Add Whereclause - List active members only
            if ($usernameIncluded)
            {
                $searchObj['where'] .= "(publicFields!='' OR allowEmail=? OR allowEmail=?) AND disable!=1 AND (start='' OR start<=?) AND (stop='' OR stop>=?)";
                array_push($searchObj['values'], 'email_member', 'email_all', $time, $time);
            } 
            else
            {
                $searchObj['where'] .= "publicFields!='' AND disable!=1 AND (start='' OR start<=?) AND (stop='' OR stop>=?)";
                array_push($searchObj['values'], $time, $time);
            }
            return $searchObj;	
	}       
			
    protected function setCfgTagWidgets() 
    {}
    protected function generateCfgTagWidgets()
    {
        return null;
    }
    
    protected function preselectMemberByTags()
    {
        return array();   
    }

    /**
     * List all members
     */

}
