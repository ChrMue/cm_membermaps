<?php
                       
/**
 * PHP version 7
 * @copyright  Christian Muenster 2009-2020
 * @author     Christian Muenster
 * @package    cm_membermaps
 * @license    LGPL
 * @filesource
 */

namespace ChrMue\cm_MemberMaps;


/**
 * Class ModuleMemberGoogleMapsPos
 *
 * @copyright  Christian Muenster 2020
 * @author     Christian Muenster 
 * @package    Controller
 */
class ModuleMemberGoogleMapsPos extends \Module
{


	/**
	 * Template
	 * @var string
	 */
protected $strTemplate = 'mod_cm_memberlist_googlemaps_pos';
	/**
	 * Groups
	 * @var array
	 */
protected $arrMlGroups = array();


protected $mappos = 'below';
	/**
	 * Fields
	 * @var array
	 */
protected $arrMlFields = array();

private $useSSL = false;

/**
 * Fields
 * @var array
 */
private $gcapi;
private $gcapiKey;
private $api;
private $apiKey;

private function getApiAndKey()
{
    $root_id = self::getRootPageFromUrl()->id;
    $root_details =  \PageModel::findWithDetails($root_id);
    
    $gcapi = $root_details->cm_gc_api;
    if (!$gcapi) { $gcapi=\Config::get('cm_gc_api'); }
    if (!$gcapi) throw new \Exception("Api not selected");
    $gcapiprop="cm_map_apikey_".strtolower($gcapi);
    
    $gcapiKey = $root_details->$gcapiprop;
    if (!$gcapiKey) { $gcapiKey=\Config::get($gcapiprop); }
    
    $api = $root_details->cm_map_api;
    if (!$api) { $api=\Config::get('cm_map_api'); }
    if (!$api) throw new \Exception("Api not selected");
    $apiprop="cm_map_apikey_".strtolower($api);
    $apiKey = $root_details->$apiprop;
    if (!$apiKey) { $apiKey=\Config::get($apiprop); }
    
    $this->gcapi=$gcapi;
    $this->gcapiKey=$gcapiKey;
    $this->api=$api;
    $this->apiKey=$apiKey;    
}

    /**
     * Display a wildcard in the back end
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### Show MEMBER with Map ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }
        return parent::generate();
    }


    /**
     * Generate module
     */
    protected function compile()
    {
        $this->useSSL=\Config::get('cm_request_gm_ssl') || \Environment::get('ssl');

        $this->getApiAndKey();
        if ($this->api=='OSM')
        {
            $GLOBALS['TL_CSS'][] = 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css';
        }
        
        $this->showMember($this->cm_membergooglemaps_member);

        //parent::compile();
    }
	
    /**
     * List a single member
     * @param integer
     */
    protected function showMember($id)
    {
        global $objPage;

        $time = time();
        $this->Template->record = array();

        // Get member
//        $objMember = \Database::getInstance()->prepare("SELECT * FROM tl_member WHERE id=? AND disable!=1 AND (start='' OR start<=?) AND (stop='' OR stop>=?)")
//                                                                ->limit(1)
//                                                                ->execute($id, $time, $time);
        $objMember = MemberGoogleMapsMemberModel::findByIdOrAlias($id);

            // No member found or group not allowed
/*
        echo $objMember->numRows.+"<br />"
        .$objMember->firstname."&nbsp;"
        .$objMember->lastname."<br />"
        .$objMember->street."<br />";
*/
        if (null == $objMember)
        {
            $this->Template->invalid = $GLOBALS['TL_LANG']['MSC']['invalidUserId'];

            // Do not index the page
            $objPage->noSearch = 1;
            $objPage->cache = 0;

            // Send 404 header
            header('HTTP/1.1 404 Not Found');
            return;
        }
        if (($objMember->website))
        {
            if (preg_match('@^(https://)@i', $objMember->website))	
            {
                    $objMember->website = substr($objMember->website,8);
            }
            elseif (preg_match('@^(http://)@i', $objMember->website)) {
                    $objMember->website = substr($objMember->website,7);
            }

        }			

        // Default variables
        $this->Template->action = ampersand(\Environment::get('indexFreeRequest'));
        $this->Template->referer = 'javascript:history.go(-1)';
        $this->Template->back = $GLOBALS['TL_LANG']['MSC']['goBack'];
        $this->Template->publicProfile = sprintf($GLOBALS['TL_LANG']['MSC']['publicProfile'], $objMember->username);
        $this->Template->noPublicInfo = $GLOBALS['TL_LANG']['MSC']['noPublicInfo'];
        $this->Template->sendEmail = $GLOBALS['TL_LANG']['MSC']['sendEmail'];
        $this->Template->submit = $GLOBALS['TL_LANG']['MSC']['sendMessage'];
        $this->Template->loginToSend = $GLOBALS['TL_LANG']['MSC']['loginToSend'];
        $this->Template->emailDisabled = $GLOBALS['TL_LANG']['MSC']['emailDisabled'];

        // Confirmation message
        if ($_SESSION['TL_EMAIL_SENT'])
        {
            $this->Template->confirm = $GLOBALS['TL_LANG']['MSC']['messageSent'];
            $_SESSION['TL_EMAIL_SENT'] = false;
        }

        // Check personal message settings
        switch ($objMember->allowEmail)
        {
            case 'email_all':
                    $this->Template->allowEmail = 3;
                    break;

            case 'email_member':
                    $this->Template->allowEmail = FE_USER_LOGGED_IN ? 3 : 2;
                    break;

            default:
                    $this->Template->allowEmail = 1;
                    break;
        }

        // No e-mail address given
        if (!$objMember->email || !strlen($objMember->email))
        {
                $this->Template->allowEmail = 1;
        }
        else
        {
            if (!version_compare(VERSION, '3.5', '<'))
            {
                $value = \StringUtil::encodeEmail($objMember->email);
            }
            else
            {
                $value = \String::encodeEmail($objMember->email);
            }
            $value = '<a href="mailto:' . $value . '">' . $value . '</a>';
            $this->Template->emailtext = $value;
        }

        // Handle personal messages
        if ($this->Template->allowEmail > 1)
        {
            $arrField = array(
                'name' => 'message', 
                'label' => $GLOBALS['TL_LANG']['MSC']['message'], //['message'],
                'inputType' => 'textarea', 
                'eval' => array('mandatory' => true, 'required' => true, 'rows' => 4, 'cols' => 40, 'decodeEntities' => true));
            $arrFieldCaptcha = array(
                'name' => 'captcha', 
                'label' => $GLOBALS['TL_LANG']['MSC']['cm_captcha'], 
                'inputType' => 'captcha', 'eval' => array('mandatory' => true));

            $arrWidget = \Widget::getAttributesFromDca($arrField, $arrField['name'], '');
            $objWidget = new \FormTextArea($arrWidget);

            if ($this->cm_email_nameRequired)
            {
                $arrFieldName = array(
                    'name' => 'name', 
                    'label' => $GLOBALS['TL_LANG']['MSC']['cm_name'], 
                    'inputType' => 'text', 
                    'eval' => array('mandatory' => true, 'required' => true)
                );
                $arrNameWidget = \Widget::getAttributesFromDca($arrFieldName, $arrFieldName['name'], '');
                $objNameWidget = new \FormTextField($arrNameWidget);
            }

            $arrEmailWidget = \Widget::getAttributesFromDca($arrFieldEmail, $arrFieldEmail['name'], '');
            $objEmailWidget = new \FormTextField($arrEmailWidget);

            $arrCaptchaWidget = \Widget::getAttributesFromDca($arrFieldCaptcha, $arrFieldCaptcha['name'], '');
            $objCaptchaWidget = new \FormCaptcha($arrCaptchaWidget);

            // Validate widget
            if (\Input::post('FORM_SUBMIT') == 'tl_send_email')
            {
                $objWidget->validate();
                if ($this->cm_email_nameRequired){
                    $objNameWidget->validate();
                }
                else
                {
                    $objNameWidget=null;
                }
                $objEmailWidget->validate();
                $objCaptchaWidget->validate();

                if (!$objCaptchaWidget->hasErrors() && !$objWidget->hasErrors() && (!$this->cm_email_nameRequired || !$objNameWidget->hasErrors()) && !$objEmailWidget->hasErrors())
                {
                    $this->sendPersonalMessage($objMember, $objWidget);
                }
            }

            $this->Template->widget = $objWidget;
            if ($this->cm_email_nameRequired)
            {
                $this->Template->namewidget = $objNameWidget;
            }
            $this->Template->emailwidget = $objEmailWidget;
            $this->Template->captchawidget = $objCaptchaWidget;

            $this->Template->submit = $GLOBALS['TL_LANG']['MSC']['sendMessage'];
        }
        $arrFields = \StringUtil::deserialize($objMember->publicFields);

        $hasGoogleMaps=preg_match('/^[-]{0,1}\d+(\.\d*){0,1},[-]{0,1}\d+(\.\d*){0,1}$/',
                                  $objMember->cm_coords);
        $this->Template->hasGoogleMaps=$hasGoogleMaps;
        if ($hasGoogleMaps)
        {
            $acceptanceSettings=array('required'=>false,'text'=>'undefined');
            $mapID  = "cm_map".$this->mappos."_".$this->id;
            $center = $this->cm_map_indivcenterpos ? $this->cm_map_centerpos : $objMember->cm_coords;
            $zoom = $objMember->cm_map_indivzoom ? $objMember->cm_map_zoom : $this->cm_map_stdzoom;
            if ($zoom == "")
                $zoom = $this->cm_map_stdzoom;


            $tempCoords = $objMember->cm_lat . "," . $objMember->cm_lng;
            $member = array('company' => $objMember->company, 'firstname' => $objMember->firstname, 'lastname' => $objMember->lastname, 'postal' => $objMember->postal, 'street' => $objMember->street, 'h_nr' => $objMember->h_nr, 'city' => $objMember->city, 'website' => $objMember->website, );

            $googleMap = $this->getGoogleMapCode($member, $tempCoords, $center, $zoom, $iconstd, $mapID, $this->cm_map_disablewheeldetail,$this->api,$this->apiKey);
            $acceptanceSettings['required']=$googleMap->acceptanceRequired;
            $acceptanceSettings['text']=$googleMap->acceptanceText;

            $this->Template->cookiebarInstalled=$googleMap->settings['cookiebarInstalled'];
            $this->Template->acceptanceButtontext=$googleMap->settings['cm_acceptance_buttontext'];
            $this->Template->acceptanceText=$googleMap->settings['cookiebarInstalled'] > 0 ? $googleMap->settings['cm_acceptance_text'] : $googleMap->acceptanceText;
            $this->Template->acceptanceRequired=$googleMap->acceptanceRequired;
            $this->Template->BaseScriptCode = $googleMap->BaseJsScript;
            $this->Template->GoogleMapCode = $googleMap->MainJsScript.$googleMap->Parse();
        }
        else
        {
            \Contao\System::log("Koord. \"".$objMember->cm_coords."\"nicht korrekt (".$objMember->id.")","cm_membermaps",TL_GENERAL);
            //$this->Template->GoogleMapCode = $GLOBALS['TL_LANG']['MSC']['cm_membermaps_error'];
            $this->Template->GoogleMapCode = '';
            $this->Template->acceptanceRequired=false;
            $this->Template->acceptanceText ="";
        }

        if ($this->cm_map_heightdetail) 
        {
            $heightArr=\StringUtil::deserialize($this->cm_map_heightdetail);
            if ($heightArr["value"])
            {
                $this->Template->mapstyle="height:".$heightArr["value"].$heightArr["unit"];
            }
        }
        $this->Template->mappos = $this->mappos;
        $this->Template->mapID = $mapID;
    }

    protected function getGoogleMapCode($member,$coords,$center,$zoom,$iconstd,$mapID,$disablewheel,$map_api,$map_apikey)
    {
        global $objPage;
        $googleMap = new \cm_Maps\Map(true, $map_api);
        $language =  $objPage->language;
        $googleMap->language = $language;
        $googleMap->mapType = \cm_Maps\cm_Map_lib::getMapTypeStr($this->cm_map_maptypedetail);
        $googleMap->showTypePanel = $this->cm_map_choosetypedetail;
        $googleMap->layout = $this->cm_map_styleDetailName;
        $baseJsScript = \cm_Maps\cm_Map_lib::getBaseScript($this->useSSL,$language,$map_api,$map_apikey);
        $mainJsScript = \cm_Maps\cm_Map_lib::getMarkerBaseScript($map_api,$this->useSSL,true, $clickType);
        $acceptanceRequired=false;
        $acceptanceText='';
        switch($this->cm_gm_acceptance_required)
        {
            case 'on':  $acceptanceRequired = true;
            $acceptanceText=$this->cm_gm_acceptance_text;
            break;
            case 'off': $acceptanceRequired = false;
            $acceptanceText="";
            break;
            case 'page':
            default:    $acceptanceRequired = $root_details->cm_gm_acceptance_required??false;
            $acceptanceText = $root_details->cm_gm_acceptance_required?$root_details->cm_gm_acceptance_text:'';
            break;
        }
        $googleMap->initCookieBarSettings($root_details);
        $googleMap->acceptanceRequired = $acceptanceRequired;
        $googleMap->acceptanceText = $acceptanceText;
    
//-------------------------------------------------    
        $infoText = preg_replace("/\r|\n/s", "", $this->cm_map_infotextdetail);
        if ($this->cm_map_routetodetail)
        {
          //  $infoText .= ($infoText?'<p class="route">':'').$GLOBALS['TL_LANG']['MSC']['cm_map_getroute'].': ';
            $infoText .= '<a onclick="window.open(this.href); return false;" href="'.($this->useSSL?'https://':'http://').'google.com/maps?saddr=&ie=UTF8&hl=de&daddr='
                .$coords.'">'.$GLOBALS['TL_LANG']['MSC']['cm_map_getroute'].'</a>';
                    //                    $infoText = $GLOBALS['TL_LANG']['MSC']['cm_map_getroute'] . ": ";
        }
//-------------------------------------------------
        $infotext=$infoText;
        $googleMap->settings = array(
            'disableWheel' => $disablewheel,
            'center' => $center,
            'cm_map_choosetypelist' => $this->cm_map_choosetypelist,
            'ctrlTypeStr' => \cm_Maps\cm_Map_lib::getCtrlTypeStr($this->cm_map_ctrltypedetail),
            'cm_map_choosezoomlist' => $this->cm_map_choosezoomdetail,
            'ctrlZoomStr' => \cm_Maps\cm_Map_lib::getCtrlZoomStr($this->cm_map_ctrlzoomlist),
            'mapType' => \cm_Maps\cm_Map_lib::getMapTypeStr($this->cm_map_maptypedetail),
            'zoom' => $zoom,
            'mapID' => $mapID,

            'routeToDetail' => $this->cm_map_routetodetail,
            'clusterMarkers' => false,
            'cm_ElementOnClickRequiresResize' => $this->cm_ElementOnClickRequiresResize

        );

        unset($googleMap->marker);
        if (preg_match('/^\-{0,1}\d+(\.\d*){0,1},\-{0,1}\d+(\.\d*){0,1}$/', $coords))
        {
            $data=array(
                'cm_coords' => $coords,
                'infotext'=> $infotext,
                'icon'=> $iconstd
            );
            $googleMap->marker = $data;
        }
    
//--------------------------------------------

/*    
   $code .= ' 
    var infoText=\''.$infoText.'\';
    var infowindow;
    var '.$mapID.'to_html="";
    var '.$mapID.'from_html="";
    var '.$mapID.'myMarker;
    var mappos = new google.maps.LatLng('.$coords.');';

    if ($this->cm_map_routetodetail)
    {
      $bubbleRoute =
      $mapID.'to_html = infoText + \'<b>'
             .$GLOBALS['TL_LANG']['MSC']['cm_map_topos']
             .'<\/b> - <a href="javascript:'.$mapID.'fromlocation()">'
             .$GLOBALS['TL_LANG']['MSC']['cm_map_frompos']
             .'<\/a>\' + ' . "\n"
//             .'\'<br \/>'
             .'\'<\/p><p class="routefrom">'
             .$GLOBALS['TL_LANG']['MSC']['cm_map_fromaddr']
             .':<form class="route" action="'.($this->useSSL?'https://':'http://').'maps.google.com/maps" method="get" target="_blank">\' + ' . "\n"
             .'\'<input type="text" SIZE=23 MAXLENGTH=50 name="saddr" id="saddr" value="" /> \' + ' . "\n"
             .'\'<INPUT value="Los" TYPE="SUBMIT">\' + ' . "\n"
             .'\'<input type="hidden" name="daddr" value="\' + mappos.lat() + \',\' + mappos.lng() + ' . "\n" 
             .'\'"/><\/form><\/p>\';';
      $bubbleRoute .=
      $mapID.'from_html = infoText + \'<a href="javascript:'.$mapID.'tolocation()">'
             .$GLOBALS['TL_LANG']['MSC']['cm_map_topos']
             .'<\/a> - <b>'
             .$GLOBALS['TL_LANG']['MSC']['cm_map_frompos']
             .'<\/b>\' + ' . "\n"
             .'\'<\/p><p class="routeto">'
             .$GLOBALS['TL_LANG']['MSC']['cm_map_toaddr']
             .':<form class="route" action="'.($this->useSSL?'https://':'http://').'maps.google.com/maps" method="get"" target="_blank">\' + ' . "\n"
             .'\'<input type="text" SIZE=23 MAXLENGTH=50 name="daddr" id="daddr" value="" /> \' + ' . "\n"
             .'\'<INPUT value="Los" TYPE="SUBMIT">\' + ' . "\n"
             .'\'<input type="hidden" name="saddr" value="\' + mappos.lat() + \',\' + mappos.lng() + ' . "\n"
             .'\'"/><\/form><\/p>\';';
      $bubbleContent = "\n" . $bubbleRoute . "\n";
    }
   $code .= ($this->cm_map_routetodetail ? $bubbleContent : "")."\n";


      if ($this->cm_map_routetodetail)
      {
        $code .='function '.$mapID.'tolocation() {
                  infowindow.setContent('.$mapID.'to_html);
                }
                function '.$mapID.'fromlocation() {
                  infowindow.setContent('.$mapID.'from_html);
                }'."\n";
      }
//--------------------------------------------
      $code .='function '.$mapID.'_initialize() {
      if(!window.google){
          return false;
      }'."\n";
      $code .='mappos = new google.maps.LatLng('.$coords.');';
    if ($this->cm_map_routetodetail)
    {
      $code .='infoText= infoText + \'<a href="javascript:'.$mapID.'tolocation()">'
      .$GLOBALS['TL_LANG']['MSC']['cm_map_topos']
      .'<\/a> - <a href="javascript:'.$mapID.'fromlocation()">'
      .$GLOBALS['TL_LANG']['MSC']['cm_map_frompos']
      .'<\/a><\/p>\';';
    }
//--------------------------------------------
    
    $code .='var mapOptions = {
        center: new google.maps.LatLng('.$center.'),
        mapTypeControl: '.($this->cm_map_choosetypedetail?'true':'false').',';
        if ($this->cm_map_choosetypedetail && ($this->cm_map_ctrltypedetail!='')) {
          $code .='mapTypeControlOptions: {
           style: google.maps.MapTypeControlStyle.';
          $code .= \cm_Maps\cm_Map_lib::getCtrlTypeStr($this->cm_map_ctrltypedetail);
          $code .='},';
        }
        $code .='navigationControl: '.($this->cm_map_choosenavdetail?'true':'false').',';
        if ($this->cm_map_choosenavdetail && ($this->cm_map_ctrlnavdetail!='')) {
          $code .='navigationControlOptions: {
           style: google.maps.NavigationControlStyle.';
          $code.= \cm_Maps\cm_Map_lib::getCtrlNavStr($this->cm_map_ctrlnavdetail);
          $code .='},';
        }
        $code.='mapTypeId: google.maps.MapTypeId.'.$mapType.',
        zoom: '.$zoom.' 
      }
      
      var map = new google.maps.Map(document.getElementById("'.$mapID.'"),mapOptions);'."\n";
    
//--------------------------------------------
      $code .=
      $mapID.'myMarker = new google.maps.Marker({
      position: mappos, 
      map: map'; 
      if ($iconStd)
        $code .=', icon: "'.$iconStd.'"';
      $code .='});

      infowindow = new google.maps.InfoWindow({
          content: infoText
      });'."\n";     
      

       $code .='google.maps.event.addListener('.$mapID.'myMarker, "click", function() {
          infowindow.open(map,'.$mapID.'myMarker);
        });';
      if ($this->cm_map_infoShowOnload)
      {
        $code.='infowindow.open(map,'.$mapID.'myMarker);';
      }
     $code .='}';

    $code .="
     if(window.addEvent) {
         window.addEvent('domready', function() {
             ".$mapID."_initialize();
    });
    } else if(typeof jQuery == 'function') {
        jQuery(document).ready(function(){
            ".$mapID."_initialize();
        });
    } else {
        window.setTimeout('".$mapID."_initialize()', 500);
    }";
*/    
        $googleMap->MainJsScript=$mainJsScript;
        $googleMap->BaseJsScript=$baseJsScript;

        $googleMap->generate();

        return $googleMap;
    
    }

}
