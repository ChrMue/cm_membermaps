<?php

/**
 * PHP version 7
 * @copyright  Christian Muenster 2009-2021
 * @author     Christian Muenster
 * @package    cm_membermaps
 * @license    LGPL
 * @filesource
 */

namespace ChrMue\cm_MemberMaps;

/**
 * Class ModuleMemberFinder
 *
 * @copyright  Christian Muenster 2021
 * @author     Christian Muenster
 * @package    Module
 */
class ModuleMemberFinder extends \Module
{

	/**
	 * Template
	 * @var string
	 */

	protected $strTemplate = 'mod_cm_memberlist_finder';

	/**
	 * Fields
	 * @var array
	 */

    private $fieldsInList = array();

    protected $arrMlFields = array();
    protected $arrMlFieldsList = array();
    protected $arrSearchFields = array();
    protected $useTags;
	 
	//protected $arrMlFields = array();

    function __construct($param)
    {
        parent::__construct($param);
    }

	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{

        if (TL_MODE == 'BE')
		{
			$objTemplate = new \BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### MEMBER FINDER to use with cm_membermaps ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}
		if (!version_compare(VERSION, '4', '<'))
		{
    		$this->useTags = $this->cm_usetags && in_array('tags', \ModuleLoader::getActive()) && !in_array('tl_member',explode(', ', \Config::get('disabledTagObjects')));
    	}
    	else
    	{
		    $this->useTags = $this->cm_usetags && in_array('tags_members', \ModuleLoader::getActive()) && in_array('tags', \ModuleLoader::getActive());
		}
		
		$this->arrMlFields = deserialize($this->ml_fields, true);

        //  search in one field selected from several fields in a dropdown
        $arrFieldsList = deserialize($this->cm_membergooglemaps_fieldslist, true);
        $this->fieldsInList = $arrFieldsList;
        //  search in several fields an once
        $this->arrSearchFields = deserialize($this->cm_memberlist_searchfieldslist, true);
       
        $this->arrMlFieldsList = null;

        if ($arrFieldsList)
        {
            foreach ($arrFieldsList as $listField)
            {
                $this->arrMlFieldsList[] = $listField["field"];
            }
        }

		// Get the target page (list view)

		if (($detailPg = \PageModel::findByPk($this->cm_memberlist_pg)) !== null)
		{
			$this->strLink = \Controller::generateFrontendUrl($detailPg->row());
		} else
		{
			$this->strLink = preg_replace('/\?.*$/', '', \Environment::get('request'));
		}

		return parent::generate();
	}

	/**
	 * Generate module
	 */
	protected function compile()
	{
		if (!version_compare(VERSION, '3.5', '<'))
			$this->import('StringUtil');
		else {
			$this->import('String');
		}
		$this->loadDataContainer('tl_member');
		$this->loadLanguageFile('tl_member');
        
		$this->Template = new \FrontendTemplate($this->strTemplate);
		$distanceobj = "";

		$this->Template->action = $this->strLink;



        $location = \Input::get('cm_location');
        $country = \Input::get('cm_country');
        if (!$country)
        {
            $country = $this->cm_map_country;
        }
        $max_dist = null;
        if (\Input::get('cm_max_dist'))
        {
            $max_dist = \Input::get('cm_max_dist');
        }
        				
		$this->Template->searchArr = $searchArr;
 
        $arrFieldsList = $this->arrMlFieldsList;

        //$arrValues = array();
        //$strWhere = '';
		$searchObj = array(
			'where' => '',
			'values'=> array()
		);
        $fieldsearch = $this->cm_memberlist_fieldsearch=='single'; 
        $multifieldsearch = $this->cm_memberlist_fieldsearch=='multi';
        
        $plzsearch = $this->cm_memberlist_plzsearch;
        $plzareasize = $this->cm_memberlist_plznumberdigits;

        $this->Template->fieldsearch = $fieldsearch;
        $this->Template->multifieldsearch = $multifieldsearch;
        $this->Template->plzsearch = $plzsearch;
		$this->Template->cm_map_locationfixed= $this->cm_map_locationfixed;
		
        if ($this->cm_memberlist_addressform || $this->cm_map_locationfixed)
        {
            $this->Template->distanceform = $this->cm_memberlist_distanceform;
            $this->Template->lbl_location = $GLOBALS['TL_LANG']['MSC']['cm_lbl_location'];
            $this->Template->lbl_country = $GLOBALS['TL_LANG']['MSC']['cm_lbl_country'];
            $this->Template->lbl_max_dist = $GLOBALS['TL_LANG']['MSC']['cm_lbl_max_dist'];
            $this->Template->lbl_max_dist_drdn = $GLOBALS['TL_LANG']['MSC']['cm_lbl_max_dist_drdn'];

            $this->Template->cm_distitem = $GLOBALS['TL_LANG']['MSC']['cm_distitem'];

            $this->Template->distsearch_label = specialchars($GLOBALS['TL_LANG']['MSC']['cm_distsearch_label']);
            $this->Template->radius_search = specialchars($GLOBALS['TL_LANG']['MSC']['cm_radius_search']);

            $this->Template->radiusform = $this->cm_memberlist_addressform || $this->cm_map_locationfixed;
            //      $this->Template->cm_distsearch_label=$GLOBALS['TL_LANG']['MSC']['cm_memberlist_distsearch'];
            $this->Template->cm_gc_acceptance_required =  $this->cm_gc_acceptance_required;
    	}
        $this->Template->visitorlocation = $location;
    	$this->Template->showcountry = $this->cm_map_showcountry;
    	$this->Template->country = $this->cm_map_country;
//		$this->Template->lbl_location = $GLOBALS['TL_LANG']['MSC']['cm_lbl_location'];
//		$this->Template->lbl_country = $GLOBALS['TL_LANG']['MSC']['cm_lbl_country'];
//		$this->Template->lbl_max_dist = $GLOBALS['TL_LANG']['MSC']['cm_lbl_max_dist'];
//		$this->Template->distsearch_label = $GLOBALS['TL_LANG']['MSC']['cm_distsearch_label'];

    	if ($this->cm_map_country_as_select)
    	{
    		$visitorcountryobj = '<select name="cm_country" class="cm_country" >';
    
    		foreach ($this->getCountries() as $countryCode => $countryName)
    		{
    			if (strtolower($countryCode) == strtolower($this->cm_map_country))
    			;
    			$visitorcountryobj .= '<option value="' . strtolower($countryCode) . '" ' . (strtolower($countryCode) == strtolower($this->cm_map_country) ? ' selected="selected"' : '') . '>' . $countryName . '</option>';
    		}
    		$visitorcountryobj .= '</select>';
    	} 
    	else
    	{
    	    $visitorcountryobj = '<input class="cm_country" type="text" name="cm_country" value="' . $this->cm_map_country . '" />';
    	}

        $this->Template->visitorcountry = $visitorcountryobj;


        $this->Template->max_dist = $max_dist;

        $strOptions = '';
        $arrSortedFields = array();

		if ($arrFieldsList && count($arrFieldsList)>0)
		{
	        // Sort fields
	        foreach ($arrFieldsList as $field)
	        {
	            $arrSortedFields[$field] = $GLOBALS['TL_DCA']['tl_member']['fields'][$field]['label'][0];
	        }
	        natcasesort($arrSortedFields);
	        // Add searchable fields to drop-down menu
	        foreach ($arrSortedFields as $k => $v)
	        {
	            if (($k != 'cm_coords') && ($k != 'cm_allowmap'))
	            {
	                $strOptions .= '  <option value="' . $k . '"' . (($k == \Input::get('search')) ? ' selected="selected"' : '') . '>' . $v . '</option>' . "\n";
	            }
	        }
		}
        $this->Template->search_fields = $strOptions;
        $this->Template->search_label = specialchars($GLOBALS['TL_LANG']['MSC']['search']);
        
		$this->Template->distanceasdropdown = $this->cm_memberlist_distanceasdropdown;

		if ($this->cm_memberlist_distanceasdropdown)
		{
			$this->Template->distvalues = \cm_Maps\cm_Map_lib::getDistanceOptionsHTML($this->cm_memberlist_distancevalues, $max_dist);
		}

	}

}
