<?php

/**
 * PHP version 7
 * @copyright  Christian Muenster 2009-2020
 * @author     Christian Muenster
 * @package    cm_membermaps
 * @license    LGPL
 * @filesource
 */

namespace ChrMue\cm_MemberMaps;


/**
 * Class ModuleMemberGoogleMapsReader
 *
 * @copyright  Christian Muenster 2021
 * @author     Christian Muenster
 * @package    Module
 *
 * based on ModuleMemberlist by Leo Feyer
 */
class ModuleMemberGoogleMapsReader extends  \Contao\ModuleMemberlist //Module
{
    /**
    * Template
    * @var string
    */

	protected $detailParam = 'show';
    /**
     * details view
     */
    protected $strTemplateDetail = 'mod_cm_memberlist_googlemaps_detail';
    /**
     * bubble content for the list view
     */
    protected $strTemplateInfoDetail = 'info_cm_membergooglemaps';

     /**
     * use cunstructor form the base class ModuleMemberlist
     */
    function __construct($param)
    {
        parent::__construct($param);
    }

   /**
     * Groups
     * @var array
     */
    protected $arrMlGroups = array();
    protected $arrMlFieldsList = array();
    private $fieldsInList = array();
    private $groupInList = array();
    protected $arrSearchFields = array();
    protected $useTags;
	private $useSSL = false;

    /**
     * position of the map - standard: below the list
     */
    protected $mappos = 'below';

    private $factor = null;

    /**
     * Fields
     * @var array
     */
    private $gcapi;
    private $gcapiKey;
    private $api;
    private $apiKey;
    
    private function getApiAndKey()
    {
        $root_id = self::getRootPageFromUrl()->id;
        $root_details =  \PageModel::findWithDetails($root_id);
        
        $gcapi = $root_details->cm_gc_api;
        if (!$gcapi) { $gcapi=\Config::get('cm_gc_api'); }
        if (!$gcapi) throw new \Exception("Api not selected");
        $gcapiprop="cm_map_apikey_".strtolower($gcapi);
        
        $gcapiKey = $root_details->$gcapiprop;
        if (!$gcapiKey) { $gcapiKey=\Config::get($gcapiprop); }
        
        $api = $root_details->cm_map_api;
        if (!$api) { $api=\Config::get('cm_map_api'); }
        if (!$api) throw new \Exception("Api not selected");
        $apiprop="cm_map_apikey_".strtolower($api);
        $apiKey = $root_details->$apiprop;
        if (!$apiKey) { $apiKey=\Config::get($apiprop); }
        
        $this->gcapi=$gcapi;
        $this->gcapiKey=$gcapiKey;
        $this->api=$api;
        $this->apiKey=$apiKey;
        
    }
    
    //protected $arrMlFields = array();

    /**
     * Display a wildcard in the back end
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### MEMBERLIST with Google Map (API V3) - Reader ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }
        if (!version_compare(VERSION, '4', '<'))
        {
            $this->useTags = $this->cm_usetags && \in_array('tags', \ModuleLoader::getActive()) && !\in_array('tl_member',explode(', ', \Config::get('disabledTagObjects')));
        }
        else
        {
            $this->useTags = $this->cm_usetags && \in_array('tags_members', \ModuleLoader::getActive()) && \in_array('tags', \ModuleLoader::getActive());
        }

        $this->arrMlGroups = \StringUtil::deserialize($this->ml_groups, true);
        $this->arrMlFields = \StringUtil::deserialize($this->ml_fields, true);

        $arrFieldsList = \StringUtil::deserialize($this->cm_membergooglemaps_fieldslist, true);
        $this->fieldsInList = $arrFieldsList;

        $this->arrSearchFields = \StringUtil::deserialize($this->cm_memberlist_searchfieldslist, true);

        if ($this->cm_memberlist_fieldsearch=='no')
        {
            $this->arrSearchFields=array();
        }

        $this->arrSearchFields = array_intersect($this->arrSearchFields, array_merge($arrFieldsList,$this->arrMlFields));
        $this->arrMlFieldsList = null;
        if ($arrFieldsList)
        {
            foreach ($arrFieldsList as $listField)
            {
                $this->arrMlFieldsList[] = $listField["field"];
            }
        }
        /*
        if (count($this->arrMlGroups) < 1 || count($this->arrMlFieldsList) < 1 || count($this->arrMlFields) < 1)
        {
            return '';
        }
        */
        return parent::generate();
    }

    /**
     * Generate module
     */
    protected function compile()
    {
    
	$this->useSSL=\Config::get('cm_request_gm_ssl') || \Environment::get('ssl');

        // Set the item from the auto_item parameter
        if (!isset($_GET[$this->detailParam]) && \Config::get('useAutoItem') && isset($_GET['auto_item']))
    	{
    	    	\Input::setGet($this->detailParam, \Input::get('auto_item'));
    	}
    	if (!\Input::get($this->detailParam))
        {
    		global $objPage;
    		
    		$objPage->noSearch = 1;
    		$objPage->cache = 0;
    		
    		/** @var \PageError404 $objHandler */
    		$objHandler = new $GLOBALS['TL_PTY']['error_404']();
    		$objHandler->generate($objPage->id);
    		return;
        }
        $this->getApiAndKey();
        if ($this->api=='OSM')
        {
            $GLOBALS['TL_CSS'][] = 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css';
        }
        parent::compile();
    }

    /**
     * List all members
     */
    protected function listAllMembers()
    {
        $this->Template->error = 'kein Mitglied ausgewählt';
    }

    /**
     * List a single member
     * @param integer
     */
    protected function listSingleMember($idOrAlias)
    {
        \Contao\Controller::loadDataContainer('tl_member');
        
        if ($this->map_dtltemplate) $this->strTemplateDetail = $this->map_dtltemplate;
 
        global $objPage;
        
        $time = time();
        $this->Template = new \FrontendTemplate($this->strTemplateDetail);
        $this->Template->record = array();
        $objMember=null;
        IF ($idOrAlias)
        {
        	$objMember = MemberGoogleMapsMemberModel::findByIdOrAlias($idOrAlias);
        }
        //print_r($objMember);
        //    die("xxxx1");
        // No member found or group not allowed
        if (null == $objMember || count(array_intersect(\StringUtil::deserialize($objMember->groups, true), $this->arrMlGroups)) < 1)
        {
            $this->Template->invalid = $GLOBALS['TL_LANG']['MSC']['invalidUserId'];

            // Do not index the page
            $objPage->noSearch = 1;
            $objPage->cache = 0;

            // Send 404 header
            header('HTTP/1.1 404 Not Found');
            return;
        }

        //---------------------------------------
        //    $objGroupNames=$this->getGroupNamesAndIcons();
        $objGroupNames = MemberGoogleMapsMemberGroupModel::getGroupNamesAndIcons($this->arrMlGroups);
        
        $groupids = \StringUtil::deserialize($objMember->groups, true);
        $iconstd = null;
        unset($groups);

        foreach ($groupids as $i => $gid)
        {
            if ($objGroupNames[$gid])
                $groups[] = $objGroupNames[$gid]["name"];
            if (!$iconstd)
                $iconstd['url'] = \cm_Maps\cm_Map_lib::getMarkerIcon( $objGroupNames[$gid]["iconstd"]);
                $iconstd['icon_anchor'] = $objGroupNames[$gid]["iconstd_anchor"];
                $iconstd['popup_anchor'] = $objGroupNames[$gid]["iconstdpopup_anchor"];
                
        }
        $groupsstr = implode(", ", $groups);
        
        //---------------------------------------

        // Default variables
        $this->Template->action =  ampersand(\Environment::get('indexFreeRequest')); 
        $this->Template->referer = 'javascript:history.go(-1)';
        $this->Template->back = $GLOBALS['TL_LANG']['MSC']['goBack'];
        $this->Template->publicProfile = sprintf($GLOBALS['TL_LANG']['MSC']['publicProfile'], $objMember->lastname);
        $this->Template->noPublicInfo = $GLOBALS['TL_LANG']['MSC']['noPublicInfo'];
        $this->Template->sendEmail = $GLOBALS['TL_LANG']['MSC']['sendEmail'];
        $this->Template->submit = $GLOBALS['TL_LANG']['MSC']['sendMessage'];
        $this->Template->loginToSend = $GLOBALS['TL_LANG']['MSC']['loginToSend'];
        $this->Template->emailDisabled = $GLOBALS['TL_LANG']['MSC']['emailDisabled'];

        // Confirmation message
        if ($_SESSION['TL_EMAIL_SENT'])
        {
            $this->Template->confirm = $GLOBALS['TL_LANG']['MSC']['messageSent'];
            $_SESSION['TL_EMAIL_SENT'] = false;
        }

        // Check personal message settings
        switch ($objMember->allowEmail)
        {
            case 'email_all' :
                $this->Template->allowEmail = 3;
                break;

            case 'email_member' :
                $this->Template->allowEmail = FE_USER_LOGGED_IN ? 3 : 2;
                break;

            default :
                $this->Template->allowEmail = 1;
                break;
        }

        // No e-mail address given
        if (!$objMember->email || !strlen($objMember->email))
        {
            $this->Template->allowEmail = 1;
        } 
        else
        {
			if (!version_compare(VERSION, '3.5', '<'))
			{
				$value = \StringUtil::encodeEmail($objMember->email);
			}
			else 
			{
				$value = \String::encodeEmail($objMember->email);
			}
            $value = '<a href="mailto:' . $value . '">' . $value . '</a>';
            $this->Template->emailtext = $value;
        }

        // Handle personal messages
        if ($this->Template->allowEmail > 1)
        {
            $arrFieldEmail = array(
                'name' => 'replytoemail', 
                'label' => $GLOBALS['TL_LANG']['MSC']['cm_email'], 
                'inputType' => 'text', 
                'eval' => array('mandatory' => true, 'required' => true, 'rgxp' => 'email'));
            $arrField = array(
                'name' => 'message', 
                'label' => $GLOBALS['TL_LANG']['MSC']['message'], //['message'],
                'inputType' => 'textarea', 
                'eval' => array('mandatory' => true, 'required' => true, 'rows' => 4, 'cols' => 40, 'decodeEntities' => true));
            $arrFieldCaptcha = array(
                'name' => 'captcha', 
                'label' => $GLOBALS['TL_LANG']['MSC']['cm_captcha'], 
                'inputType' => 'captcha', 'eval' => array('mandatory' => true));

            $arrWidget = \Widget::getAttributesFromDca($arrField, $arrField['name'], '');
            $objWidget = new \FormTextArea($arrWidget);            
                       
            if ($this->cm_email_nameRequired)
            {
              $arrFieldName = array(
                  'name' => 'name', 
                  'label' => $GLOBALS['TL_LANG']['MSC']['cm_name'], 
                  'inputType' => 'text', 
                  'eval' => array('mandatory' => true, 'required' => true)
              );
              $arrNameWidget = \Widget::getAttributesFromDca($arrFieldName, $arrFieldName['name'], '');
              $objNameWidget = new \FormTextField($arrNameWidget);
            }
            
            $arrEmailWidget = \Widget::getAttributesFromDca($arrFieldEmail, $arrFieldEmail['name'], '');
            $objEmailWidget = new \FormTextField($arrEmailWidget);

            $arrCaptchaWidget = \Widget::getAttributesFromDca($arrFieldCaptcha, $arrFieldCaptcha['name'], '');
            $objCaptchaWidget = new \FormCaptcha($arrCaptchaWidget);

            // Validate widget
            if (\Input::post('FORM_SUBMIT') == 'tl_send_email')
            {
                $objWidget->validate();
                if ($this->cm_email_nameRequired){
                   $objNameWidget->validate();
                }
                else 
                {
                   $objNameWidget=null;
                }
                
                $objEmailWidget->validate();
                $objCaptchaWidget->validate();

                if (!$objCaptchaWidget->hasErrors() && !$objWidget->hasErrors() && (!$this->cm_email_nameRequired || !$objNameWidget->hasErrors()) && !$objEmailWidget->hasErrors())
                {
                    $this->cm_sendPersonalMessage($objMember, $objWidget, $objNameWidget, $objEmailWidget);
                }
            }

            $this->Template->widget = $objWidget;
            if ($this->cm_email_nameRequired)
            {
              $this->Template->namewidget = $objNameWidget;
            }
            $this->Template->emailwidget = $objEmailWidget;
            $this->Template->captchawidget = $objCaptchaWidget;
            $this->Template->submit = $GLOBALS['TL_LANG']['MSC']['sendMessage'];
        }

        $arrFieldsTmp = \StringUtil::deserialize($objMember->publicFields);
        $arrModuleFields = $this->arrMlFields;
        $arrFields = array_intersect($this->arrMlFields, $arrFieldsTmp);

		$hasExternalMap = MemberGoogleMapsMemberModel::hasMapPlaceholder($objMember->id);
		$showMap = false;
		$mapID = false;
		$acceptanceSettings=array('required'=>false,'text'=>'undefined');
        if ($this->cm_map_ondetail && $objMember->cm_allowmap)
        {
            //      if (preg_match('/^\-{0,1}\d+(\.\d*){0,1},\-{0,1}\d+(\.\d*){0,1}$/',$objMember->cm_coords))
            /* if ($this->isValidCoordinates($objMember->cm_coords))*/
            if (\cm_Maps\cm_Map_lib::validateCoordsExact($objMember->cm_coords))
            {
                if ($hasExternalMap) 
                {
                    $this->mappos = "ext";
                }
				else 
				{
	                switch ($this->cm_map_posdetail)
	                {
	                    case 'above' :
	                    case 'left' :
	                    case 'right' :
	                        $this->mappos = 'above';
	                        break;
	                    default :
	                        $this->mappos = 'below';
	                        break;
	                }
				}
                if ($this->cm_map_setstyledetail && $this->cm_map_styleiddetail)
                {
                    $objMapStyles = \Database::getInstance()
                                        ->prepare("SELECT * FROM tl_cm_gmaplayout WHERE id = ?") 
                    								 ->execute($this->cm_map_styleiddetail);

                    $objMapStyles->next;
                    $GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/cm_maps/assets/' . $objMapStyles->name . '.mst';
                    $this->cm_map_styleDetailName = 'cmMapStyle_' . $objMapStyles->id;
                }

                $mapID = "cm_map" . $this->mappos . "_" . $this->id;
                $center = $objMember->cm_map_indivcenter ? $objMember->cm_map_center : $objMember->cm_coords;
                $zoom = $objMember->cm_map_indivzoom ? $objMember->cm_map_zoom : $this->cm_map_stdzoom;
                if ($zoom == "")
                    $zoom = $this->cm_map_stdzoom;

                $tempCoords = $objMember->cm_lat . "," . $objMember->cm_lng;
                $member = array('raw'=>$objMember, 'company' => $objMember->company, 'firstname' => $objMember->firstname, 'lastname' => $objMember->lastname, 'postal' => $objMember->postal, 'street' => $objMember->street, 'h_nr' => $objMember->h_nr, 'city' => $objMember->city, 'website' => $objMember->website, );

                $googleMap = $this->getGoogleMapCode($member, $tempCoords, $center, $zoom, $iconstd, $mapID, $this->cm_map_disablewheeldetail,$this->api,$this->apiKey);
                $acceptanceSettings['required']=$googleMap->acceptanceRequired;
                $acceptanceSettings['text']=$googleMap->acceptanceText;
                $showMap = true;
                $this->Template->cookiebarInstalled=$googleMap->settings['cookiebarInstalled'];
                $this->Template->acceptanceButtontext=$googleMap->settings['cm_acceptance_buttontext'];
                $this->Template->acceptanceText=$googleMap->settings['cookiebarInstalled'] > 0 ? $googleMap->settings['cm_acceptance_text'] : $googleMap->acceptanceText;
                $this->Template->acceptanceRequired=$googleMap->acceptanceRequired;
                $this->Template->BaseScriptCode = $googleMap->BaseJsScript;
                $this->Template->GoogleMapCode = $googleMap->MainJsScript.$googleMap->Parse();

            } else
            {
                $this->Template->GoogleMapCode = '';
                $this->Template->acceptanceRequired=false;
                $this->Template->acceptanceText="";
                //$this->Template->GoogleMapCode = $GLOBALS['TL_LANG']['MSC']['cm_membergooglemaps_error'];
            }
            if ($this->cm_map_heightdetail)
            {
                $heightArr = \StringUtil::deserialize($this->cm_map_heightdetail);
                if ($heightArr["value"])
                {
                    $this->Template->mapstyle = "height:" . $heightArr["value"] . $heightArr["unit"];
                }
            }
        } 
        else
        {
            $this->Template->GoogleMapCode = "";
            $this->Template->acceptanceRequired=false;
            $this->Template->acceptanceText="";
        }
		$this->Template->details = '';
		
		$objElement = MemberGoogleMapsMemberModel::findPublishedContent($objMember->id);
		$hasExternalMap=false;
		if ($objElement !== null)
		{
			while ($objElement->next())
			{
				$element = $objElement->current();
				switch ($element->type)
				{
					case 'cm_mapPlaceholder': 
						if ($showMap)
						{
							$element->mapid=$mapID;
							$element->acceptanceRequired=$acceptanceSettings['required'];
							$element->acceptanceText=$acceptanceSettings['text'];
							$renderedElement= $this->getContentElement($element);
							$this->Template->details .= $renderedElement;
							//$this->Template->details .= "--- vor diesem Eintrag soll die Karte erscheinen!";
						}
						else {
							$this->Template->details .= '<! -- no map -->';
						}
						$hasExternalMap=true;
						break;
					default: $this->Template->details .= $this->getContentElement($element); 
						break;
				}
			}
		}

        $this->Template->mappos = $this->mappos;
        $this->Template->mapID = $mapID;

        // Add public fields
        if (\is_array($arrFields) && count($arrFields))
        {
            $count = -1;

            foreach ($arrFields as $k => $v)
           {
				if ($v=="website" && ($objMember->$v))
				{
					if (preg_match('@^(https://)@i', $objMember->$v))	
					{
						$objMember->$v = substr($objMember->$v,8);
					}
					elseif (preg_match('@^(http://)@i', $objMember->$v)) {
						$objMember->$v = substr($objMember->$v,7);
					}
				}	
                $class = 'row_' . ++$count . (($count == 0) ? ' row_first' : '') . (($count >= (count($arrFields) - 1)) ? ' row_last' : '') . ((($count % 2) == 0) ? ' even' : ' odd');

                $arrFields[$k] = array(
                	'raw' => $objMember->row(), 
                	'groups' => $groupsstr, 
                	'iconstd' => $iconstd, 
                	'content' => $this->formatValue($v, $objMember->$v, true), 
                	'class' => $class, 
                	'label' => (strlen($label = $GLOBALS['TL_DCA']['tl_member']['fields'][$v]['label'][0]) ? $label : $v), 
                	'field' => $v
				);
            }
            $this->Template->record = $arrFields;
        }
    }

    protected function getGoogleMapCode($member, $coords, $center, $zoom, $iconstd, $mapID, $disablewheel,$map_api,$map_apikey)
    {
    	if ($this->map_infdtltemplate) $this->strTemplateInfoDetail = $this->map_infdtltemplate;
		
        global $objPage;
        $googleMap = new \cm_Maps\Map(true, $map_api);
        $language = $objPage->language;
        $googleMap->language = $language;
        $googleMap->mapType = \cm_Maps\cm_Map_lib::getMapTypeStr($this->cm_map_maptypedetail);
        $googleMap->showTypePanel = $this->cm_map_choosetypedetail;

        $googleMap->layout = $this->cm_map_styleDetailName;

        $root_id = \Contao\Frontend::getRootPageFromUrl()->id;
        $root_details = \Contao\PageModel::findWithDetails($root_id);
        
        $baseJsScript = \cm_Maps\cm_Map_lib::getBaseScript($this->useSSL,$language,$map_api,$map_apikey);
        $clickType = $this->cm_map_clicktype;
        $mainJsScript = \cm_Maps\cm_Map_lib::getMarkerBaseScript($map_api,$this->useSSL,true, $clickType);
        $acceptanceRequired=false;
        $acceptanceText='';
        switch($this->cm_gm_acceptance_required)
        {
            case 'on':  $acceptanceRequired = true;
                        $acceptanceText=$this->cm_gm_acceptance_text;
                        break;
            case 'off': $acceptanceRequired = false;
                        $acceptanceText="";
                        break;
            case 'page':
            default:    $acceptanceRequired = $root_details->cm_gm_acceptance_required??false;
                        $acceptanceText = $root_details->cm_gm_acceptance_required?$root_details->cm_gm_acceptance_text:'';
                        break;
        }
        $googleMap->initCookieBarSettings($root_details);
        $googleMap->acceptanceRequired = $acceptanceRequired;
        $googleMap->acceptanceText = $acceptanceText;

        $googleMap->settings = array(
       		'disableWheel' => $disablewheel,
	        'center' => $center, 
	        'cm_map_choosetypelist' => $this->cm_map_choosetypelist, 
	        'ctrlTypeStr' => \cm_Maps\cm_Map_lib::getCtrlTypeStr($this->cm_map_ctrltypelist),
	        //      'cm_map_choosenavlist' => $this->cm_map_choosenavdetail,
	        //      'ctrlNavStr' => cm_Map_lib::getCtrlNavStr($this->cm_map_ctrlnavlist),
	        'cm_map_choosezoomlist' => $this->cm_map_choosezoomdetail, 
	        'ctrlZoomStr' => \cm_Maps\cm_Map_lib::getCtrlZoomStr($this->cm_map_ctrlzoomlist), 
	        'mapType' => \cm_Maps\cm_Map_lib::getMapTypeStr($this->cm_map_maptypedetail), 
	        'zoom' => $zoom, 
	        'mapID' => $mapID, 
	        'showTypePanel' => $this->cm_map_choosetypedetail, 
	        'infoShowOnload' => $this->cm_map_infoShowOnload, 
	        'lat' => $lat, 
	        'lng' => $lng, 
	        'indiv' => $indiv, 
	        'hasLocal' => $hasLocal, 
	        'routeToDetail' => $this->cm_map_routetodetail,
		    'clusterMarkers' => false,
            'cm_ElementOnClickRequiresResize' => $this->cm_ElementOnClickRequiresResize
        );
        $googleMap->settings['cm_acceptance_text'] = \StringUtil::toHtml5($root_details->cm_gm_acceptance_text);
        $googleMap->settings['cm_acceptance_text'] = preg_replace('/[\t\n\r]+/', ' ', $googleMap->settings['cm_acceptance_text']);
        $googleMap->settings['cm_acceptance_buttontext'] = $root_details->cm_acceptance_buttontext;
        
        unset($googleMap->marker);
        if (preg_match('/^\-{0,1}\d+(\.\d*){0,1},\-{0,1}\d+(\.\d*){0,1}$/', $coords))
        {
            $infoTemplate = new \FrontendTemplate($this->strTemplateInfoDetail);
            $infoTemplate->setData($member);
            $infoTemplate->linktowebsite = $this->cm_map_linktowebsite;
            //$infoTemplate->urldetail = $this->Template->url . ($this->cm_membergooglemaps_hidedetaillink ? '' : '?show=' . $row["id"]);
            $infoTemplate->urldetail = $this->Template->url;
            $infoText = '';
            if ($this->cm_map_routetodetail)
            {
               // $infoText = $GLOBALS['TL_LANG']['MSC']['cm_map_getroute'] . ": ";
                $infoText = '<a onclick="window.open(this.href); return false;" href="'.($this->useSSL?'https://':'http://').'google.com/maps?saddr=&ie=UTF8&hl=de&daddr='
                    .$coords.'">'.$GLOBALS['TL_LANG']['MSC']['cm_map_getroute'].'</a>';
                    //                    $infoText = $GLOBALS['TL_LANG']['MSC']['cm_map_getroute'] . ": ";
            }
            $infoTemplate->routetext = $infoText;
            $infotext = preg_replace('/[\r|\n]*/', '', addslashes($infoTemplate->parse()));
            $iconstd= \cm_Maps\cm_Map_lib::getMarkerIcon($iconstd);
            $data=array(
                'cm_coords' => $coords,
                'infotext'=> $infotext,
			    'icon'=> $iconstd
			);
            $googleMap->marker = $data;
        }

        $googleMap->BaseJsScript=$baseJsScript;
        $googleMap->generate();
        //$code = $baseJsScript;
        //$code .= $mainJsScript;
        $googleMap->MainJsScript=$mainJsScript;
        //$code .= $googleMap->parse();
        return $googleMap;
    }

    /**
     * Send a personal message
     * @param object
     * @param object
     */
    protected function cm_sendPersonalMessage(\Contao\Model\Collection $objMember, \Widget $objWidget, \Widget $objNameWidget=null, \Widget $objReplyTo)
    {
        $objEmail = new \Email();

        $objEmail->from = $GLOBALS['TL_ADMIN_EMAIL'];
        $objEmail->fromName = 'Contao mailer';
        $objEmail->text = $objWidget->value;
		if ($objNameWidget)
		{
//			$objEmail->text = sprintf($GLOBALS['TL_LANG']['MSC']['sendersProfile'],$objNameWidget->value)."\n\n---\n\n".$objEmail->text;
			$objEmail->text = $objNameWidget->value."\n\n---\n\n".$objEmail->text;
		}

        // Add reply to
        if (FE_USER_LOGGED_IN)
        {
            $this->import('FrontendUser', 'User');
            $replyTo = $this->User->email;

            // Add name
            if (strlen($this->User->firstname))
            {
                $replyTo = $this->User->firstname . ' ' . $this->User->lastname . ' <' . $replyTo . '>';
            }

            $objEmail->subject = sprintf($GLOBALS['TL_LANG']['MSC']['subjectFeUser'], $this->User->username, \Environment::get('host'));
            $objEmail->text .= "\n\n---\n\n" . sprintf($GLOBALS['TL_LANG']['MSC']['sendersProfile'], \Environment::get('base') . preg_replace('/show=[0-9]+/', 'show=' . $this->User->id, \Environment::get('request')));

            $objEmail->replyTo($replyTo);
        } else
        {
            $replyTo = $objReplyTo->value;
            $objEmail->replyTo($replyTo);
            $objEmail->subject = sprintf($GLOBALS['TL_LANG']['MSC']['subjectUnknown'], \Environment::get('host'));
        }
		$objEmail->text .="\n\n---\n\nFrom ".$replyTo;
        // Send e-mail
        $objEmail->sendTo($objMember->email);
        $_SESSION['TL_EMAIL_SENT'] = true;

        $this->reload();
    }

}

