<?php
                   
/**
 * PHP version 7
 * @copyright  Christian Muenster 2009-2023
 * @author     Christian Muenster
 * @package    cm_membermaps
 * @license    LGPL
 * @filesource
 */

namespace ChrMue\cm_MemberMaps;

/**
 * Class ModuleMemberGoogleMapsList
 *
 * @copyright  Christian Muenster 2023
 * @author     Christian Muenster
 * @package    Module
 *
 * based on ModuleMemberlist by Leo Feyer
 */
class ModuleMemberGoogleMapsList extends \Contao\ModuleMemberlist
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate_list = 'mod_cm_memberlist_googlemaps';
    protected $strTemplate_table = 'mod_cm_memberlist_googlemaps_table';
    protected $strTemplate_tabless = 'mod_cm_memberlist_googlemaps_tabless';
    protected $detailParam = 'show';
    /**
     * bubble content for the list view
     */
    protected $strTemplateInfo = 'info_cm_membergooglemaps_list';
    /**
     * use cunstructor form the base class ModuleMemberlist
     */
    function __construct($param)
    {
        parent::__construct($param);
    }

    /**
     * Groups
     * @var array
     */
    protected $arrMlGroups = array();
    protected $arrMlFields = array();
    protected $arrMlFieldsList = array();
    private $fieldsInList = array();
    private $groupInList = array();
    protected $arrSearchFields = array();
    protected $useTags;
    protected $useTagsBundle;
    private $useSSL = false;

    /**
     * position of the map - standard: below the list
     */
    protected $mappos = 'below';

    private $factor = null;

    /**
     * Fields
     * @var array
     */
    private $gcapi;
    private $gcapiKey;
    private $api;
    private $apiKey;
    
    private function getApiAndKey()
    {
        $root_id = self::getRootPageFromUrl()->id;
        $root_details =  \PageModel::findWithDetails($root_id);
        
        $gcapi = $root_details->cm_gc_api;
        if (!$gcapi) { $gcapi=\Config::get('cm_gc_api'); }
        if (!$gcapi) throw new \Exception("Api not selected");
        $gcapi_type=explode("_",  $gcapi);
        $gcapiprop="cm_map_apikey_".strtolower($gcapi_type[0]);
        
        $gcapiKey = $root_details->$gcapiprop;
        if (!$gcapiKey) { $gcapiKey=\Config::get($gcapiprop); }
        
        $api = $root_details->cm_map_api;
        if (!$api) { $api=\Config::get('cm_map_api'); }
        if (!$api) throw new \Exception("Api not selected");
        $apiprop="cm_map_apikey_".strtolower($api);
        $apiKey = $root_details->$apiprop;
        if (!$apiKey) { $apiKey=\Config::get($apiprop); }
        
        $this->gcapi=$gcapi;
        $this->gcapiKey=$gcapiKey;
        $this->api=$api;
        $this->apiKey=$apiKey;        
    }

    //protected $arrMlFields = array();

    /**
     * Display a wildcard in the back end
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### MEMBERLIST with Map  - List ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }
        if (!version_compare(VERSION, '4', '<'))
        {
            $this->useTags = $this->cm_usetags && \in_array('tags', \ModuleLoader::getActive()) && !\in_array('tl_member',explode(', ', \Config::get('disabledTagObjects')));
        }
        else
        {
            $this->useTags = $this->cm_usetags && \in_array('tags_members', \ModuleLoader::getActive()) && \in_array('tags', \ModuleLoader::getActive());
        }
        $this->useTagsBundle = $this->cm_usetags && \in_array('CodefogTagsBundle', \ModuleLoader::getActive());

        $this->arrMlGroups = \StringUtil::deserialize($this->ml_groups, true);
        $this->arrMlFields = \StringUtil::deserialize($this->ml_fields, true);

        $arrFieldsList = \StringUtil::deserialize($this->cm_membergooglemaps_fieldslist, true);
        $this->fieldsInList = $arrFieldsList;

        $this->arrSearchFields = \StringUtil::deserialize($this->cm_memberlist_searchfieldslist, true);

        if ($this->cm_memberlist_fieldsearch=='no')
        {
            $this->arrSearchFields=array();
        }
        
        $this->arrSearchFields = array_intersect($this->arrSearchFields, array_merge($arrFieldsList,$this->arrMlFields));
       
        $this->arrMlFieldsList = null;
        if ($arrFieldsList)
        {
            foreach ($arrFieldsList as $listField)
            {
                $this->arrMlFieldsList[] = $listField["field"];
            }
        }

        if (count($this->arrMlGroups) < 1 || count($this->arrMlFieldsList) < 1 || count($this->arrMlFields) < 1)
        {
            return '';
        }  		
        return parent::generate();
    }

    /**
     * Generate module
     */
    protected function compile()
    {
        $this->useSSL=\Config::get('cm_request_gm_ssl') || \Environment::get('ssl');
        // Set the item from the auto_item parameter
        if (!isset($_GET[$this->detailParam]) && \Config::get('useAutoItem') && isset($_GET['auto_item']))
        {
            \Input::setGet($this->detailParam, \Input::get('auto_item'));
        }
        $this->getApiAndKey();
        if ($this->api=='OSM')
        {
                $GLOBALS['TL_CSS'][] = 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css';
        }
        parent::compile();
    }

   /**
     * List all members
     */
    protected function listAllMembers()
    {
        \Contao\Controller::loadDataContainer('tl_member');       
        if ($this->cm_addresslist_tableless)
        {
            if ($this->map_tbltemplate) $this->strTemplate_tabless= $this->map_tbltemplate;
        } 
        else
        {
            if ($this->map_tbltemplate) $this->strTemplate_table= $this->map_tbltemplate;
        }
	if ($this->map_lsttemplate) 
        {
            $this->strTemplate_list = $this->map_lsttemplate;
        }        

        global $objPage;
        $language = $objPage->language;

        $showCircle = false;
        $latitude = null;
        $longitude = null;

        $this->Template = new \FrontendTemplate($this->strTemplate_list);
	$this->Template->allInOne = false;

        if ($this->cm_addresslist_tableless)
        {
            $this->ElementTemplate = new \FrontendTemplate($this->strTemplate_tabless);
        } 
        else
        {
            $this->ElementTemplate = new \FrontendTemplate($this->strTemplate_table);
        }

        if (!array_search("cm_coords", $this->arrMlFieldsList))
        {
            array_push($this->arrMlFieldsList, "cm_coords");
        }
        if (!array_search("cm_allowmap", $this->arrMlFieldsList))
        {
            array_push($this->arrMlFieldsList, "cm_allowmap");
        }

        $arrValidMembers = '';
        //---fuer Tags ... ---------------------------------------------------
        if ($this->useTags)
        {    
            if (strlen(\Input::get('tag')))
            {
                $relatedlist = (strlen(\Input::get('related'))) ? preg_split("/,/", \Input::get('related')) : array();
                $alltags = array_merge(array(\Input::get('tag')), $relatedlist);

                $tagids = MemberGoogleMapsTagModel::getTagIds($alltags);
                $arrValidMembers = $tagids;
                if (count($arrValidMembers) == 0)
                {
                    $this->Template->thead = array();
                    $this->Template->tbody = array();

                    // Pagination
                    $objPagination = new \Pagination($objTotal->count, $per_page);
                    $this->Template->pagination = $objPagination->generate("\n  ");
                    $this->Template->per_page = $per_page;
                    if ($this->cm_shownumberofadresses)
                    {
                        $this->Template->numberOfAddresses = sprintf(specialchars($GLOBALS['TL_LANG']['MSC']['cm_numberofmembers']),$objTotal->count);
                    }
                
                    $this->ElementTemplate->numberOfAddresses = sprintf(specialchars($GLOBALS['TL_LANG']['MSC']['cm_numberofmembers']),$objTotal->count);
                
                    // Template variables
                    $this->Template->action = ampersand(\Environment::get('request'));
                    $this->Template->search_label = specialchars($GLOBALS['TL_LANG']['MSC']['search']);
                    $this->Template->per_page_label = specialchars($GLOBALS['TL_LANG']['MSC']['list_perPage']);
                    $this->Template->search = \Input::get('search');
                    $this->Template->for = \Input::get('for');
                    $this->Template->order_by = \Input::get('order_by');
                    $this->Template->sort = \Input::get('sort');
                    $this->Template->plzarea = \Input::get('plzarea');
                    return;
                }
            }
        }
        else if ($this->useTagsBundle)
        {
            $this->setCfgTagWidgets();
            $widgets = $this->generateCfgTagWidgets();
            $arrValidMembers = $this->preselectMemberByTags();
            $this->Template->cfgTagsWidgets = $widgets;
        }

        //------------------------------------------------------
        $location = \Contao\Input::get('cm_location');

        $geocoding_required = !$this->cm_map_locationfixed && $location != "";
         $cm_show_gc_privacyError =false;
        if ($geocoding_required && $this->cm_gc_acceptance_required 
                && \Input::get('cm_gc_privacy') && \Input::get('cm_gc_privacy')!='accepted') return;
        
        $this->Template->show_gc_privacyError=$cm_show_gc_privacyError;
        if ($this->cm_gc_acceptance_required)
        {
            $this->Template->lbl_cm_gc_privacy = $this->cm_gc_acceptance_label;
        }

        
        $time = time();
        $arrFieldsList = $this->arrMlFieldsList;
        $searchObj = array(
                        'where' => '',
                        'values'=> array()
                    );

        $fieldsearch = $this->cm_memberlist_fieldsearch=='single'; 
        $multifieldsearch = $this->cm_memberlist_fieldsearch=='multi'; 
        
        $plzsearch = $this->cm_memberlist_plzsearch;
        $plzareasize = $this->cm_memberlist_plznumberdigits;

        $this->Template->fieldsearch = $fieldsearch;
        $this->Template->multifieldsearch = $multifieldsearch;
        $this->Template->plzsearch = $plzsearch;
        $this->Template->cm_map_locationfixed= $this->cm_map_locationfixed;

        // Search query
        $searchStr=\Contao\Input::get('search');
        $forStr=\Contao\Input::get('for');
        $searchObj = self::getTextSearchOptions($searchObj,$multifieldsearch,$fieldsearch,$this->arrSearchFields,$searchStr,$forStr);

        /// ----- plz search  ----------------------------------
        $plzArea = \Contao\Input::get('plzarea');
        $searchObj = self::getPlzSearchOptions($searchObj,$plzsearch,$plzareasize,$plzArea);

        //------------------------------------------------
        $isGroupRequested = null;
        $requestedGroups = array();
        if (\Input::get('gf') != '')
        {
            $requestedGroups = \Input::get('gf');
            foreach ($requestedGroups as $group)
            {
                $isGroupRequested[$group] = true;
            }
        }
        //----------------------------
        $country = \Input::get('cm_country');
        if (!$country)
        {
            $country = $this->cm_map_country;
        }
        $max_dist = null;
        if (\Input::get('cm_max_dist'))
        {
            $max_dist = \Input::get('cm_max_dist');
        }

        $std_dist = null;

        $geocoding_required = !$this->cm_map_locationfixed && $location != "";
        $hasLocal = false;
        $searchArr = array();
        if ($geocoding_required)
        {
            $visitorsLocation = str_replace(' ', '+', ($location));
            // execute geocoding
            try
            {
 //               $root_id = self::getRootPageFromUrl()->id;
 //               $root_details =  \PageModel::findWithDetails($root_id);
//                $apiKey = $root_details->cm_map_apikey;
//                if (!$apiKey) { $apiKey=\Config::get('cm_map_apikey'); }
                $data = \cm_Maps\cm_Map_lib::getGoogleMapsGeoData($visitorsLocation, $this->useSSL ,$country, $this->gcapi, $this->gcapiKey);
                //check geocoding result
                if (!$data || $data["status"]!="OK")
                    throw new \Exception("Adresse nicht gefunden");
                $latitude = $data["lat"];
                $longitude = $data["lng"];
                //list($latitude, $longitude, $altitude) = explode(",", $data);
                if (($longitude == "") || ($latitude == ""))
                    throw new \Exception("Adresse nicht gefunden");
                $hasLocal = true;
                $showCircle = $this->cm_map_showcircle && ($max_dist > 0);

                // show distance:
                $this->fieldsInList[] = array('field' => 'dist');
                $searchArr = array('location' => $location, 
                				   'country' => $country, 
                				   'max_dist' => $max_dist);
    	    } 
    	    catch (\Exception $e)
            {
                $this->Template->Error = "Fehler: " . $GLOBALS['TL_LANG']['MSC']['cm_memberlist_notfound'];
                $hasLocal = false;
            }
        }
        if ($this->cm_map_locationfixed)
        {
            $location="";
            $showCircle = $this->cm_map_showcircle && ($max_dist > 0);
            $hasLocal = true;
            $this->fieldsInList[] = array('field' => 'dist');
            $searchArr = array('location' => $location, 
                               'country' => $country, 
                               'max_dist' => $max_dist);
            list($latitude, $longitude, $altitude) = explode(',', $this->cm_map_fixlocation);
        }
        
        $this->Template->searchArr = $searchArr;
        $lat = $latitude;
        $lng = $longitude;
        //$this->Template->Error .="Standort: ".($hasLocal?'yes':'no')." L:".$longitude." B:".$latitude;
        
        if (false && $this->cm_memberlist_addressform)
        {
            $this->Template->distanceform = $this->cm_memberlist_distanceform;
            $this->Template->lbl_location = $GLOBALS['TL_LANG']['MSC']['cm_lbl_location'];
            $this->Template->lbl_country = $GLOBALS['TL_LANG']['MSC']['cm_lbl_country'];
            $this->Template->lbl_max_dist = $GLOBALS['TL_LANG']['MSC']['cm_lbl_max_dist'];
            $this->Template->lbl_max_dist_drdn = $GLOBALS['TL_LANG']['MSC']['cm_lbl_max_dist_drdn'];

            $this->Template->cm_distitem = $GLOBALS['TL_LANG']['MSC']['cm_distitem'];

            $this->Template->distsearch_label = specialchars($GLOBALS['TL_LANG']['MSC']['cm_distsearch_label']);
            $this->Template->radius_search = specialchars($GLOBALS['TL_LANG']['MSC']['cm_radius_search']);

            $this->Template->radiusform = $this->cm_memberlist_addressform;
            //      $this->Template->cm_distsearch_label=$GLOBALS['TL_LANG']['MSC']['cm_memberlist_distsearch'];
        }
        //    $this->Template->listdistance=$this->cm_memberlist_distanceintab && $hasLocal;
        $this->ElementTemplate->listdistance = $this->cm_memberlist_distanceintab && $hasLocal;
        $this->Template->visitorlocation = $location;


        $this->Template->country = $this->cm_map_country;

        if ($this->cm_map_country_as_select)
        {
            $countries = trim($this->cm_map_country_list);
            $countrylist = $countries ? explode( ',', $countries) : null;
            
            $visitorcountryobj = '<select name="cm_country" class="cm_country" >';

            foreach ($this->getCountries() as $countryCode => $countryName)
            {
                if (!$countrylist || \in_array($countryName,$countrylist))
                {
                    $visitorcountryobj .= '<option value="' . strtolower($countryCode) . '" ' 
                        .(strtolower($countryCode) == strtolower($country) ? ' selected="selected"' : '') . '>' . $countryName . '</option>';
                }
            }
            $visitorcountryobj .= '</select>';
        } 
        else
        {
            $visitorcountryobj = '<input class="cm_country" type="text" name="cm_country" value="' . $this->cm_map_country . '" />';
        }

        $this->Template->visitorcountry = $visitorcountryobj;

        $this->Template->max_dist = $max_dist;

        $strOptions = '';
        $arrSortedFields = array();

        // Sort fields
        foreach ($arrFieldsList as $field)
        {
            $arrSortedFields[$field] = $GLOBALS['TL_DCA']['tl_member']['fields'][$field]['label'][0];
        }
        natcasesort($arrSortedFields);

        // Add searchable fields to drop-down menu
        foreach ($arrSortedFields as $k => $v)
        {
            if (($k != 'cm_coords') && ($k != 'cm_allowmap'))
            {
                $strOptions .= '  <option value="' . $k . '"' . (($k == \Input::get('search')) ? ' selected="selected"' : '') . '>' . $v . '</option>' . "\n";
            }
        }

        $this->Template->search_fields = $strOptions;
        //---------------------------------------
        // Read group names and  groupe icons
        //    $objGroupNames=$this->getGroupNamesAndIcons();
        $objGroupNames = MemberGoogleMapsMemberGroupModel::getGroupNamesAndIcons($this->arrMlGroups);

	$searchObj = self::getGroupFilterSearchOptions($searchObj, $requestedGroups);
		
        //---------------------------------------
        // Build Whereclause - Groupfilter

        // Filter groups

        $searchObj = self::getGroupSearchOptions($searchObj,$this->arrMlGroups);
        
	$searchObj = self::getActiveMemberFilterOptions($searchObj,\in_array('username', $arrFieldsList), $time);
		
        // Split results
        $page = \Input::get('page') ? \Input::get('page') : 1;
        $per_page = \Input::get('per_page') ? \Input::get('per_page') : $this->perPage;

        //Sort list
        $sortorder = "";
        $firstitem = true;

        if ($this->cm_memberlist_distancesort && $hasLocal)
        {
            $sortorder = 'dist ' . $this->cm_memberlist_distanceorder;
        } 
        else
        {
            if ($this->cm_membergooglemaps_sortandorder)
            {
                foreach (\StringUtil::deserialize($this->cm_membergooglemaps_sortandorder) as $orderitems)
                {
                    if ($firstitem)
                    {
                        $sortorder .= " ";
                        $firstitem = false;
                    } else
                    {
                        $sortorder .= ",";
                    }
                    $sortorder .= $orderitems["field"] . " " . $orderitems["order"];
                }
            }
        }
        $order_by="";
        //    if (!($this->Input->get('order_by')=='dist' && !($this->Input->get('cm_max_dist') && $max_dist!=0)))
        if (!(\Input::get('order_by') == 'dist' && !$hasLocal))
        {

            $order_by = (\Input::get('order_by')) ? 
                    \Input::get('order_by') . ' ' . \Input::get('sort') : 
                    $sortorder;
        }
        // Begin query

        $distFormula = \cm_Maps\cm_Map_lib::getDistanceFormula($lat, $lng);

        // Get total number of members

       $selectedfields=array_unique(array_merge($arrFieldsList,$this->arrMlFields));
        if ($hasLocal)
        {

            $objTotal = \Database::getInstance() 
            				-> prepare("SELECT COUNT(*) AS count FROM (SELECT id, " . $distFormula . " AS dist from tl_member WHERE " 
            						. ($arrValidMembers ? "id IN (" . join(",", $arrValidMembers) 
            						. ") AND " : "") 
            						. $searchObj['where'] 
            						. ") as T WHERE " 
            				    . ((\Input::get('cm_max_dist') && $max_dist) ? " dist<=" . $max_dist : "1")) 
            				-> execute($searchObj['values']);

            				$objMemberStmt = \Database::getInstance()
            				-> prepare("SELECT id, alias,username, publicFields, `groups`" 
            					/*	. (count($arrFieldsList) > 0 ? ", " . implode(", ", $arrFieldsList) : " ")    */
            						. (count($selectedfields) > 0 ? ", " . implode(", ", $selectedfields) : " ") 
            						. ", dist"
            						. " FROM (select *, " . $distFormula . " AS dist FROM tl_member WHERE " 
            						. ($arrValidMembers ? "id IN (" . join(',', $arrValidMembers) . ") AND " : "") 
            						. $searchObj['where'] 
            						. " ) AS T WHERE " 
            				    . ((!$this->cm_map_showfar && \Input::get('cm_max_dist') && $max_dist) ? " dist<=" . $max_dist : "1") 
            						. ($order_by ? " ORDER BY " . $order_by : ""));
        } else
        {
            $objTotal = \Database::getInstance()
            				-> prepare("SELECT COUNT(*) AS count FROM tl_member WHERE " 
            						. ($arrValidMembers ? "id IN (" . join(",", $arrValidMembers) . ") AND " : "") 
            						. $searchObj['where']) 
            				-> execute($searchObj['values']);

            				$objMemberStmt = \Database::getInstance()
            				-> prepare("SELECT id, alias,username, publicFields, `groups`" 
            						. (count($selectedfields) > 0 ? ", " . implode(", ", $selectedfields) : " ") 
            						. " FROM tl_member WHERE " 
            						. ($arrValidMembers ? "id IN (" . join(',', $arrValidMembers) . ") AND " : "") 
            						. $searchObj['where'] 
            						. ($order_by ? " ORDER BY " . $order_by : ""));
        } 
        // Limit
        
        if ($per_page)
        {
            $objMemberStmt->limit($per_page, (($page - 1) * $per_page));
        }

        $memberCollection = $objMemberStmt->execute($searchObj['values']);

        // Prepare URL
        $selfUrl = preg_replace('/\?.*$/', '', \Environment::get('request'));        
             
        if (($detailPg = \Contao\PageModel::findByPk($this->cm_memberdetail_pg)) == null) {
		   $detailPg = $objPage;
		}
		
		$this->Template->url = $strUrl;

        $this->Template->baseurl = $strUrl;

        $blnQuery = false;

        // Add GET parameters
        foreach (preg_split('/&(amp;)?/', $_SERVER['QUERY_STRING']) as $fragment)
        {
            if (strlen($fragment) && strncasecmp($fragment, 'order_by', 8) !== 0 && strncasecmp($fragment, 'sort', 4) !== 0 && strncasecmp($fragment, 'page', 4) !== 0)
            {
                $strUrl .= (!$blnQuery ? '?' : '&amp;') . $fragment;
                $blnQuery = true;
            }
        }

        $strVarConnector = $blnQuery ? '&amp;' : '?';

        // Prepare table
        $arrTh = array();
        $arrTd = array();
        $arrItem = array();

        // THEAD

        //Tabellenheader erzeugen

        $tabfields = $this->fieldsInList;
        for ($i = 0; $i < count($tabfields); $i++)
        {
            $thisField = $tabfields[$i]['field'];
            if ($thisField != "cm_allowmap")
            {
                $class = '';
                $sort = 'asc';
                if ($thisField == "dist")
                {
                    $strField = $GLOBALS['TL_LANG']['MSC']['cm_distance'];
                } else
                {
                    $strField = strlen($label = $GLOBALS['TL_DCA']['tl_member']['fields'][$thisField]['label'][0]) ? $label : $thisField;
                }

                if (\Contao\Input::get('order_by') == $thisField)
                {
                    $sort = (\Contao\Input::get('sort') == 'asc') ? 'desc' : 'asc';
                    $class = ' sorted ' . \Contao\Input::get('sort');
                }

                $arrTh[] = array(
                	'link' => $strField, 
                	'href' => (ampersand($selfUrl) . $strVarConnector . 'order_by=' . $thisField) . '&amp;sort=' . $sort, 
                	'title' => specialchars(sprintf($GLOBALS['TL_LANG']['MSC']['list_orderBy'], $strField)), 
                	'class' => $class . 'col_' . $i . (($i == 0) ? ' col_first' : ''));
            }
        }
        //---------------------------------------
        $start = -1;
        $limit = $memberCollection->count();
        // TBODY
        while ($memberCollection->next())
        {
            $publicFields = \StringUtil::deserialize($memberCollection->publicFields, true);
			$allowAlias = \in_array('firstname', $publicFields)
			&& \in_array('lastname', $publicFields)
			&& \in_array('city', $publicFields);

			$detailUrlPart = ((\Config::get('useAutoItem') && !\Contao\Config::get('disableAlias')&&$memberCollection->alias&&$allowAlias) ?  '/'. $memberCollection->alias:'/show/'.$memberCollection->id );	
                
            //$detailUrl = $this->Template->url . '?show=' . $row["id"];
			$detailUrl = $this->generateFrontendUrl($detailPg->row(), $detailUrlPart, $objPage->language);

            //---------------------------------------
			$groupids = \StringUtil::deserialize($memberCollection->groups, true);
            $iconstd = null;
            $iconnear = null;
            
            unset($groups);
            foreach ($groupids as $i => $gid)
            {
                if ($objGroupNames[$gid])
                {
                    $groups[] = $objGroupNames[$gid]["name"];
                }
                if (!$iconstd && $objGroupNames[$gid]["iconstd"])
                {
                    $iconstd=array();                    
                    $iconstd['url'] = \cm_Maps\cm_Map_lib::getMarkerIcon($objGroupNames[$gid]["iconstd"]);
                    $iconstd['icon_anchor'] = $objGroupNames[$gid]["iconstd_anchor"];
                    $iconstd['popup_anchor'] = $objGroupNames[$gid]["iconstdpopup_anchor"];
                }
				
                if ($this->cm_map_nearmarker && !$iconnear)
                {
                    $iconnear=array();
                    $iconnear['url'] = \cm_Maps\cm_Map_lib::getMarkerIcon($objGroupNames[$gid]["iconnear"]);
                	$iconnear['icon_anchor'] = $objGroupNames[$gid]["iconnear_anchor"];
                	$iconnear['popup_anchor'] = $objGroupNames[$gid]["iconnearpopup_anchor"];
                	
					
                }
            }
            //die($this->cm_map_nearmarker?"true":"false");
            if (!$this->cm_map_nearmarker)
            {
                $iconnear=$iconstd;
            }
            $groupsstr = implode(", ", $groups);
            //---------------------------------------

            $x = $memberCollection->row();
            $dist = "";
            $diststr = "-";

            $near = $hasLocal && ((int)$max_dist>0) && ($memberCollection->dist == "-" 
            || $memberCollection->dist <= $max_dist);

            if ($hasLocal)
            {
                if ($memberCollection->dist != "-")
                {
                    $memberCollection->dist = round($memberCollection->dist, ($dist < 10 ? 1 : 0));
                    $memberCollection->dist = number_format($memberCollection->dist, $memberCollection->dist < 10 ? 1 : 0, $GLOBALS['TL_LANG']['MSC']['decimalSeparator'], $GLOBALS['TL_LANG']['MSC']['thousandsSeparator']);
                }
            }

            $class = 'row_' . ++$start . (($start == 0) ? ' row_first' : '') . ((($start + 1) == $limit) ? ' row_last' : '') . ((($start % 2) == 0) ? ' even' : ' odd') . ($near ? ' near' : ' ');
            if (!$hasLocal || !$max_dist || $this->cm_map_showfar || $near)
            {
                $arrData = $memberCollection->row();
                //---------------------------------------
                //use new array
                $tabfields = $this->fieldsInList;
                foreach ($tabfields as $k => $m)
                {
                    $v = $m['field'];
                    if ($v=="website" && ($memberCollection->$v))
                    {
                        if (preg_match('@^(https://)@i', $memberCollection->$v))	
                        {
                            $memberCollection->$v = substr($memberCollection->$v,8);
                        }
                        elseif (preg_match('@^(http://)@i', $memberCollection->$v)) {
                            $memberCollection->$v = substr($memberCollection->$v,7);
                        }
                    }
                    if (($v != "cm_coords") && ($v != "cm_allowmap"))
                    {
                        $value = '-';

                        if ($v == 'username' || \in_array($v, $publicFields))
                        {
                            $value = $this->formatValue($v, $memberCollection->$v);
                        }
                        if ($v == 'dist')
                        {
                            $value = $memberCollection->$v;
                        }
                        //  				$arrData = $objMember->row();
                        unset($arrData['publicFields']);

                        $arrTd[$class][$k] = array(
                            'raw' => $arrData, 
                            'groups' => $groupsstr, 
                            'iconstd' => $iconstd, 
                            'content' => $value, 
                            'class' => 'col_' . $k . (($k == 0) ? ' col_first' : ''), 
                            'id' => $memberCollection->id, 
                            'field' => $v,
                            'url' => $detailUrl,
                    //'dist' => $diststr,
                            'near' => $near);
                    }
                }

                //data for Google map etc
                //-------------------------------

                foreach ($arrFieldsList as $k => $v)
                {
                    if (($v != "cm_coords") && ($v != "cm_allowmap"))
                    {
                        $value = '-';

                        if ($v == 'username' || \in_array($v, $publicFields))
                        {
                            $value = $this->formatValue($v, $memberCollection->$v);
                        }

                        //  				$arrData = $objMember->row();

                        unset($arrData['publicFields']);
                        $arrItem[$class][$k] = array(
                            'raw' => $arrData, 
                            'groups' => $groupsstr, 
                            'iconstd' => $iconstd, 
                            'iconnear' => $iconnear, 
                            'content' => $value, 
                            'class' => 'col_' . $k . (($k == 0) ? ' col_first' : ''), 
                            'id' => $memberCollection->id, 
                            'field' => $v,
                            'allowAlias' => $allowAlias,
                        //'dist' => $diststr,
                            'near' => $near);
                    }
                }

                //-------------------------------
            }
        }
        $this->Template->notFound = ($limit == 0);  

        $routeToTextPattern = '<a onclick="window.open(this.href); return false;" '
            .'href="'.($this->useSSL?'https://':'http://').'maps.google.com/maps?saddr=&ie=UTF8&hl=de&daddr='
            .'%s">'.$GLOBALS['TL_LANG']['MSC']['cm_map_getroutetable'].'</a>';

        $this->Template->tbody = $arrTd;
        $this->Template->tbody = $arrItem;
        $this->ElementTemplate->col_last = 'col_' . ++$k;
        $this->ElementTemplate->thead = $arrTh;
        $this->ElementTemplate->tbody = $arrTd;
        $this->ElementTemplate->cm_memberlist_hidedetaillink = $this->cm_memberlist_hidedetaillink;
        $this->ElementTemplate->cm_map_routetotable = $this->cm_map_routetotable;

        $this->ElementTemplate->url = $this->Template->url;
        $this->ElementTemplate->tdDist = $GLOBALS['TL_LANG']['MSC']['cm_distance'];
        $this->ElementTemplate->routeToTextPattern=$routeToTextPattern;

        // Pagination
        $objPagination = new \Contao\Pagination($objTotal->count, $per_page);
        $this->Template->pagination = $objPagination->generate("\n  ");
        $this->Template->per_page = $per_page;

        // Template variables
        $this->Template->action = \Contao\Environment::get('indexFreeRequest'); 
        $this->Template->search_label = specialchars($GLOBALS['TL_LANG']['MSC']['search']);
        $this->Template->per_page_label = specialchars($GLOBALS['TL_LANG']['MSC']['list_perPage']);
        $this->Template->fields_label = $GLOBALS['TL_LANG']['MSC']['all_fields'][0];
        $this->Template->keywords_label = $GLOBALS['TL_LANG']['MSC']['keywords'];
        $this->Template->plz_search = $GLOBALS['TL_LANG']['MSC']['cm_plz_search'];
        $this->Template->plzarea_label = $GLOBALS['TL_LANG']['MSC']['plzarea'];

        $this->Template->search = \Contao\Input::get('search');
        $this->Template->for = \Contao\Input::get('for');
        $this->Template->order_by = \Contao\Input::get('order_by');
        $this->Template->sort = \Contao\Input::get('sort');
        $this->Template->tdDist = $GLOBALS['TL_LANG']['MSC']['cm_distance'];
	$this->Template->textOnEmpty = $this->cm_memberlist_notfound;

        $this->Template->showtable = true;

        $infoTemplate = new \FrontendTemplate($this->strTemplateInfo);
        // store googlemaps coordinates

        if ($this->cm_map_onlist)
        {
            try
            {
                $googleMapCoords = array();
                foreach ($this->Template->tbody as $class => $row)
                {
                    if ((count($row) > 0) && $row[0]["raw"]["cm_allowmap"] && $row[0]["raw"]["cm_coords"] && $row[0]["raw"]["cm_coords"] != "0,0")
                    {
                       $rawtmp[]= array();
                       $rawtmp['id'] = $row[0]["raw"]['id'];
                       foreach ($this->arrMlFields as $key) {
                         $rawtmp[$key] = $row[0]["raw"][$key];	
                       }
                       array_push($googleMapCoords, array(
                            "raw" => $rawtmp,
                            "name" => $row[0]["raw"]["lastname"],
                            "groups" => $row[0]["groups"], 
                            "iconstd" => $row[0]["iconstd"],
                            "iconnear" => $row[0]["iconnear"],
                            "coords" => $row[0]["raw"]["cm_coords"],
                            "id" => $row[0]["raw"]["id"],
                            "alias" => $row[0]["raw"]["alias"],
                            "allowAlias" => $row[0]['allowAlias'],
                            "near" => $row[0]["near"]));
                    }
                }
                if ($this->cm_map_setstylelist && $this->cm_map_styleidlist)
                {
                    $objMapStyles = \Database::getInstance()
                    	-> prepare("SELECT * FROM tl_cm_gmaplayout WHERE id = ?") 
                    	-> execute($this->cm_map_styleidlist);

                    $objMapStyles->next;
                    $GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/cm_maps/assets/' . $objMapStyles->name . '.mst';
                    $this->cm_map_styleListName = 'cmMapStyle_' . $objMapStyles->id;
                }
       //         if (count($googleMapCoords) == 0) throw new \Exception();
																			    
		if (count($googleMapCoords)==0 && !$this->cm_map_showmaponempty) throw new \Exception();
 
                //-map generieren
                switch ($this->cm_map_poslist)
                {
                    case 'above' :
                    case 'left' :
                    case 'right' :
                        $this->mappos = 'above';
                        break;
                    default :
                        $this->mappos = 'below';
                        break;
                }
                $mapID = "cm_map" . $this->mappos . "_" . $this->id;
//                $indiv = $this->cm_map_autozoomlist;
                $indiv = $this->cm_map_indivcenterlist;
                if ($indiv)
                {
                    $center = $this->cm_map_centerlist;
                }
                $zoom = $this->cm_map_zoomlist;
				
                if (count($googleMapCoords)==0) 
                {
                    $center=$this->cm_map_centerempty;
                    $zoom=$this->cm_map_zoomempty;
                    $indiv=true;
                    $this->Template->notFound = true;
                }
                $clusterStyles='';
                if ($this->cm_map_cluster) 
                {
                    $clusterGridSize=($this->cm_map_cluster_gridsize?$this->cm_map_cluster_gridsize:20);
                    $clusterMaxZoom=($this->cm_map_cluster_maxzoom?$this->cm_map_cluster_maxzoom:15);
                    
                    if ($this->cm_map_clusterlayoutid && $this->cm_map_clusterlayoutid>0)
                    {
                        $clusterLayout = new \cm_Maps\ClusterLayout();
                        $clusterStyles = $clusterLayout->compileDefinition($this->cm_map_clusterlayoutid);
                    }
                }

                $googleMap= $this->getGoogleMapCodeList($googleMapCoords, $indiv, $zoom, $center, $mapID, $this->cm_map_disablewheellist, $max_dist, $hasLocal, $lat, $lng, $showCircle,
                $this->cm_map_cluster,$clusterGridSize,$clusterMaxZoom,$clusterStyles, $this->cm_memberdetail_pg,$this->api,$this->apiKey);
                $this->Template->cookiebarInstalled=$googleMap->settings['cookiebarInstalled'];
                $this->Template->acceptanceButtontext=$googleMap->settings['cm_acceptance_buttontext'];
                $this->Template->acceptanceText=$googleMap->settings['cookiebarInstalled'] > 0 ? $googleMap->settings['cm_acceptance_text'] : $googleMap->acceptanceText;
                $this->Template->acceptanceRequired=$googleMap->acceptanceRequired;
                $this->Template->BaseScriptCode =$googleMap->BaseJsScript;
                $this->Template->clusterMarkers =$this->cm_map_cluster==true;
                if ($this->cm_map_cluster) 
                {
                    $markerClusterJs =\cm_Maps\cm_Map_lib::getMarkerClusterScript($this->useSSL,$language,$this->api,$this->apiKey); 
                    $this->Template->MarkerClusterScriptCode=$markerClusterJs;
                    $googleMap->MarkerClusterJsScript = $markerClusterJs;
                //$this->Template->ClusterScriptCode =$googleMap->ClusterJsScript;
                    //$this->Template->MarkerClusterCssCode=\cm_Maps\cm_Map_lib::getMarkerClusterCss($this->useSSL,$language,$this->api,$this->apiKey);
                }
                $this->Template->GoogleMapCode = $googleMap->MainJsScript.$googleMap->parse();
                //         $this->Template->showtable = !$this->cm_membergooglemaps_showtableonsearch
                //                                       ||($this->cm_membergooglemaps_showtableonsearch
                //                                       && $this->Template->search);
                $isSearch =  ($this->Template->for 
                                || $this->useTags 
                                || $this->Template->visitorlocation
                                || $this->Template->plzarea);
                $this->Template->showtable = !$this->cm_membergooglemaps_hidetable 
                		|| ($this->cm_membergooglemaps_showtableonsearch && $isSearch);
                //-------------------------------
                if ($this->cm_map_heightlist)
                {
                    $heightArr = \Contao\StringUtil::deserialize($this->cm_map_heightlist);
                    if ($heightArr["value"])
                    {
                        $this->Template->mapstyle = "height:" . $heightArr["value"] . $heightArr["unit"];
                    }
                }
            } 
            catch (\Exception $e)
            {
                $mapID = "";
                $this->Template->GoogleMapCode = "";
                $this->Template->acceptanceRequired=false;
                $this->Template->acceptanceText="";
            }
        } 
        else
        {
            $mapID = "";
            $this->Template->GoogleMapCode = "";
            $this->Template->acceptanceRequired=false;
            $this->Template->acceptanceText="";
            // $this->Template->showtable=true; //set on top as default
        }
        $this->Template->mappos = $this->mappos;
        $this->Template->mapID = $mapID;
        $this->Template->listElements = preg_replace('/[\r|\n]*/', '', $this->ElementTemplate->parse());
    }

    /**
     * List a single member
     * @param integer
     */
    protected function listSingleMember($id)
    {
        $this->listAllMembers();
    }

    protected function getGoogleMapCodeList($rows, $indiv, $zoom, $center, $mapID, $disablewheel, $max_dist, $hasLocal, $lat, $lng, $showCircle,
        $cluster,$cluster_gridSize,$cluster_maxZoom,$clusterStyles, $detailPgId,$map_api,$map_apikey)
    {    
    	if ($this->map_inftemplate) $this->strTemplateInfo = $this->map_inftemplate;

        global $objPage;
 		
        if (!$detailPgId || ($detailPg = \PageModel::findByPk($detailPgId)) == null) 
        {
            $detailPg = $objPage;
        }
        $language=$detailPg->language;

        $googleMap = new \cm_Maps\Map(false, $map_api);
        $googleMap->singleView = false;
        $googleMap->layout = $this->cm_map_styleListName;

        $googleMap->language = $language;
        $googleMap->mapType = \cm_Maps\cm_Map_lib::getMapTypeStr($this->cm_map_maptypelist);
        $googleMap->showTypePanel = $this->cm_map_choosetypelist;

        //    $param_refid = $this->google_api_key;
         $root_id = \Contao\Frontend::getRootPageFromUrl()->id;
         $root_details = \Contao\PageModel::findWithDetails($root_id);
//         $map_apikey = $root_details->cm_map_apikey;
//         if (!$map_apikey) $map_apikey=\Config::get('cm_map_apikey');
        
        $baseJsScript = \cm_Maps\cm_Map_lib::getBaseScript($this->useSSL, $language, $map_api,$map_apikey);
        $clickType = $this->cm_map_clicktype;
        $mainJsScript = \cm_Maps\cm_Map_lib::getMarkerBaseScript($map_api,$this->useSSL,false, $clickType, $this->cm_map_staybubbleopened);
        $clusterCssScripts = null;
        if ($cluster) 
        {
            $googleMap->MarkerClusterJsScript =\cm_Maps\cm_Map_lib::getMarkerClusterScript($this->useSSL,$language,$this->api,$this->apiKey); 
            $markerClusterCss=\cm_Maps\cm_Map_lib::getMarkerClusterCss($this->useSSL,$language,$this->api,$this->apiKey);
            $this->Template->MarkerClusterCssCode=$markerClusterCss;
            $markerClusterCssScript='';
            foreach ($markerClusterCss as $cssUrl) {
                $markerClusterCssScript .= "$('<link>').appendTo('head').attr({type: 'text/css',rel: 'stylesheet',href: '".$cssUrl."'});";
            }
            $googleMap->MarkerClusterCssScript = $markerClusterCssScript;
        }
        $acceptanceRequired=false;
        $acceptanceText='';
        switch($this->cm_gm_acceptance_required)
        {
            case 'on':  $acceptanceRequired = true;
                        $acceptanceText=$this->cm_gm_acceptance_text;
                        break;
            case 'off': $acceptanceRequired = false;
                        $acceptanceText="";
                        break;
            case 'page':
            default:    $acceptanceRequired = $root_details->cm_gm_acceptance_required??false;
                        $acceptanceText = $root_details->cm_gm_acceptance_required?$root_details->cm_gm_acceptance_text:'';
                        break;
        }
        $googleMap->initCookieBarSettings($root_details);
        $googleMap->acceptanceRequired = $acceptanceRequired;
        $googleMap->acceptanceText = $acceptanceText;
        $googleMap->settings = array(
       		'disableWheel' => $disablewheel,
        	'center' => $center, 
        	'cm_map_choosetypelist' => $this->cm_map_choosetypelist, 
        	'ctrlTypeStr' => \cm_Maps\cm_Map_lib::getCtrlTypeStr($this->cm_map_ctrltypelist),
        //      'cm_map_choosenavlist' => $this->cm_map_choosenavlist,
        //      'ctrlNavStr' => cm_Map_lib::getCtrlNavStr($this->cm_map_ctrlnavlist),
        	'cm_map_choosezoomlist' => $this->cm_map_choosezoomlist, 
        	'ctrlZoomStr' => \cm_Maps\cm_Map_lib::getCtrlZoomStr($this->cm_map_ctrlzoomlist), 
        	'mapType' => \cm_Maps\cm_Map_lib::getMapTypeStr($this->cm_map_maptypelist), 
        	'zoom' => $zoom, 
                'mapID' => $mapID, 
        	'showTypePanel' => $this->cm_map_choosetypelist, 
        	'lat' => $lat, 
        	'lng' => $lng, 
        	'indiv' => $indiv, 
                'fitToCircle' => $this->cm_fitToCircle,
        	'hasLocal' => $hasLocal, 
        	'routetodetail' => false, 
    		'clusterMarkers' => $cluster,
    		'clusterGridSize' => $cluster_gridSize,
    		'clusterMaxZoom' => $cluster_maxZoom,
            'clusterStyles' => $clusterStyles,
            'cm_ElementOnClickRequiresResize' => $this->cm_ElementOnClickRequiresResize,
			'clickTypeEvent' => $clickType==2?'click':'mouseover'

		);
        $googleMap->settings['cm_acceptance_text'] = \StringUtil::toHtml5($root_details->cm_gm_acceptance_text);
        $googleMap->settings['cm_acceptance_text'] = preg_replace('/[\t\n\r]+/', ' ', $googleMap->settings['cm_acceptance_text']);

        $googleMap->settings['cm_acceptance_buttontext'] = $root_details->cm_acceptance_buttontext;
        
        unset($googleMap->markerData);

        $data = array();
        foreach ($rows as $row)
        {
            //      if (!preg_match('/^\-{0,1}\d+(\.\d*){0,1},\-{0,1}\d+(\.\d*){0,1}$/',$row["coords"])) {
            if ( !\cm_Maps\cm_Map_lib::validateCoordsExact($row["coords"]) )
            {
                $data = array();
            } else
            {
				
                $detailUrlPart = ((\Contao\Config::get('useAutoItem') && !\Contao\Config::get('disableAlias')&&$row["alias"]&&$row["allowAlias"]) ?  '/'. $row["alias"]:'/show/'.$row["id"] );	
                    
                    //$detailUrl = $this->Template->url . '?show=' . $row["id"];
                $detailUrl = $this->generateFrontendUrl($detailPg->row(), $detailUrlPart, $language);
    				
                $infoTemplate = new \Contao\FrontendTemplate($this->strTemplateInfo);
                $infoTemplate->setData($row);
                $infoTemplate->linktowebsite = $this->cm_map_linktowebsite;
                //$infoTemplate->urldetail = $this->Template->url . ($this->cm_membergooglemaps_hidedetaillink ? '' : '?show=' . $row["id"]);
                $infoTemplate->urldetail = $this->generateFrontendUrl($detailPg->row(), ($this->cm_membergooglemaps_hidedetaillink ? '' : $detailUrlPart), $language);
                    
                $infoText = '';
                if ($this->cm_map_routetolist)
                {
                    $infoText = '<a onclick="window.open(this.href); return false;" href="'.($this->useSSL?'https://':'http://').'google.com/maps?saddr=&ie=UTF8&hl=de&daddr='
                                .$row['coords'].'">'.$GLOBALS['TL_LANG']['MSC']['cm_map_getroutelist'].'</a>';
                                    //                    $infoText = $GLOBALS['TL_LANG']['MSC']['cm_map_getroute'] . ": ";
                                    //                $infoText = $GLOBALS['TL_LANG']['MSC']['cm_map_getroute'] . ": ";
                }
                $infoTemplate->routetext = $infoText;
    
                $infotext = preg_replace('/[\r|\n]*/', '', addslashes($infoTemplate->parse()));
                $data = array('infotext' => $infotext,
                                'cm_coords' => $row["coords"],
                                'icon' => $row[($this->cm_map_nearmarker && $row["near"] == 1) ? "iconnear" : "iconstd"],
                                'color' => ($this->cm_map_nearmarker && $row["near"] == 1) ? "blue" : "",
                                'url' => $detailUrl,
                                'data' => $row['raw'],
                                'clickable' => !$this->cm_membergooglemaps_hidedetaillink,
                                'near'      => $row["near"]
                        );
            }
            $googleMap->markerData[] = $data;
        }
            
        if ($hasLocal)
        {
            $this->cm_map_locicon_anchor = \StringUtil::deserialize($this->cm_map_locicon_anchor,true);
            $indivlocIcon = array('url' => \cm_Maps\cm_Map_lib::getMarkerIcon($this->cm_map_locicon),
                'icon_anchor' => \StringUtil::deserialize($this->cm_map_locicon_anchor,true),
                'iconpopup_anchor' => \StringUtil::deserialize($this->cm_map_locicon_anchor,true)
            );
            
            $data = array(
                'infotext' => $GLOBALS['TL_LANG']['MSC']['cm_map_visitorlocation'], 
                'cm_coords' => $lat . ',' . $lng,
                'icon' =>  $this->cm_map_indivlocicon ? $indivlocIcon : null,
                'color' => 'yellow', 
                'url' => '', 
                'clickable' => false);

            $googleMap->markerData[] = $data;
        }

        if ($showCircle)
        {
            $data = array(
                'circleCoords' => $lat . ',' . $lng, 
                'radius' => $max_dist * 1000, 
                'color' => ($this->cm_map_circlecolor ? '#' . $this->cm_map_circlecolor : '#FF0000'), 
                'opacity' => 0.8, 
                'weight' => 3, 
                'fillColor' => ($this->cm_map_circlecolor ? '#' . $this->cm_map_circlecolor : '#FF0000'), 
                'fillOpacity' => 0.3);
            $googleMap->circleData = $data;
        }
        $googleMap->BaseJsScript=$baseJsScript;
        $googleMap->generate();		
        $googleMap->MainJsScript=$mainJsScript;
        //$code .= $googleMap->parse();

        //return $code;
        return $googleMap;
    }

    private static function getTextSearchOptions($searchObj,$multifieldsearch,$fieldsearch,$arrSearchFields,$searchStr,$forStr){
        if (($multifieldsearch || $fieldsearch) && $forStr != '' && $forStr != '*')
        {
            if ($multifieldsearch)
            {
                $searchObj['where'] .= 'LOWER(CONCAT(' . implode(",", $arrSearchFields) . '))' . " REGEXP LOWER(?) AND ";
                $searchObj['values'][] = $forStr;
            } 
 	        else if ($fieldsearch)
	        {
	            $searchfield = $searchStr;
                    if($searchfield)
	            {
	                if (\in_Array($searchfield,$arrSearchFields,true))
	                {
	                    $searchObj['where'] .= "LOWER(".$searchStr . ") REGEXP LOWER(?) AND ";
	                    $searchObj['values'][] = $forStr;
	                }
                        else{
	                    $searchObj['where'] .= " 1=0 AND ";
	                    
//	                    print_r($arrSearchFields); die("->".$searchfield);
	                }
	            }
	        }
        }
		return $searchObj;	
	}      

	private static function getPlzSearchOptions($searchObj,$plzsearch,$size,$plzArea)
	{
        if ($plzsearch)
        {
            if ($plzArea)
            {
                $area = trim($plzArea);
                if (preg_match('/^\d{' . $size . '}\d*$/', $area))
                {
                    $searchObj['where'] .= " postal REGEXP '^" . $area . ".*' AND ";
                }
            }
        }
		return $searchObj;
	}	

	private static function getGroupFilterSearchOptions($searchObj, $requestedGroups)
	{
	    if (!$requestedGroups || !\is_array($requestedGroups) || count($requestedGroups)==0) { return $searchObj; }
            $intMaxGrouptofilter = count($requestedGroups) - 1;
            // Filter groups
            for ($i = 0; $i <= $intMaxGrouptofilter; $i++)
            {
                $searchObj['where'] .= ($i == 0 ? "(" : "")."`groups` LIKE ?";

                if ($i < $intMaxGrouptofilter)
                {
                    $searchObj['where'] .= " OR ";
                } else
                {
                    $searchObj['where'] .= ") AND ";
                }
                $searchObj['values'][] = '%"' . $requestedGroups[$i] . '"%';
            }
            return $searchObj;
	}

	private static function getGroupSearchOptions($searchObj,$arrMlGroups)
	{
	    $intGroupLimit = (count($arrMlGroups) - 1);
        
            for ($i = 0; $i <= $intGroupLimit; $i++)
            {
                $searchObj['where'] .= ($i == 0 ? "(" : "");
                if ($i < $intGroupLimit)
                {
                    $searchObj['where'] .= "`groups` LIKE ? OR ";
                    $searchObj['values'][] = '%"' . $arrMlGroups[$i] . '"%';
                } else
                {
                    $searchObj['where'] .= "`groups` LIKE ?) AND ";
                    $searchObj['values'][] = '%"' . $arrMlGroups[$i] . '"%';
                }
            }
            return $searchObj;
	}

	private static function getActiveMemberFilterOptions($searchObj,$usernameIncluded, $time)
	{
        // Add Whereclause - List active members only
            if ($usernameIncluded)
            {
                $searchObj['where'] .= "(publicFields!='' OR allowEmail=? OR allowEmail=?) AND disable!=1 AND (start='' OR start<=?) AND (stop='' OR stop>=?)";
                array_push($searchObj['values'], 'email_member', 'email_all', $time, $time);
            } else
            {
                $searchObj['where'] .= "publicFields!='' AND disable!=1 AND (start='' OR start<=?) AND (stop='' OR stop>=?)";
                array_push($searchObj['values'], $time, $time);
            }
            return $searchObj;	
	}       

}
