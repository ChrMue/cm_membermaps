![Logo](https://christian-muenster.de/files/cm/images/logo.png)

# CmMembermaps

## Aktuelle Neuerungen

!! geocoding API unter Settings bzw. in der Startseite anpassen !! 


Die Erweiterung unterstützt jetzt contaoCookiebar und den privacy manager der Premium Contao Themes.

Sofern die oveleon/contao-cookiebar oder der privacy manager eingesetzt werden, kann googlemap bzw. openstreetmap über die cookiebar freigeschaltet werden.

Die Suche ist jetzt nicht mehr case sensitive.

Fehler beim Geocoding behoben.
