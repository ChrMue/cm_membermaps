<?php

/**
 * cm_MemberMaps
 * Extension for Contao Open Source CMS (contao.org)
 *
 * @copyright Christian Muenster 2019
 * @package cm_MemberMaps
 * @author  Christian Münster (aka ChrMue)
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */

/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace ChrMue\cm_MemberMaps;

use Contao\System;
use Symfony\Component\VarDumper\VarDumper;
use Symfony\Component\Validator\Constraints\Time;
use Contao\Controller;


/**
 * Class UpgradeHandler
 *
 * upgrades older Versions
 * @copyright Christian Muenster 2019
 * @package cm_MemberMaps
 * @author  Christian Münster (aka ChrMue)
 */
class UpgradeHandler extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }
    
    private $errors = array();

    private static function process_gm($version)
    {
        if (true)
        {
            $objDatabase=null;
            try
            {
                $objDatabase = \Contao\Database::getInstance();
            } catch (\Exception $e)
            {
                $errors[] = $e->getMessage();
            }
            if ($objDatabase->tableExists('tl_module'))
            {
                //echo "table tl_module exists<br />";
                
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_ondetail", "char(1) NOT NULL default '1' ", "cm_map_ondetail");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_onlist", "char(1) NOT NULL default '1' ", "cm_map_onlist");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_heightdetail", "varchar(64) NOT NULL default '' ", "cm_map_heightdetail");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_posdetail", "varchar(64) NOT NULL default '' ", "cm_map_posdetail");
                
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_maptypedetail", "char(1) NOT NULL default 'n' ", "cm_map_maptypedetail");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_choosetypedetail", "char(1) NOT NULL default '1' ", "cm_map_choosetypedetail");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_stdzoom", "int(2) NOT NULL default '15' ", "cm_map_stdzoom");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_heightlist", "varchar(64) NOT NULL default '' ", "cm_map_heightlist");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_poslist", "varchar(64) NOT NULL default '' ", "cm_map_poslist");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_maptypelist", "char(1) NOT NULL default 'n' ", "cm_map_maptypelist");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_choosetypelist", "char(1) NOT NULL default '1' ", "cm_map_choosetypelist");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_autozoomlist", "char(1) NOT NULL default '1' ", "cm_map_autozoomlist");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_zoomlist", "int(2) NOT NULL default '0' ", "cm_map_zoomlist");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_centerlist", "varchar(64) NOT NULL default '' ", "cm_map_centerlist");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_zoompos", "int(2) NOT NULL default '15' ", "cm_map_zoompos");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_indivcenterpos", "char(1) NOT NULL default '' ", "cm_map_indivcenterpos");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_centerpos", "varchar(64) NOT NULL default '' ", "cm_map_centerpos");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_routetodetail", "char(1) NOT NULL default '' ", "cm_map_routetodetail");
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_infoShowOnload", "char(1) NOT NULL default '' ", "cm_map_infoShowOnload");
                self::replaceField($objDatabase, "tl_module", "cm_memberlist_showfar", "char(1) NOT NULL default '' ", "cm_map_showfar");
                self::replaceField($objDatabase, "tl_module", "cm_memberlist_nearmarker", "char(1) NOT NULL default '' ", "cm_map_nearmarker");
                
                self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_tableless", "char(1) NOT NULL default '' ", "cm_addresslist_tableless");
                
                self::replaceField($objDatabase, "tl_module", "cm_memberlist_seachfieldslist", "blob NULL ", "cm_memberlist_searchfieldslist");
                self::replaceField($objDatabase, "tl_module", "cm_memberlist_multifieldseach ", "char(1) NOT NULL default '' ", "cm_memberlist_multifieldsearch ");
                
            }
            if ($objDatabase->tableExists('tl_member_group'))
            {
                // echo "table tl_member_group exists<br />";
                self::replaceField($objDatabase, "tl_member_group", "cm_membergooglemaps_iconstd", "varchar(255) NOT NULL default ''", "cm_map_iconstd");
                self::replaceField($objDatabase, "tl_member_group", "cm_membergooglemaps_iconnear", "varchar(255) NOT NULL default ''", "cm_map_iconnear");
            }
            if ($objDatabase->tableExists('tl_member'))
            {
                //echo "table tl_member exists<br />";
                $Table = 'tl_member';
                
                if ($objDatabase->fieldExists('cm_membergooglemaps_coords', $Table)
                    && !$objDatabase->fieldExists('cm_membergooglemaps_lat', $Table))
                {
                    // echo "field cm_membergooglemaps_coords exists<br />";
                    //Migration mit Neufeldanlegung
                    //Feld anlegen
                    $Declaration = "varchar(64) NOT NULL default '' ";
                    
                    self::addField($objDatabase, $Table, "cm_membergooglemaps_lat", $Declaration);
                    self::addField($objDatabase, $Table, "cm_membergooglemaps_lng", $Declaration);
                    //1.7.2
                    $Declaration = "varchar(128) COLLATE utf8_bin NOT NULL default ''";
                    self::addField($objDatabase, $Table, "alias", $Declaration);
                    
                    $objMember = $objDatabase->prepare("SELECT id,alias,firstname,lastname,city,cm_membergooglemaps_coords FROM tl_member WHERE cm_membergooglemaps_autocoords='1'")->execute();
                    // echo "<br />".$objMember->numRows;
                    while ($objMember->next())
                    {
                        $id = $objMember->id;
                        // echo "<br />ID:".$id;
                        $coords = $objMember->cm_membergooglemaps_coords;
                        // echo " coords:".$coord;
                        try
                        {
                            $setArr=array();
                            
                            if (preg_match('/^(|­{0,1}\d+(\.\d*){0,1},­{0,1}\d+(\.\d*){0,1})$/', $coords))
                            {
                                list($latitude, $longitude, $altitude) = explode(',', $coords);
                                //  echo " :: ".$latitude;
                                //  echo " :: ".$longitude;
                                $setArr['cm_membergooglemaps_lat'] = $latitude;
                                $setArr['cm_membergooglemaps_lng'] = $longitude;
                            }
                            
                            $alias =  $objMember->alias;
                            if ($alias == '')
                            {
                                $autoAlias = true;
                                $alias = sprintf('%s-%s',$objMember->firstname,$objMember->lastname);
                                if ($objMember->city)
                                {
                                    $alias .='-'.$objMember->city;
                                }
                                
                                if (!\version_compare($version, '3.5', '<'))
                                {
                                    $alias = standardize(\StringUtil::restoreBasicEntities($alias));
                                } else {
                                    $alias = standardize(\String::restoreBasicEntities($alias));
                                }
                            }
                            
                            $objAlias = $objDatabase->prepare("SELECT id FROM tl_member WHERE alias=?")
                            ->execute($alias);
                            
                            // Check whether the news alias exists
                            if ($objAlias->numRows > 1 && !$autoAlias)
                            {
                                throw new \Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $alias));
                            }
                            
                            if ($autoAlias)
                            {
                                // Add ID to alias
                                if ($objAlias->numRows)
                                {
                                    $alias .= '-' . $id;
                                }
                                $setArr['alias'] = $alias;
                            }
                            
                            try
                            {
                                $objDatabase->prepare("UPDATE " . $Table . " %s  WHERE id=?")->set($setArr)->execute($id);
                            } catch (\Exception $e)
                            {
                                $errors[] = $e->getMessage();
                            }
                        } catch (\Exception $e)
                        {
                            $errors[] = $e->getMessage();
                        }
                    }
                    $addTemplate = true;
                    
                }
                
                //Upgrade to Version 2.0
                self::replaceField($objDatabase, $Table, "cm_membergooglemaps_requestcount", "int(10) NOT NULL default '0'", "cm_googlemaps_requestcount");
                self::replaceField($objDatabase, $Table, "cm_membergooglemaps_autocoords", "char(1) NOT NULL default '1'", "cm_googlemaps_autocoords");
                self::replaceField($objDatabase, $Table, "cm_membergooglemaps_lat", "varchar(32) NOT NULL default ''", "cm_googlemaps_lat");
                self::replaceField($objDatabase, $Table, "cm_membergooglemaps_lng", "varchar(32) NOT NULL default ''", "cm_googlemaps_lng");
                self::replaceField($objDatabase, $Table, "cm_membergooglemaps_coords", "varchar(64) NOT NULL default ''", "cm_googlemaps_coords");
                self::replaceField($objDatabase, $Table, "cm_membergooglemaps_allowmap", "char(1) NOT NULL default ''", "cm_googlemaps_allowmap");
                self::replaceField($objDatabase, $Table, "cm_membergooglemaps_indivcenter", "char(1) NOT NULL default ''", "cm_googlemaps_indivcenter");
                self::replaceField($objDatabase, $Table, "cm_membergooglemaps_indivzoom", "char(1) NOT NULL default ''", "cm_googlemaps_indivzoom");
                self::replaceField($objDatabase, $Table, "cm_membergooglemaps_center", "varchar(64) NOT NULL default ''", "cm_googlemaps_center");
                self::replaceField($objDatabase, $Table, "cm_membergooglemaps_zoom", "int(2) NOT NULL default '15'", "cm_googlemaps_zoom");
            }

            if ($objDatabase->tableExists('tl_cm_location'))
            {
                //Upgrade to Version 2.0
                self::replaceField($objDatabase, "tl_cm_location", "cm_membergooglemaps_requestcount", "int(10) NOT NULL default '0'", "cm_googlemaps_requestcount");
                self::replaceField($objDatabase, "tl_cm_location", "cm_membergooglemaps_autocoords", "char(1) NOT NULL default '1'", "cm_googlemaps_autocoords");
                self::replaceField($objDatabase, "tl_cm_location", "cm_membergooglemaps_lat", "varchar(32) NOT NULL default ''", "cm_googlemaps_lat");
                self::replaceField($objDatabase, "tl_cm_location", "cm_membergooglemaps_lng", "varchar(32) NOT NULL default ''", "cm_googlemaps_lng");
                self::replaceField($objDatabase, "tl_cm_location", "cm_membergooglemaps_coords", "varchar(64) NOT NULL default ''", "cm_googlemaps_coords");
                self::replaceField($objDatabase, "tl_cm_location", "cm_membergooglemaps_allowmap", "char(1) NOT NULL default ''", "cm_googlemaps_allowmap");
                self::replaceField($objDatabase, "tl_cm_location", "cm_membergooglemaps_indivcenter", "char(1) NOT NULL default ''", "cm_googlemaps_indivcenter");
                self::replaceField($objDatabase, "tl_cm_location", "cm_membergooglemaps_indivzoom", "char(1) NOT NULL default ''", "cm_googlemaps_indivzoom");
                self::replaceField($objDatabase, "tl_cm_location", "cm_membergooglemaps_center", "varchar(64) NOT NULL default ''", "cm_googlemaps_center");
                self::replaceField($objDatabase, "tl_cm_location", "cm_membergooglemaps_zoom", "int(2) NOT NULL default '15'", "cm_googlemaps_zoom");
            }
            if (!\version_compare($version, '3.2', '<'))
            {
                
                if ($objDatabase->tableExists('tl_member_group'))
                {
                    self::convertSingleFieldIfExists($objDatabase, 'tl_member_group','cm_map_iconstd');
                    self::convertSingleFieldIfExists($objDatabase, 'tl_member_group', 'cm_map_iconnear');
                }
                if ($objDatabase->tableExists('tl_module'))
                {
                    self::convertSingleFieldIfExists($objDatabase, 'tl_module', 'cm_map_locicon');
                }
            }
        }        
    }
    
    private static function process_gm_osm($version)
    {
        
        if (true)
        {
            $objDatabase=null;
            try
            {
                $objDatabase = \Contao\Database::getInstance();
            } catch (\Exception $e)
            {
                $errors[] = $e -> getMessage();
            }
            if ($objDatabase -> tableExists('tl_module'))
            {
                //echo "table tl_module exists<br />";
                
                //self::replaceField($objDatabase, "tl_module", "cm_membergooglemaps_ondetail", "char(1) NOT NULL default '1' ", "cm_map_ondetail");
                
                
            }
            if ($objDatabase -> tableExists('tl_member_group'))
            {
                // echo "table tl_member_group exists<br />";
                //self::replaceField($objDatabase, "tl_member_group", "cm_membergooglemaps_iconstd", "varchar(255) NOT NULL default ''", "cm_map_iconstd");
            }
            
            if ($objDatabase -> tableExists('tl_member'))
            {
                self::replaceField($objDatabase, "tl_member", "cm_googlemaps_coords", "varchar(64) NOT NULL default ''", "cm_coords");
                self::replaceField($objDatabase, "tl_member", "cm_googlemaps_lat", "varchar(32) NOT NULL default ''", "cm_lat");
                self::replaceField($objDatabase, "tl_member", "cm_googlemaps_lng", "varchar(32) NOT NULL default ''", "cm_lng");
                
                self::replaceField($objDatabase, "tl_member", "cm_googlemaps_allowmap", "char(1) NOT NULL default ''", "cm_allowmap");
                self::replaceField($objDatabase, "tl_member", "cm_googlemaps_autocoords", "char(1) NOT NULL default '1'", "cm_autocoords");
                self::replaceField($objDatabase, "tl_member", "cm_googlemaps_requestcount", "int(10) NOT NULL default '0'", "cm_gc_requestcount");
                
                self::replaceField($objDatabase, "tl_member", "cm_googlemaps_indivcenter", "char(1) NOT NULL default ''", "cm_map_indivcenter");
                self::replaceField($objDatabase, "tl_member", "cm_googlemaps_center", "varchar(64) NOT NULL default ''", "cm_map_center");
                self::replaceField($objDatabase, "tl_member", "cm_googlemaps_indivzoom", "char(1) NOT NULL default ''", "cm_map_indivzoom");
                self::replaceField($objDatabase, "tl_member", "cm_googlemaps_zoom", "int(2) NOT NULL default '15'", "cm_map_zoom");
                
            }
            if ($objDatabase -> tableExists('tl_page'))
            {
                self::replaceField($objDatabase, "tl_page", "cm_map_apikey", "varchar(255) NOT NULL default ''", "cm_map_apikey_gm");
            }
        } /// V1
    }


	
    public function run()
    {
        $version = VERSION;
        self::process_gm($version);
        return;
        self::process_gm_osm($version);
    }

    static function addField($objDatabase, $Table, $FieldNew, $Declaration)
    {
            if (!$objDatabase->fieldExists($FieldNew, $Table))
            {
                    //Feld anlegen
                    try
                    {
                        $objDatabase->execute("ALTER TABLE `" . $Table . "` ADD `" . $FieldNew . "` " . $Declaration);
                    } catch (\Exception $e)
                    {
                        $errors[] = $e->getMessage();
                    }
            }
            return;
    }

    static function replaceField($objDatabase, $Table, $FieldOld, $Declaration, $FieldNew)
    {
            //	echo "migrate ".$FieldOld." to ".$FieldNew."<br />";
        if (!$objDatabase->fieldExists($FieldOld, $Table) || $objDatabase->fieldExists($FieldNew, $Table)) {return;}
            //Migration mit Neufeldanlegung
            //Feld anlegen
        try
        {
            $objDatabase->execute("ALTER TABLE `" . $Table . "` ADD `" . $FieldNew . "` " . $Declaration);
        } catch (\Exception $e)
        {
            echo "<br>".$e->getMessage();
            $errors[] = $e->getMessage();
        }

        $addTemplate = true;
        try
        {
            $objDatabase->prepare("UPDATE " . $Table . " SET " . $FieldNew . "=" . $FieldOld . " WHERE 1")->execute();
        } catch (\Exception $e)
        {
            $errors[] = $e->getMessage();
        }
        //Feld versuchen zu fuellen, macht der naechste Abschnitt
        return;
    }

    static function convertSingleFieldIfExists($objDatabase, $Table, $Field)
    {
        if ($objDatabase->fieldExists($Field, $Table))
        {
            $objDesc = $objDatabase->query("DESC $Table $Field");
	    if ($objDesc->Type != 'binary(16)')
            {
                \Contao\Database\Updater::convertSingleField($Table, $Field);
            }
        }
    }
}
