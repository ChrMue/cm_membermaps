<?php

/**
 * PHP version 5
 * @copyright  Christian Muenster 2009-2020
 * @author     Christian Muenster
 * @package    cm_maps
 * @license    LGPL
 * @filesource
 */

namespace ChrMue\cm_MemberMaps;

/**
 * Class ClusterLayout
 *
 * @copyright  Christian Münster 2020
 * @package    Controller
 */

class Inserttags extends \Frontend
{

	/**
	 * Import String library
	 */

	/**
	 * Update a Map Layout
	 * @param integer
	 */

    public function cm_memberReplaceInsertTags($strTag)
    {
        // Parameter abtrennen
        $arrSplit = explode('::', $strTag);

        if ($arrSplit[0] != 'cm_member' && $arrSplit[0] != 'cache_cm_member')
        {
            //nicht unser Insert-Tag
            return false;
        }
        
       // Parameter angegeben?
        if (isset($arrSplit[1]) && $arrSplit[1] != '')
        {
            $tag = $arrSplit[1];
            $arrDetailSplit = explode(',', $tag);
            $field = $arrDetailSplit[0];
            $reqid= $arrDetailSplit[1];
            if (!$reqid && !isset($_GET[$this->detailParam]) && \Config::get('useAutoItem') && isset($_GET['auto_item']))
        	{
        	    	$reqid = \Input::get('auto_item');
        	}
            $objMember = MemberGoogleMapsMemberModel::findByIdOrAlias($reqid);
            $arrFieldsTmp = deserialize($objMember->publicFields);
            if (strtolower($field)=='password') return "xxx";
            if ($field=='id' || in_array($field,$arrFieldsTmp))
            {   
                return $objMember->$field;
            }
        } 
        return '';
    }
}