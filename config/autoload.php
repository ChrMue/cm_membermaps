<?php

/**
 * PHP version 5
 * @copyright  Christian Muenster 2019 
 * @author     Christian Muenster 
 * @package    cm_MemberMaps
 * @license    LGPL 
 * @filesource based on original memberlist from Leo Feyer
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'ChrMue',
));  


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Models
	'ChrMue\cm_MemberMaps\MemberGoogleMapsTagModel'         => 'system/modules/cm_membermaps/models/MemberGoogleMapsTagModel.php',
	'ChrMue\cm_MemberMaps\MemberGoogleMapsMemberModel'      => 'system/modules/cm_membermaps/models/MemberGoogleMapsMemberModel.php',
	'ChrMue\cm_MemberMaps\MemberGoogleMapsMemberGroupModel' => 'system/modules/cm_membermaps/models/MemberGoogleMapsMemberGroupModel.php',
	// Modules
	'ChrMue\cm_MemberMaps\ModuleMemberGoogleMapsPos'        => 'system/modules/cm_membermaps/modules/ModuleMemberGoogleMapsPos.php',
	'ChrMue\cm_MemberMaps\ModuleMemberGoogleMapsTags'       => 'system/modules/cm_membermaps/modules/ModuleMemberGoogleMapsTags.php',
	'ChrMue\cm_MemberMaps\ModuleMemberFinder'               => 'system/modules/cm_membermaps/modules/ModuleMemberFinder.php',
    'ChrMue\cm_MemberMaps\ModuleMemberGoogleMapsList'       => 'system/modules/cm_membermaps/modules/ModuleMemberGoogleMapsList.php',
    'ChrMue\cm_MemberMaps\ModuleMemberGoogleMapsReader'	  => 'system/modules/cm_membermaps/modules/ModuleMemberGoogleMapsReader.php',
	// Classes
	'ChrMue\cm_MemberMaps\UpgradeHandler'                   => 'system/modules/cm_membermaps/classes/UpgradeHandler.php', 

//  'ChrMue\cm_MemberMaps\tl_cm_memberlist'  => 'system/modules/cm_membermaps/classes/tl_cm_memberlist.php',

//    'ChrMue\cm_MemberMaps\cm_SortWizard'                    => 'system/modules/cm_membermaps/classes/cm_SortWizard.php',
//    'ChrMue\cm_MemberMaps\cm_ListWizard'                    => 'system/modules/cm_membermaps/classes/cm_ListWizard.php',
//	'ChrMue\cm_MemberMaps\cm_map_google_lib' => 'system/modules/cm_membermaps/classes/cm_map_google_lib.php',
//  	'ChrMue\cm_MemberMaps\cm_Map_lib'                   => 'system/modules/cm_membermaps/classes/cm_Map_lib.php',
  	'ChrMue\cm_MemberMaps\memberHelper'                         => 'system/modules/cm_membermaps/classes/memberHelper.php',

    'ChrMue\cm_MemberMaps\Inserttags'	  => 'system/modules/cm_membermaps/classes/Inserttags.php'

));

//ChrMue\cm_MemberMaps\
 if (false && file_exists(TL_ROOT . '/system/modules/cm__lib/assets/base.php'))
 {
   include_once(TL_ROOT . '/system/modules/cm__lib/assets/base.php');
 }
 else
 {
  ClassLoader::addClasses(array
  (
  	'ChrMue\cm_MemberMaps\cm_mgm_fSockOpenConnect' => 'system/modules/cm_membermaps/classes/base.php',
  	'ChrMue\cm_MemberMaps\cm_mgm_cURLConnect'      => 'system/modules/cm_membermaps/classes/base.php',
  	'ChrMue\cm_MemberMaps\GoogleMapsXMLData'       => 'system/modules/cm_membermaps/classes/base.php',
  ));
}//   include_once('base.php');

/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	// 'cm_googlemap_js_main'                 => 'system/modules/cm_membermaps/templates',
	'cm_googlemap_js_routedef'             => 'system/modules/cm_membermaps/templates',
	'info_cm_membergooglemaps'             => 'system/modules/cm_membermaps/templates',
	'info_cm_membergooglemaps_list'        => 'system/modules/cm_membermaps/templates',
	'mod_cm_memberlist_finder'             => 'system/modules/cm_membermaps/templates',
	'mod_cm_memberlist_googlemaps'         => 'system/modules/cm_membermaps/templates',
	'mod_cm_memberlist_googlemaps_detail'  => 'system/modules/cm_membermaps/templates',
	'mod_cm_memberlist_googlemaps_pos'     => 'system/modules/cm_membermaps/templates',
	'mod_cm_memberlist_googlemaps_side'    => 'system/modules/cm_membermaps/templates',
	'mod_cm_memberlist_googlemaps_table'   => 'system/modules/cm_membermaps/templates',
	'mod_cm_memberlist_googlemaps_tabless' => 'system/modules/cm_membermaps/templates',
));

