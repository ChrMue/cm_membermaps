<?php

/**
 * cm_MemberMaps
 * Extension for Contao Open Source CMS (contao.org)
 *
 * @copyright Christian Muenster 2019 
 * @package cm_MemberMaps
 * @author  Christian Münster (aka ChrMue)
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */

//die("xxx");                
if(class_exists('\\ChrMue\\cm_MemberMaps\\UpgradeHandler'))
{
	$runonce= new \ChrMue\cm_MemberMaps\UpgradeHandler();
	$runonce->run();
}