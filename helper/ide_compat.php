<?php

/*
 * This file is part of Contao.
 *
 * (c) Leo Feyer
 *
 * @license LGPL-3.0-or-later
 */

// This file is not used in Contao. Its only purpose is to make PHP IDEs like
// Eclipse, Zend Studio or PHPStorm realize the class origins, since the dynamic
// class aliasing we are using is a bit too complex for them to understand.



namespace cm_Maps {
    /* interfaces */
    interface API_interface extends \ChrMue\cm_Maps\API_interface {}
    
    /* classes */
    class Map extends \ChrMue\cm_Maps\Map {}
    class cm_Map_lib extends \ChrMue\cm_Maps\cm_Map_lib {}
    class ClusterLayout extends \ChrMue\cm_Maps\ClusterLayout {}
}
namespace Database {
    class Updater extends \Contao\Database\Updater {}
}